//
//  GLOBALES
//
const ABM_FORMULARIO_CREACION   = 'c';  //Estas constantes estan definidas en el ABMEntity
const ABM_FORMULARIO_EDICION        = 'm';
const ABM_FORMULARIO_ELIMNACION     = 'd';
const ABM_FORMULARIO_ASOCIACION     = 'a';
const ABM_FORMULARIO_DESASOCIACION  = 'u';

const ABM_DATATABLE_SELECTOR = '#ABMDatatable';


var datatablesULM = [];


//
//  FUNCIONES
//


/**
 * Crea un Datatable con una configuracion default o bien es posible customizarlo pasando una configuracion propia
 * @param selector
 * @param nombreReporte (string): nombre para darle al reporte al exportarlo en CSV. Si es vacio o null se colocara un nombre default
 * @param configuracion (object): param opcional para setear las variables del datatable
 */
function DataTableULM(selector,nombreReporte,configuracion){
    var nombre = (nombreReporte)? nombreReporte:'Reporte';

    var configuracionDefault = {
        language: {
            url: '/assets/DataTables/Spanish.json'
        },
        "pageLength": 25,
        "lengthMenu": [ 25, 50, 75, 100, 150],
        "autoWidth": true,  //Si se requiere manejar widths de cada columna, desactivar y setear ese valor en colum.definitions (ver ABM de Presentaciones como ejemplo)
        dom: 'Blfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                text: '<i class="fa fa-lg fa-clipboard"></i> Copiar tabla',
                exportOptions: {
                    modifier: {
                        page: 'current'
                    }
                }
            },
            {
                text: '<i class="fa fa-lg fa-table"></i> CSV',
                extend: 'csv',
                className: 'btn btn-default ',
                title: 'HerramientasULM - '+nombre,
                extension: '.csv',
                fieldSeparator: ';',
                charset: 'UTF8',
                exportOptions: {
                    modifier: {
                        page: 'current'
                    }
                }
            }
        ]
    };


    if(typeof configuracion !== "undefined"){
        //se setea la configuracion default segun los campos provistos por el usuario
        $.extend( configuracionDefault, configuracion );
    }

    datatablesULM[selector] = $(selector).DataTable(configuracionDefault);
}

/**
 * Añade a las columnas la opcion de filtrado fronEnd
 * @param selector del datatable
 */
function agregarFiltradoDatatableULM(selector) {
    var cabecerasSinFiltro=[];
    var filtrosHTML = [];

    var indice=0;
    $(selector + ' thead th').each( function () {
        var filtrar = $(this).data('filtrar');  //data-filtrar="false"

        //Si no sido definido, se asume q es filtrable...sino se verifica que se haya seteado en false
        if(filtrar == undefined | filtrar == "false"){
            filtrosHTML.push('<td><input type="text" placeholder="'+$(this).text()+'" title="Filtre por este campo" /></td>');
        }
        else{
            cabecerasSinFiltro.push(indice);
            filtrosHTML.push('<td><input type="text" style="display: none;"/></td>');
        }
        indice++;
    } );

    var subCabeceraHTML = '<tr class="subCabecera">';
    filtrosHTML.forEach(function(filtroHTML,index){
        subCabeceraHTML+=filtroHTML;
    });
    subCabeceraHTML+='</tr><tr class="controladores"><td colspan="100%" style="">' +
        '<a role="button" class="btn btn-info btn-sm btnLimpiarFiltrosDatatable" title="Limpiar filtros"><i class="fa fa-trash-o fa-fw" ></i></a>' +
        '<a role="button" class="btn btn-success btn-sm btnRefrescarDatatable" title="Refrescar tabla"><i class="fa fa-refresh fa-fw" ></i></a>' +
        ' </td></tr>';

    $(selector + ' thead ').append(subCabeceraHTML);

    // Apply the search
    indice=0;
    datatablesULM[selector].columns().every( function () {
        var that = this;

        if(!cabecerasSinFiltro.includes(indice)){
            $($(selector+' .subCabecera input').get(indice)).on('keyup change', function () {
                if (that.search() !== this.value) {
                    that.search(this.value).draw();
                }
            } );
        }
        indice++;
    });
}

function datatableULMAjaxRefresh(selector) {
    if(typeof selector == 'undefined'){
        selector = Object.keys(datatablesULM)[0]; //setea la default
    }

    datatablesULM[selector].ajax.reload();
}



//
//  FUNCIONES DEL ABM
//

/**
 * Realiza un request al ABM segun la convencion (ver Edicion de PNUDPResentaciones p.e).
 * Procesa el formulario indicado por su name (formName) y si sale todo satisfactorio muestra el mensaje del response y ejecuta el CallbackOK(). En caso de error muestra el error retornado por el servidor
 * @param formName nombre del formulario
 * @param callbackOK (opciona) callback invocado tras mostrar mensaje satisfactorio
 */
function formularioABM(formName, callbackOK){
    var $form = $('form[name="'+formName+'"]');
    //Se valida el formulario, y si esta OK se envia por ajax
    if($form.get(0).reportValidity()) {
        $.ajax({
            type: "POST",
            url: '/abm/?_entity=' + $form.data('entity'),
            data: $form.serialize(),
            dataType: 'json',
            success: function (response) {
                if (response.error === false) {
                    alertify.success(response.message);
                    if (typeof callbackOK === 'function') {
                        callbackOK();
                    }
                }
                else {
                    alertify.alert('Error en el formulario', response.message);
                }
            },
            error: function (jqXHR, exception) {
                //ReadyState:
                //  0 error de conexion.
                //  4 error de HTTP
                var motivo = (jqXHR.readyState == 0) ? 'ha fallado la conexion con el servidor.' : 'Error HTTP (' + jqXHR.responseText + ').';
                alertify.alert('Ha ocurrido un error en la invocacion al ABM!', 'Motivo: ' + motivo);
            }
        });
    }
    else{
        alertify.alert('El formulario no ha sido correctamente cargado: verifique los datos ingresados!.');
    }
}



/**
 * Acondiciona un formulario para que pueda operar para la creacion de una nueva entidad.
 * @param formName
 * @param callbackOK (opciona) callback invocado tras finalizar las tareas
 */
function formularioABMCreacion(formName, callbackOK){
    var $form = $('form[name="'+formName+'"]');
    //Limpio sus campos (ante posible datos cargados del uso de edicion).
    $('form').find('input').val('');

    //Seteo al formulario para que funcione como de creacion
    $form.find('input[name="_operation"]').val(ABM_FORMULARIO_CREACION);
    $form.parent().parent().find('#modalABM_operacionLabel').html('Creación');
    $form.parent().parent().find('#modalABM_operacionButton').html('Crear');

    if(typeof callbackOK === 'function'){
        callbackOK();
    }
}



/**
 * Rellena un formulario con los datos de la entidad. Para ello debe definirse el formulario como el de "formularioABM".
 * @param formName
 * @param id
 * @param callbackOK (opciona) callback invocado tras recuperar la entity y rellenar los campos
 */
function formularioABMEdicion(formName, id, callbackOK){
    var $form = $('form[name="'+formName+'"]');
    //Seteo al formulario para que funcione como de Edicion
    $form.find('input[name="_operation"]').val(ABM_FORMULARIO_EDICION);
    $form.parent().parent().find('#modalABM_operacionLabel').html('Edición');
    $form.parent().parent().find('#modalABM_operacionButton').html('Editar');

    var ABMEntity = $form.data('entity');

    showEntity(ABMEntity, id, function(entity){
        //se iteran las propiedades de la entity, y se setean los campos del form
        var propiedades = Object.keys(entity);
        propiedades.forEach(function(propiedad, index){
            $form.find('*[name="entity['+propiedad+']"]').val(entity[propiedad]);
        });
        if(typeof callbackOK === 'function'){
            callbackOK();
        }
    })
}

/**
 * Realiza el request para eliminar la entidad (entity) especificando su ID.
 * Si logra encontrarla, invoca el callback con el objeto representando esa entidad,
 * caso contrario lo comunica directamente con mensajes sobre la interfaz
 * @param ABMEntity nombre de la ABMEntity(PNUDPresentacion,..)
 * @param id de la entidad
 * @param callbackOK (opcional) de la entidad
 */
function deleteEntity(ABMEntity, id, callbackOK){
    $.ajax({
        type: "GET",
        url: '/abm/?_entity='+ABMEntity,
        data:{'entity[id]':id,'_operation':ABM_FORMULARIO_ELIMNACION},
        dataType: 'json',
        success: function( response ) {
            if(response.error === false){
                //dato de la entity
                if(typeof callbackOK === 'function'){
                    callbackOK();
                }
            }
            else{
                alertify.alert('Ha ocurrido un error','No se pudo eliminar la entidad.<br/>'+response.message);
            }
        },
        error: function(jqXHR, exception) {
            alertify.alert('Ha ocurrido un error en la invocacion al ABM!. Motivo '+jqXHR.responseText);
        }
    });
}


/**
 * Realiza el request para asociar la entidad (ABMEntity) con otra (targetEntity).
 * Si logra asociarla, invoca el callback, caso contrario lo comunica directamente con mensajes sobre la interfaz
 * @param ABMEntity nombre de la ABMEntity(PNUDPresentacion,..)
 * @param id de la entidad
 * @param targetEntity nombre de la entity(PNUDPresentacion,..) que se quiere asociar
 * @param targetId de la entidad target
 * @param operacion string constante que indica si se realizara una asociacion o desasociacion. (ABM_FORMULARIO_XXX)
 * @param callbackOK (opcional) de la entidad
 */
function asociateEntity(ABMEntity, id,targetEntity, targetId, operacion,  callbackOK){
    $.ajax({
        type: "GET",
        url: '/abm/?_entity='+ABMEntity,
        data:{'entity[id]':id,'_operation':operacion,'target[entity]':targetEntity,'target[id]':targetId},
        dataType: 'json',
        success: function( response ) {
            if(response.error === false){
                //dato de la entity
                if(typeof callbackOK === 'function'){
                    callbackOK();
                }
            }
            else{
                alertify.alert('Ha ocurrido un error','No se pudo asociar la entidad.<br/>'+response.message);
            }
        },
        error: function(jqXHR, exception) {
            alertify.alert('Ha ocurrido un error en la invocacion al ABM!. Motivo '+jqXHR.responseText);
        }
    });
}




/**
 * Consulta al usuario sobre la eliminacion de un elemento de la tabla del ABM , si responde  afirmativamente se procede a su eliminacion.
 * Una vez finalizada el flujo se ejecuta el callback si fue definido
 *
 * Se arma un confirm con el nombre representativo de la entidad a eliminar, a partir del nombre de la columna del Datatable del ABM (rowFieldName)
 *
 * @param ABMEntity nombre de la ABMEntity(PNUDPresentacion,..)
 * @param id de la entidad
 * @param rowFieldName nombre del campo del Datatable del ABM (nombre, insumo,..)
 * @param callback (opcional) de la entidad
 */
function formularioEliminacion(ABMEntity, id,rowFieldName, callbackOK){
    var nombre = datatablesULM[ABM_DATATABLE_SELECTOR].row('#entity_'+id).data()[rowFieldName];

    alertify.confirm(
        'Eliminación del dato',
        'Esta seguro que desea eliminar '+nombre+' (#'+id+')?.<br/><small>Esta operacion no puede deshacerse!.</small>',
        function(){
            deleteEntity(ABMEntity,id,function(){
                //refrescamos el datatable
                datatableULMAjaxRefresh(ABM_DATATABLE_SELECTOR);

                alertify.success('Se eliminó '+nombre+' ;)');
                if(typeof callbackOK === 'function'){
                    callbackOK();
                }
            });
        },
        function(){
            //cuando cancela...
        }
    );
}
/**
 * Realiza el request para obtener los datos de la entidad (entity) especificando su ID.
 * Si logra encontrarla, invoca el callback con el objeto representando esa entidad,
 * caso contrario lo comunica directamente con mensajes sobre la interfaz
 * @param ABMEntity nombre de la ABMEntity(PNUDPresentacion,..)
 * @param id de la entidad
 * @param callback (opcional) de la entidad
 */
function showEntity(ABMEntity, id, callbackOK){
    $.ajax({
        type: "GET",
        url: '/abm/ajax.php?entity='+ABMEntity+'&operation=show&parameters[id]='+id,
        dataType: 'json',
        success: function( response ) {
            if(response.error === false){
                //dato de la entity
                if(typeof callbackOK === 'function'){
                    callbackOK((response.data[0]));
                }
            }
            else{
                alertify.alert('Ha ocurrido un error','No se pudo recuperar la informacion de la entidad.<br/>Motivo: '+response.message);
            }
        },
        error: function(jqXHR, exception) {
            alertify.alert('Ha ocurrido un error en la invocacion al ABM!. Motivo '+jqXHR.responseText);
        }
    });
}


/**
 * Realiza el request para invocar un metedo via ajax. Se requiere indicar el nombre y los datos
 * Si logra encontrarla, invoca el callback con el objeto representando esa entidad,
 * caso contrario lo comunica directamente con mensajes sobre la interfaz
 * @param ABMEntity nombre de la ABMEntity(PNUDPresentacion,..)
 * @param id de la entidad
 * @param callback (opcional) de la entidad
 */
function callOperationEntity(ABMEntity, metodo,data, callbackOK){
    //construcion de la URL incluyendo o no los datos si fueron definidos
    var url= '/abm/ajax.php?entity='+ABMEntity+'&operation=call_operation&parameters[method_name]='+metodo;
    if(data){
        url = url +'&parameters[method_data]='+data;
    }

    $.ajax({
        type: "GET",
        url: url ,
        dataType: 'json',
        success: function( response ) {
            if(response.error === false){
                //dato de la entity
                if(typeof callbackOK === 'function'){
                    callbackOK((response.data));
                }
            }
            else{
                alertify.alert('Ha ocurrido un error','No se pudo recuperar la informacion de la entidad.<br/>Motivo: '+response.message);
            }
        },
        error: function(jqXHR, exception) {
            alertify.alert('Ha ocurrido un error en la invocacion al ABM!. Motivo '+jqXHR.responseText);
        }
    });
}
//  FIN FUNCIONES DEL ABM


/**
 * Crea un selector enriquecido de insumos de la ULM
 *
 * @param selector es el elemento que se va a convertir en el selector (por lo general un input)
 * @param inputTarget es el elemento donde se va a almacenar el valor
 * @return la instancia creada
 */
function selectorInsumosULM(selectorRender, inputTarget){
    var $ms = $(selectorRender).magicSuggest({
        data: '/abm/ajax.php?entity=Insumo&operation=select',
        valueField: 'codigoOCA',
        displayField: 'insumo',
        groupBy: 'insumo',
        allowFreeEntries: false,    //Inhabilita la introduccion de valores
        renderer: function(data){
            return '<div class="ms-insumo">' +
                    '  <div class="name">' + data.insumo + '  ( '+data.codigoOCA+')</div>' +
                    '  <div style="clear:both;"></div>' +
                    '  <div class="prop">' +
                    '    <div class="lbl">Presentacion : </div>' +
                    '    <div class="val">' + data.presentacion + '</div>' +
                    '  </div>' +
                    '  <div class="prop">' +
                    '    <div class="lbl">Proveedor : </div>' +
                    '    <div class="val">' + data.proveedor + '</div>' +
                    '  </div>' +
                    '  <div style="clear:both;"></div>' +
                    '</div>';
        },
        selectionRenderer: function(data){
            return data.insumo +' ('+data.codigoOCA +')';
        },
        maxSelection: 1,    //Maxima seleccion de elementos o introduccion de datos
        maxSelectionRenderer: function(v){
            return 'Solo es posible seleccionar un insumo';
        },

        minChars:3,         //Cantidad minima de caracteres para desplegar las sugerencias
        minCharsRenderer: function(v){
            return 'Ingrese un par de caracteres para buscar';
        },
        noSuggestionText: 'No hay insumos que concuerden con la busqueda :(',
        placeholder: 'Busque un insumo...',
        required: true
    });

    //Se setea el valor seleccionado
    $($ms).on('selectionchange', function(e,m){
       $(inputTarget).val(this.getValue()[0]);
    });

    return $ms;
}

//Setups requeridos
$(function() {

    $('.dataTable').each(function(){
       agregarFiltradoDatatableULM('#'+$(this).prop('id'));
    });

    $('.btnLimpiarFiltrosDatatable').click(function () {
        var $inputs = $(this).parent().parent().parent().find('.subCabecera input');
        $inputs.val('');
        $inputs.trigger('change');
    });


    $('.btnRefrescarDatatable').click(function () {
        datatableULMAjaxRefresh();
    });
    //deshabilitacion de elementos del menu
    $(".nav li.disabled a").unbind("click");
}
);

