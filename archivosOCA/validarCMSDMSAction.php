<?php
include_once('../core/kernel.php');
include_once('../core/config.php');

/**
 * Script que valida que el par de CMS+DMS sea valido con respecto a los datos que contiene (integridad de la informacion)
 */

//  CONSTANTES
//

//Debe terminar segun Windows (CR LF/ 0x0D 0x0A)
const REGEX_FIN_LINEA = '/\x0d\x0a$/m';


const REGEX_REGISTRO_CMS = '/^(?=.{418}$)
(?<Nro_de_Pedido>[0-9]{16})
(?<Letra>[M]{1})
(?<Identificacion>[S]{1})
(?<Origen>[0-9]{4})
(?<Division>[0-9]{3})
(?<Tipo_Transaccion>[0-9]{6})
(?<Fecha_de_pedido>[0-9]{8})
(?<Fecha_de_aprobacion>[0-9]{8})
(?<Cod_Cliente_destinatario>[0-9]{6})
(?<Cuit_Destinatario>[0-9]{15})
(?<Razon_Social_destinatario>.{30})
(?<Localidad_Destinatario>.{30})
(?<Codigo_Postal_Destinatario>[0-9]{5})
(?<Deposito_Reparto>[0-9]{4})
(?<Prioridad>.{4})
(?<Observacion01>.{30})
(?<Observacion02>.{30})
(?<Observacion03>.{30})
(?<Observacion04>.{30})
(?<domicilio_entrega>.{30})
(?<condicion_pago>.{4})
(?<gestion_cobranza>.{1})
(?<importe_gestion_cobranza>[0-9]{9})
(?<items_pedido>[0-9]{4})
(?<Total_unidades_pedido>[0-9]{13})
(?<Grupo_cliente>.{2})
(?<tipo_pedido>.{3})
(?<grupo_mercaderia>.{2})
(?<fecha_pactada_entrega>[0-9]{8})
(?<origen_remito>[0-9]{4})
(?<numero_remito>[0-9]{8})
(?<valor_declaro_seguro>[0-9]{9})
(?<total_kilos_pedido>[0-9]{7})
(?<total_bultos>[0-9]{7})
(?<tipo_comprobante>.{4})
(?<numero_comprobante_orig>.{8})
(?<numero_guia>[0-9]{10})
(?<usuario_proceso>.{10})
(?<fecha_proceso>[0-9]{8})
(?<hora_proceso>[0-9]{6})
$/ixm';


const REGEX_REGISTRO_DMS = '/^(?=.{170}$)
(?<Nro_de_Pedido>[0-9]{16})
(?<Letra>[M]{1})
(?<Identificacion>[S]{1})
(?<Origen>[0-9]{4})
(?<Codigo_de_producto>090[0-9A-Z]{15})
(?<Ubicacion>[0-9]{6})
(?<Cantidad_pedida>[0-9]{13})
(?<Cantidad_FARM>[0-9]{13})
(?<Lote_especifico>.{15})
(?<Observaciones_lote>.{30})
(?<Tipo_de_producto>.{2})
(?<Estado_linea>.{1})
(?<Usuario_de_proceso>.{10})
(?<Fecha_de_proceso>[0-9]{8})
(?<Hora_de_proceso>[0-9]{6})
(?<Codigo_de_aduana>.{2})
(?<Numero_de_despacho>.{16})
(?<Fecha_de_nacionalizacion>[0-9]{8})
$/ixm';



//  FUNCIONES
//

function interpretarArchivoCMS($uploadFile, &$estructura){
    $nombreArchivo = $uploadFile['name'];

    $estructura['nombre']   = $nombreArchivo;

    $archivo = fopen($uploadFile['tmp_name'],"r");

    $linea = fgets($archivo);

    $numeroDeLinea = 0;
    do{
        $lineaProcesable = substr($linea,0,-2);

        //Si no es el registro fin..
        if(!preg_match_all('/^FIN/m',$lineaProcesable)){
            $numeroDeLinea++;

            if (preg_match_all(REGEX_REGISTRO_CMS, $lineaProcesable, $resultadoRegex)) {
                $registro = [
                    'numeroDeLinea' => $numeroDeLinea,

                    'numeroPedido' => (int)ltrim($resultadoRegex['Nro_de_Pedido'][0], '0'),

                    'cantidadItems' => (int)ltrim($resultadoRegex['items_pedido'][0], '0'),
                    'cantidadTotal' => (int)ltrim($resultadoRegex['Total_unidades_pedido'][0], '0'),

                    'destinatario' => rtrim($resultadoRegex['Razon_Social_destinatario'][0]),
                    'codigo_destino' => ltrim($resultadoRegex['Cod_Cliente_destinatario'][0],'0'),
                ];

                $estructura['datos'][] = $registro;
            } else {
                throw new Exception('Hay registros que no concuerdan con la especificacion del CMS o no han sido posible interpertarlos!.');
            }
        }
        $linea = fgets($archivo);
    } while (!feof($archivo));

    fclose($archivo);

    $estructura['lineas'] = $numeroDeLinea;

    //Eliminacion del archivo temporal
    unlink($uploadFile['tmp_name']);
}

/**
 * Se interpreta el DMS y genera la estructura correspondiente. Adicionalmente devuelve los codigo de OCA involucrados
 * @param $uploadFile archivo subidos
 * @param $estructura datos interpretados
 * @return array codigos de OCA involucrados (univocos) en la distribucion.
 * @throws Exception
 */
function interpretarArchivoDMS($uploadFile, &$estructura){
    $nombreArchivo = $uploadFile['name'];

    $estructura['nombre']       = $nombreArchivo;

    //se calcula la fecha en base al nombre del archivo
    $nombreArchivoFecha = null;
    preg_match('/^DMS([0-9]{2})([0-9]{2})0[01][0-9]/',$nombreArchivo,$nombreArchivoFecha);    // Grupo de captura #1: "MM". Grupo #2: "DD"
    if($nombreArchivoFecha && (count($nombreArchivoFecha) >= 2)){  //>1 indica se capturo la fecha
        $estructura['fecha']   =  $nombreArchivoFecha[2].'/'.$nombreArchivoFecha[1].'/'.date('Y');
        $estructura['fecha_mes']   =  trim($nombreArchivoFecha[1],'0');

        $mesDistribucion = $estructura['fecha_mes'];
        //Si la distribucion se hizo en los primeros dias, se la considera del mes anterior
        if(trim($nombreArchivoFecha[2],'0')>= 1 && trim($nombreArchivoFecha[2],'0') <=7){
            $mesDistribucion--;

            if($mesDistribucion == 0){
                $mesDistribucion = 12;
            }
        }


        // mes de distribucions: mes siguiente, legible.
        switch ($mesDistribucion){
            case 1:     $estructura['fecha_mesDistribucion'] = 'Febrero';
                        break;

            case 2:     $estructura['fecha_mesDistribucion'] = 'Marzo';
                        break;

            case 3:     $estructura['fecha_mesDistribucion'] = 'Abril';
                        break;

            case 4:     $estructura['fecha_mesDistribucion'] = 'Mayo';
                        break;

            case 5:     $estructura['fecha_mesDistribucion'] = 'Junio';
                        break;

            case 6:     $estructura['fecha_mesDistribucion'] = 'Julio';
                        break;

            case 7:     $estructura['fecha_mesDistribucion'] = 'Agosto';
                        break;

            case 8:     $estructura['fecha_mesDistribucion'] = 'Septiembre';
                        break;

            case 9:     $estructura['fecha_mesDistribucion'] = 'Octubre';
                        break;

            case 10:    $estructura['fecha_mesDistribucion'] = 'Noviembre';
                        break;

            case 11:    $estructura['fecha_mesDistribucion'] = 'Diciembre';
                        break;

            case 12:    $estructura['fecha_mesDistribucion'] = 'Enero';
                        break;
        }
    }

    $archivo = fopen($uploadFile['tmp_name'],"r");

    $linea = fgets($archivo);

    $numeroDeLinea = 0;
    $codigosOCA = [];   //estructura auxiliar codigo OCA
    do{
        $lineaProcesable = substr($linea,0,-2);

        //Si no es el registro fin..
        if(!preg_match_all('/^FIN/m',$lineaProcesable)){
            $numeroDeLinea++;

            if(preg_match_all(REGEX_REGISTRO_DMS, $lineaProcesable, $resultadoRegex)){
                $numeroPedido = (int) ltrim($resultadoRegex['Nro_de_Pedido'][0],'0');

                $codigoOCA = substr($resultadoRegex['Codigo_de_producto'][0],3);

                $registro = [
                    'numeroDeLinea' => $numeroDeLinea,

                    'codigoOCA'     => $codigoOCA,
                    'cantidad'      => (int) ltrim($resultadoRegex['Cantidad_pedida'][0],'0'),
                ];

                $estructura['datos'][$numeroPedido][] = $registro;

                //Se añade el codigo en la estructura auxiliar
                $codigosOCA[] = $codigoOCA;
            }
            else{
                throw new Exception('Hay registros que no concuerdan con la especificacion del DMS o no han sido posible interpertarlos!.');
            }
        }

        $linea = fgets($archivo);
    } while (!feof($archivo));

    fclose($archivo);

    $estructura['lineas'] = $numeroDeLinea;

    //Eliminacion del archivo temporal
    unlink($uploadFile['tmp_name']);

    return array_unique($codigosOCA);
}

/**
 * Indica si estan bien nombrados los archivos (y por ende se corresponden). Se incluye que contemplen el nombre: [CD]MSMMDD0[01][0-9]
 * @param $cms
 * @param $dms
 * @return bool true si estan bien nombrados, false caso contrario
 */
function validarNombreDeArchivos($cms,$dms){
    //Se espera que el nombre sea al menos XMSMMDD0[01][0-9]
    if(!preg_match('/^CMS[0-9]{4}0[01][0-9]/',$cms['nombre'])){
        return false;
    }

    if(!preg_match('/^DMS[0-9]{4}0[01][0-9]/',$dms['nombre'])){
        return false;
    }

    $subStringNombreCMS = substr($cms['nombre'],3);
    $subStringNombreDMS = substr($dms['nombre'],3);

    return ($subStringNombreCMS == $subStringNombreDMS);
 }



function validacionDeIntegridad($cms, $dms, &$observaciones = []){

    foreach($cms['datos'] as $registroCMS){
        //Existe el pedido del CMS en el DMS?
        if(array_key_exists($registroCMS['numeroPedido'],$dms['datos'])){
            $registrosDMSAsociados = $dms['datos'][$registroCMS['numeroPedido']];

            //La cantidad de items reportada coincide con las lineas asociadas del DMS?
            if($registroCMS['cantidadItems'] != count($registrosDMSAsociados)){
                $observaciones[] = 'ERROR#03: La cantidad de insumos involucrados (items) en el CMS con numero de pedido '.$registroCMS['numeroPedido'].' destino "'.$registroCMS['destinatario'].'" (Linea #'.$registroCMS['numeroDeLinea'].') no coinciden con las reportadas en el DMS. Cantidad en el CMS: '.$registroCMS['cantidadItems'].', cantidad en el DMS: '.count($registrosDMSAsociados);
            }

            //Las cantidad totalizada de los DMS concuerda con la reportada en el CMS?
            $totalizacionCantidadesDMS = 0;
            foreach($registrosDMSAsociados as $registroDMS){
                $totalizacionCantidadesDMS+=$registroDMS['cantidad'];
            }

            if($registroCMS['cantidadTotal'] !== $totalizacionCantidadesDMS){
                $observaciones[] = 'ERROR#04: La cantidad total de insumos reportados en el CMS con numero de pedido '.$registroCMS['numeroPedido'].' destino "'.$registroCMS['destinatario'].'" (Linea #'.$registroCMS['numeroDeLinea'].') no coinciden con la totalizacion en el DMS. Cantidad en CMS: '.$registroCMS['cantidadTotal'].', cantidad totalizada en el DMS: '.$totalizacionCantidadesDMS;
            }
        }
        else{
            $observaciones[] = 'ERROR#02: No existen registros asociados en el DMS con pedido con destino "'.$registroCMS['destinatario'].'" (Linea #'.$registroCMS['numeroDeLinea'].') . Posiblemente el numero de pedido sea erroneo o en el DMS se hace referencia a otro.';
        }
    }

    //En el DMS se hacen referencia a pedidos inexistentes en el CMS?
    $pedidosDMS = array_keys($dms['datos']);
    $pedidosCMS = array_column($cms['datos'], 'numeroPedido');

    //los pedidos del CMS es igual al merge de los pedidos del cms+dms?
    if(array_diff($pedidosCMS,$pedidosDMS)){
        $observaciones[] = 'ERROR#05: hay numeros de pedidos definidos en el DMS que no se corresponden con los del CMS. Numeros de pedidos involucrados en los archivos: <ol><li>CMS: '.implode($pedidosCMS,', ').'<br/>(Diferencias con el DMS: '.implode(array_diff($pedidosCMS,$pedidosDMS),', ').')</li><li>DMS: '.implode($pedidosDMS,', ').'<br/>(Diferencias con el CMS: '.implode(array_diff($pedidosDMS,$pedidosCMS),', ').')</li></ol>';
    }
}


/**
 * Recupera toda la info requerida sobre los insumos: nombre, presentacion, multiplicador y programa asociado
 * @param array $codigosOCA
 */
function obtenerDetallesInsumos($db,$codigosOCA = []){
    $detallesInsumos = [];
    if($codigosOCA){
        //Se ejecuta la query para obtener la info

        $codigosOCAInString = "'".implode("','",$codigosOCA)."'";

        $resultadoDB = $db->select('
            select codigo_oca, v_i.nombre as insumo, v_i.nombre_ft as nombre_ft,presentacion, presentacion_multiplicador as multiplicador, p.nombre as programa
            from	v_insumos v_i
                    left join INSUMOS_PROGRAMA ip
                    on (ip.insumo = v_i.id)
                    left join PROGRAMAS p
                    on (ip.programa = p.id)
            where codigo_oca in ('.$codigosOCAInString.')
        ');


        //se recorre y normaliza la respuesta
        foreach ($resultadoDB as $resultado){
            $registro = [
                'insumo'        => $resultado->insumo,
                'nombre_ft'     => $resultado->nombre_ft,
                'presentacion'  => $resultado->presentacion,
                'multiplicador' => $resultado->multiplicador,
                'programa'      => $resultado->programa
            ];

            $detallesInsumos[$resultado->codigo_oca]=$registro;

        }
    }

    return $detallesInsumos;
}


/**
 * Recupera toda la info de los posibles destinos involucrados
 */
function obtenerDetallesDestinos($db){
    $detallesDestinos = [];

    $resultadoDB = $db->select('
        select id, nombre, nombre_corto, nombre_ft
        from pnud_destinos
    ');

    //se recorre y normaliza la respuesta
    foreach ($resultadoDB as $resultado){
        $registro = [
            'nombre'        => $resultado->nombre,
            'nombre_corto'  => $resultado->nombre_corto,
            'nombre_ft'  => $resultado->nombre_ft,
        ];

        $detallesDestinos[$resultado->id]=$registro;
    }

    return $detallesDestinos;
}


//  MAIN
//

//Se carga el archivo enviado por el usuario
$uploadFileCMS = $_FILES['archivoCMS'];
$uploadFileDMS = $_FILES['archivoDMS'];

$response = [
    'error'         => false,
    'mensaje'       => '',
    'observaciones' => [],

    'cms'           => [],  //datos interpretados
    'dms'           => [],

    'insumos'       => []   //info extra para dar detalles en el frondEnd
];

//Estrucutras donde se contendra toda la info a analizar/validar:
//  [ nombre=>'CMSXXXXXXX.txt', lineas=>10, 'datos'=>[ [numeroDeLinea=>1, 'numeroPedido'=>'xxxxx','cantidadItems'=>2,'cantidadTotal'=>5000],...]]
$cms = [
    'nombre' => '',
    'lineas' => '',
    'datos'  => []
];

//  [ nombre=>'DMSXXXXXXX.txt', lineas=>10, 'datos'=>[ [numeroDeLinea=>1, 'numeroPedido'=>'xxxxx','cantidadItems'=>2,'cantidadTotal'=>5000],...]]
$dms = [
    'nombre'    => '',
    'lineas'    => '(no calculadas)',
    'fecha'     => '(no calculada)',
    'fecha_mes' => '(no calculado)',
    'fecha_mesDistribucion' =>  '(no calculado)',   //Segun como lo usamos en la ULM
    'datos'     => []
];

$codigosOCA = [];
try{
    interpretarArchivoCMS($uploadFileCMS, $cms);
    $codigosOCA = interpretarArchivoDMS($uploadFileDMS, $dms);

    //Primera validacion
    if(! validarNombreDeArchivos($cms,$dms)){
        $response['observaciones'][] = 'ERROR#01: Los archivos tienen nombres que indican que no se corresponden!.';
    }

    //Validacion de integridad
    validacionDeIntegridad($cms,$dms,$response['observaciones']);

    //Se adjuntan para visualizar
    $response['cms'] = $cms;
    $response['dms'] = $dms;

    //Se asocia la informacion requerida
    $response['insumos'] = obtenerDetallesInsumos($db,$codigosOCA);
    $response['destinos'] = obtenerDetallesDestinos($db);
}
catch (Exception $exception){
    $response['error']   = true;
    $response['mensaje'] = $exception->getMessage();
}

echo json_encode($response);
?>