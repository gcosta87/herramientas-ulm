<?php include_once('../core/config.php'); ?>
<?php include_once('../core/header.php'); ?>
<div class="bs-docs-section">
    <div class="page-header">
        <div class="row">
            <div class="col-md-12">
                <h1 id="buttons"><i class="fa fa-check-square fa-fw" aria-hidden="true"></i> Validador CMS+DMS <small>Validación de integridad de datos!</small></h1>
            </div>
        </div>
    </div>


    <div class="alert alert-warning alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4>Los archivos que se van a analizar deben estar correctamente generados!</h4>
        <p>Los archivos que se van analizar tuvieron que haber pasado satisfactoriamente el <strong>validador de la especificación</strong> y/o haberse generado por algún Sistema (no de forma manual).</p>
        <p>Si se sube un archivo mal formado/generado puede dar resultados erroneos.</p>
        <br/>
        <p><a style="margin-left: 100px;" role="button" class="btn btn-default" href="validar.php">Ir al validador</a></p>
    </div>

    <h2>Análisis <small>Se determinará si los archivos poseen datos válidos (integridad)</small></h2>
    <form name="formulario" action="/archivosOCA/validarCMSDMSAction.php" method="post" enctype="multipart/form-data">
        <div class="row">

                <div class="col-md-6">
                    <input id="archivoCMS" name="archivoCMS" type="file"  class="file-loading"/>
                </div>
                <div class="col-md-6 ">
                    <input id="archivoDMS" name="archivoDMS" type="file"  class="file-loading"/>
                </div>
        </div>
        <div class="row">
                <div class="col-md-1 col-md-offset-5">
                    <input type="submit" id="btnAnalizar" role="button" class="btn btn-success btn-lg" value="Analizar" style="margin-top: 50px;"/>
                </div>
        </div>
    </form>
    <div class="row">
        <div class="col-md-offset-10 col-md-2">
            <span data-toggle="tooltip" title="Ver detalles del proceso de analisis" data-placement="top"><a nohref role="button" class="btn btn-default" data-toggle="collapse" data-target="#detallesAnalisis">¿Qué se analizará?</a></span>
        </div>
        <div class="col-md-12">
            <div id="detallesAnalisis" class="collapse">
                <h2>Detalles del Análisis <small>Los elementos que se analizan</small></h2>
                <ul>
                    <li>Nombre de ambos archivos se debe corresponder.</li>
                    <li>Integridad en los datos:
                        <ul>
                            <li>Numeros de pedido: que los numeros de pedido se correspondan en ambos archivos, y no haya ninguna diferencia o inconsistencia (referencia a pedido no declarado)</li>
                            <li>De cada pedido, que concuerden:
                                <ol>
                                    <li>Las cantidades expresadas de items ("renglones").</li>
                                    <li>La sumatoria de insumos distribuidos.</li>
                                </ol>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<style>
    #observaciones ol li {
        margin-bottom: 8px;
    }

    #observaciones ol li tt {
        background-color: #e0e0e5;
        padding: 2px 6px;
        font-family: monospace, sans-serif;
    }

    dd{
        margin-bottom:5px;
    }
</style>
    <div id="resultados" class="collapse">
        <h2>Resultados del análisis</h2>

        <h3>Informacion básica</h3>
        <div clas="row">
            <div class="col-sm-3 col-sm-offset-1">
                <h4>CMS</h4>
                <dl>
                    <dt>Nombre:</dt>
                    <dd><span id="cms_nombre" class="spanVariable"></span></dd>
                    <dt>Registros:</dt>
                    <dd><span id="cms_registros" class="spanVariable"></span></dd>
                </dl>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
                <h4>DMS</h4>
                <dl>
                    <dt>Nombre:</dt>
                    <dd><span id="dms_nombre" class="spanVariable"></span></dd>
                    <dt>Registros:</dt>
                    <dd><span id="dms_registros" class="spanVariable"></span></dd>
                </dl>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
                <h4>Distribución</h4>
                <dl >
                    <dt>Mes:</dt>
                    <dd><span id="distribucion_mes" class="spanVariable"></span></dd>
                    <dt>Fecha:</dt>
                    <dd><span id="distribucion_fecha" class="spanVariable"></span></dd>
                </dl>
            </div>
        </div>
        <br/>
        <h3>Observaciones</h3>
        <div id="observaciones"></div>
        <br/><br/>
        <h3>Interpretación <small>Decodificacion de lo analizado</small></h3>
        <div id="interpretacionContenedor"></div>
    </div>
<script>

    /**
     * Se restablecen los componentes
     */
    function restablecerComponentes(){
        //limpieza del componente
        setTimeout(function(){
            //Restablecimiento de Krajee
            $("#archivoCMS,#archivoDMS").fileinput('refresh');
        },250);

        $('#interpretacionTabla').prev().hide();
        $('#interpretacionTabla').html('');
        $('#detallesAnalisis').collapse('hide');
    }

    function configurarFileInput(selectorJQuery, leyenda){
        $(selectorJQuery).fileinput({
            language: 'es',
            allowedFileExtensions: ['txt'],
            browseClass: "btn btn-primary",
            browseLabel: leyenda,
            showCancel:false,
            showCaption: false,
            showUpload: false,
            showClose:false,
            required: true
        });
    }

    $(document).on('ready', function() {
        configurarFileInput('#archivoCMS', 'Seleccionar CMS');
        configurarFileInput('#archivoDMS', 'Seleccionar DMS');

        //El usr selecciono un archivo
        $("#archivoCMS,#archivoDMS").on('fileselect', function(event, numFiles, label) {
            //Restablecimiento de panel de resultados
            $('#resultados').collapse('hide');
            $('.spanVariable').text('');

            $('#observaciones').html('');

            $('#interpretacionContenedor').prev().hide();
            $('#interpretacionContenedor').html('');

        });



        $('form[name="formulario"]').submit(function(e) {
            e.preventDefault();

            var postData = new FormData(this);

            $.ajax({
                url : $(this).attr('action'),
                type: $(this).attr('method'),
                data : postData,
                processData: false,
                contentType: false,
                success:function(data, textStatus, jqXHR){
                    var respuesta = JSON.parse(data);
                    restablecerComponentes();

                    if(!respuesta.error){
                        $('#cms_nombre').text(respuesta.cms.nombre);
                        $('#cms_registros').text(respuesta.cms.lineas);

                        $('#dms_nombre').text(respuesta.dms.nombre);
                        $('#dms_registros').text(respuesta.dms.lineas);


                        $('#distribucion_mes').text(respuesta.dms.fecha_mesDistribucion);
                        $('#distribucion_fecha').text(respuesta.dms.fecha);


                        if(respuesta.observaciones.length >= 1) {
                            var observacionesHTML = '<ol>';
                            respuesta.observaciones.forEach(function (observacion, index) {
                                observacionesHTML += '<li>' + observacion + '</li>';
                            });
                            observacionesHTML += '</ol>';
                            $('#observaciones').html(observacionesHTML);
                        }
                        else{
                            var mensajeSatisfactorio = '<div class="alert alert-info fade in" role="alert">'+
                                '<h4>Excelente!</h4>'+
                                '<p>Los datos definidos en los archivo son correctos!.</p>'+
                                '</div>';

                            $('#observaciones').html(mensajeSatisfactorio);

                            //renderizar los datos como para seguimiento de pedidos
                            renderizarDistribucacion(respuesta);
                        }
                    }
                    else{
                        //Se indica que hubo un error al procesar los archivos (posible no cumplimiento de especificacion,etc..)
                        var mensajeError = '<div class="alert alert-danger alert-dismissible fade in" role="alert">'+
                        '<h4>Hubo un error al procesar los archivos!</h4>'+
                        '<p>'+respuesta.mensaje+'</p>'+
                        '</div>';

                        $('#observaciones').html(mensajeError);
                    }

                    //Se muestra el panel
                    $('#resultados').collapse('show');
                },
                error: function(jqXHR, textStatus, errorThrown){
                    //error "critico"
                    alertify.error('Ocurrio un error al enviar los archivos al servidor :(...');
                }
            });
        });
    });

    function renderizarDistribucacion(respuesta){
        var cms = respuesta.cms,
            dms=respuesta.dms,
            insumos=respuesta.insumos,
            destinos = respuesta.destinos;

        //muestro el header
        $('#interpretacionContenedor').prev().show();

        tablaHTML = '<dl class="dl-horizontal">\
                        <dt>Mes de distribucion:</dt>\
                        <dd>'+dms.fecha_mesDistribucion+'</dd>\
                        <dt>Fecha:</dt>\
                        <dd>'+dms.fecha+'</dd>\
                    </dl><br/>';

        //Se define el HTML de la tabla
        // numeroDePedido, Mes distribuicion, Fecha, programa, destino, Insumo, presentacion, cajas, unidades,
        tablaHTML +=     '<table class="table table-condensed table-striped table-hover">\
                        <thead>\
                        <th>Pedido</th>\
                        <th>Programa</th>\
                        <th>Destino</th>\
                        <th>Codigo OCA</th>\
                        <th>Insumo</th>\
                        <th>Presentacion</th>\
                        <th>Cajas</th>\
                        <th>Unidades</th>\
                        </thead>';

        //estructura auxiliar para almancenar numeroDePedido->destinos
        arrPedidoDestinos = [];
        cms.datos.forEach(function(registro,index){
            arrPedidoDestinos[registro.numeroPedido]=destinos[registro.codigo_destino];
        });

        Object.keys(dms.datos).forEach(function(numeroDePedido,index){
            pedido = dms.datos[numeroDePedido];

            pedido.forEach(function(renglon, index){
                //datos del insumo y destino
                dataInsumo = insumos[renglon.codigoOCA];

                dataDestino = arrPedidoDestinos[numeroDePedido];

                renglonHTML = '<tr class="'+(((dataInsumo == null) || (dataInsumo.programa == null))? 'danger':'')+'">';
                //numeroDePedido,  programa, destino, Insumo, presentacion, cajas, unidades,

                renglonHTML+='<td>'+numeroDePedido+'</td>';
                renglonHTML+='<td class="text-capitalize">'+((dataInsumo.programa != null)? dataInsumo.programa : "(desconocido)" )+'</td>';
                renglonHTML+='<td>'+((dataDestino.nombre_ft != null)? dataDestino.nombre_ft:dataDestino.nombre_corto)+'</td>';
                renglonHTML+='<td>'+renglon.codigoOCA+'</td>';
                renglonHTML+='<td title="'+((dataInsumo!=null)? dataInsumo.insumo:'(desconocido)')+'">'+((dataInsumo!=null)? ((dataInsumo.nombre_ft != null)? dataInsumo.nombre_ft:dataInsumo.insumo) : '('+renglon.codigoOCA+')')+'</td>';
                renglonHTML+='<td>'+dataInsumo.presentacion+'</td>';
                renglonHTML+='<td>'+renglon.cantidad+'</td>';
                renglonHTML+='<td>'+(dataInsumo.multiplicador * renglon.cantidad)+'</td>';

                renglonHTML+='</tr>';

                tablaHTML+=renglonHTML;
            });
        });

        tablaHTML+='</table>';
        $('#interpretacionContenedor').html(tablaHTML);
    }

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

</script>
    </div>
<?php include_once('../core/footer.php'); ?>