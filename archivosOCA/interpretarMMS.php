<?php include_once('../core/config.php'); ?>
<?php include_once('../core/header.php'); ?>
<div class="bs-docs-section">
    <div class="page-header">
        <div class="row">
            <div class="col-md-12">
                <h1 id="buttons"><i class="fa fa-file-text fa-fw" aria-hidden="true"></i> Intérprete de MMS <small>Interprete y analice el contenido de un MMS!</small></h1>
            </div>
        </div>
    </div>

    <h2>Interpretación <small>Se analizará el archivo</small></h2>
    <div class="row">
        <div class="col-md-11 col-md-offset-1">
            <input id="archivo" name="archivo" type="file" class="file-loading"/>
        </div>
    </div>

    <div id="resultados" class="collapse">
        <h2>Resultados del análisis</h2>

        <h3>Informacion básica</h3>
        <strong>Nombre</strong>: <span id="archivo_nombre" class="spanVariable"></span><br/>
        <strong>Cantidad de registros</strong>: <span id="archivo_lineas" class="spanVariable"></span><br/>
        <strong data-toggle="tooltip" data-placement="top" title="Cantidad de insumos concretos involucrados en el archivo">Cantidad de insumos</strong>: <span id="insumos_cantidad" class="spanVariable"></span><br/>
        <br/>
        <h3>MMS de la OCF <span id="ocf" class="spanVariable"></span></h3>
        <div id="interpretacion"></div>
    </div>
<script>

    /**
     * Se restablecen los componentes
     */
    function restablecerComponentes(){
        //limpieza del componente
        setTimeout(function(){
            //Restablecimiento de Krajee
            $("#archivo").fileinput('refresh');
        },250);
    }


    $(document).on('ready', function() {
        $("#archivo").fileinput({
            uploadUrl: '/archivosOCA/interpretarMMSAction.php',
            maxFilePreviewSize: 10240,
            language: 'es',
            allowedFileExtensions: ['txt'],
            showCancel:false,
            showCaption: false,
            showClose:false,
            dropZoneTitle:'Arrastre un <strong>MMS</strong>',
            browseOnZoneClick: true,
            showBrowse: false
        });

        //El usr selecciono un archivo
        $('#archivo').on('fileselect', function(event, numFiles, label) {
            //Restablecimiento de panel de resultados
            $('#resultados').collapse('hide');
            $('.spanVariable').text('');

            $('#interpretacion').html('');
        });

        //Una vez subido el archivo
        $("#archivo").on('fileuploaded', function(event, data, previewId, index) {
            var form = data.form, files = data.files, extra = data.extra, response = data.response, reader = data.reader;
            restablecerComponentes();

            //Seteo del panel resultados
            $('#archivo_nombre').text(response.archivo.nombre);
            $('#archivo_lineas').text(response.archivo.lineas);
            $('#ocf').text(response.ocf);

            var interpretacion = response.interpretacion;
            var dataInsumos = response.dataInsumos;
            var insumos = Object.keys(interpretacion);

            $('#insumos_cantidad').text(insumos.length);

            var interpretacionHTML = '<div class="row">';
            insumos.forEach(function(insumo, index){
                //Por cada insumo genero un panel
                var panelInsumo = '<div class="col-sm-6">';
                var filaTablas ='';
                var totalCajas = 0;
                var totalUnidades = 0;

                interpretacion[insumo].forEach(function(registro, index){
                    filaTablas+='<tr>';
                    filaTablas+='<td>'+registro.cajas+'</td>';
                    if(dataInsumos.hasOwnProperty(insumo)){
                        filaTablas+='<td>'+(registro.cajas * dataInsumos[insumo].multiplicador)+'</td>';
                        totalUnidades+= parseInt(registro.cajas * dataInsumos[insumo].multiplicador);
                    }
                    else{
                        filaTablas+='<td> - </td>';
                    }

                    filaTablas+='<td>'+registro.lote+'</td>';
                    filaTablas+='<td>'+registro.vencimiento+'</td>';
                    filaTablas+='</tr>';

                    totalCajas+= parseInt(registro.cajas);

                });

                panelInsumo +='<div class="panel panel-default"><div class="panel-heading">'+insumo+'</div>';
                panelInsumo+= '<div class="panel-body">';
                if(dataInsumos.hasOwnProperty(insumo)){
                    panelInsumo+= '<strong>Insumo</strong>: '+dataInsumos[insumo].insumo+'<br/>';
                    panelInsumo+= '<strong>Presentación</strong>: '+dataInsumos[insumo].presentacion+'<br/>';
                }
                else{
                    panelInsumo+= '<p class="text-warning">El insumo no es conocido por el Sistema. Reportelo para que sea incluido en la nómina.</p>';
                }

                panelInsumo+= '<table class="table"><thead><tr><th>Cajas</th><th>Unidades</th><th>Lote</th><th>Vencimiento</th></tr></thead><tbody>';
                panelInsumo+= filaTablas;

                panelInsumo+= '</tbody></table><p class="text-right">Total Cajas: '+totalCajas+'&nbsp;&nbsp;&nbsp; Unidades: '+((totalUnidades == 0)? ' - ':totalUnidades)+'</p>';
                panelInsumo+='</div>';  //panel body
                panelInsumo+='</div>';  //panel
                panelInsumo+='</div>';  //col-sm

                interpretacionHTML+=panelInsumo;
            });

            interpretacionHTML+='</div>';

            $('#interpretacion').html(interpretacionHTML);


            //Se muestra el panel
            $('#resultados').collapse('show');
        });
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    })
</script>
<?php include_once('../core/footer.php'); ?>