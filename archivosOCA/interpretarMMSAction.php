<?php
include_once('../core/kernel.php');
include_once('../core/config.php');
use Models\Insumo;


//  CONSTANTES
//
const REGEX_REGISTRO_MMS = '/^(?=.{128}$)
(?<Nro_de_Remito>[0-9]{10})
(?<Codigo_movimiento>.{3})
(?<Codigo_motivo>.{4})
(?<Remito_interno>[0-9]{6})
(?<Almacen_destino>.{4})
(?<Almacen_origen>.{4})
(?<Compania>.{3})
(?<Codigo_producto>.{15})
(?<Lote>.{15})
(?<Unidades>[0-9]{13})
(?<Unidad_de_medida>.{3})
(?<Codigo_aduana>.{2})
(?<Numero_despacho>.{16})
(?<Fecha_nacionalizacion>[0-9]{8})
(?<Fecha_proceso>[0-9]{8})
(?<Fecha_vencimiento>[0-9]{8})
(?<Hora_proceso>[0-9]{6})
$/ixm';

//  FUNCIONES
//

function interpretarArchivo($uploadFile, &$response){
    $nombreArchivo = $uploadFile['name'];
    $response['archivo']['nombre']=$nombreArchivo;

    $response['interpretacion'] = [];   //Estructura para renderizar posteriormente via JS. Ejemplo: codigo_oca => [[cantidad,lote,vto,..],...],
    $response['archivo']['lineas']  = '(no calculadas)';

    $archivo = fopen($uploadFile['tmp_name'],"r");

    $linea = fgets($archivo);

    $codigosOCA= [];    //array auxiliar para posteriormente añadir informacion extra
    $numeroDeLinea = 0;

    do{
        //TODO / FIXME se borran los ultimos dos bytes de la linea: asi se sabe la longitud y se salvan las regex que fallan ante este tipo de EOL (se requieren probar modificadores,...)
        $lineaProcesable = substr($linea,0,-2);
        $numeroDeLinea++;

        $remitoString = null;

        if(preg_match_all(REGEX_REGISTRO_MMS,$lineaProcesable,$resultadoRegex)){
            $codigoOCA = $resultadoRegex['Codigo_producto'][0];
            $codigosOCA[]=$codigoOCA;

            $remitoString = $resultadoRegex['Nro_de_Remito'][0];

            $fechaVencimientoString =$resultadoRegex['Fecha_vencimiento'][0];   //AAAAMMDD

            $registro = [
                'cajas'      => ltrim($resultadoRegex['Unidades'][0],'0'),
                'lote'          => rtrim($resultadoRegex['Lote'][0]),
                'vencimiento'   => substr($fechaVencimientoString,6,2).'/'.substr($fechaVencimientoString,4,2).'/'.substr($fechaVencimientoString,0,4),
            ];

            $response['interpretacion'][$codigoOCA][]= $registro;
        }
        else{
             //Se le indica al componente de archivos que hubo un indicio de error
            $response['error'] = 'Hay registros que no concuerdan con la especificacion o no han sido posible interpertarlos';
        }

        $linea = fgets($archivo);
    } while (!feof($archivo));

    fclose($archivo);

    //Se añade informacion extra: Insumo, presentacion y multiplicador
    $response['dataInsumos']= [];
    foreach ($codigosOCA as $codigoOCA){
        $insumo = Insumo::where('codigo_oca','=',$codigoOCA)->first();

        if($insumo){
            $registroInsumo = [
                'insumo' => $insumo->PNUDInsumo->nombre_oca,
                'presentacion' => $insumo->PNUDPresentacion->nombre,
                'multiplicador' => $insumo->PNUDPresentacion->multiplicador,
            ];

            $response['dataInsumos'][$codigoOCA] = $registroInsumo;
        }

    }

    $response['archivo']['lineas'] = $numeroDeLinea;
    $remitoString = ltrim($remitoString,'0');
    $response['ocf'] = substr($remitoString,0,-2).'/'.substr($remitoString,-2);
}


//  MAIN
//

//Se carga el archivo enviado por el usuario
$uploadFile = $_FILES['archivo'];

$response = [
    'error' => '',  //Utilizado para enviar mensajes al FileUploader Krajee: se muestra al usuario dentro de la caja de Drag&Drop

    'mensaje' => '',    // Uso interno
];
//Se analiza el tipo de Archivo y se analiza segun su caso


interpretarArchivo($uploadFile,$response);

//Eliminacion del archivo temporal
unlink($uploadFile['tmp_name']);

echo json_encode($response);
?>