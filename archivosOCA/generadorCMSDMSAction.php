<?php
include_once('../core/kernel.php');
include_once('../core/config.php');
use Models\PNUDDestino;

//TODO adaptar todo para el contexto CMS+DMS
const FIN_DE_LINEA = "\r\n";

//  FUNCIONES HELPERS / CONVERSORES
//
function extraerNumeroDePedidoString($pedido){
    return str_pad($pedido['numero'],16,'0',STR_PAD_LEFT);
}

function renderizarFechaString(){
    return date('Ymd');
}

//  Relacionadas al CMS
function renderizarDestinoString(PNUDDestino $destino){
    $destinoString = str_pad($destino->id,6,'0',STR_PAD_LEFT);

    $destinoString .= '000000000000000'; //cuit

    $destinoString .= str_pad(substr($destino->nombre,0,30),30,' ',STR_PAD_RIGHT);
    $destinoString .= str_pad(substr($destino->localidad,0,30),30,' ',STR_PAD_RIGHT);

    //Codigo Postal, prioridad y observaciones
    $destinoString .= '000000000                                                                                                                            ';

    $destinoString .= str_pad(substr($destino->direccion,0,30),30,' ', STR_PAD_LEFT);

    return $destinoString;
}


function renderizarTotalizacionInsumos($pedido){
    //Totalizacion de insumos involucrados
    $totalizacionString = str_pad(count($pedido['insumos']),4,'0',STR_PAD_LEFT);

    //totalizacion de cantidades de insumos a ditribuir
    $totalizacion = 0;
    foreach($pedido['insumos'] as $insumo){
        $totalizacion += intval($insumo['cantidad']);
    }

    $totalizacionString .= str_pad($totalizacion,13,'0',STR_PAD_LEFT);

    return $totalizacionString;
}

//  Relacionadas al DMS
function renderizarCodigoInsumo($insumo = array()){
    return '090'.$insumo['insumo'];
}
//
//  FIN FUNCIONES HELPERS / CONVERSORES

/**
 * Analiza el pedido y renderiza la informacion del mismo segun la especificacion CMS
 * @param $respuesta
 * @param $pedido
 * @throws Exception si hubo algun error
 */
function añadirLineaCMS(&$respuesta,$pedido){
    $registroCMS = extraerNumeroDePedidoString($pedido);
    $registroCMS .= 'MS0000000000000';

    $fechaString = renderizarFechaString();
    $registroCMS .= $fechaString;
    $registroCMS .='00000000';


    //DESTINO
    //busqueda del destino
    $destino = PNUDDestino::find($pedido['destino']);
    if(!$destino){
        throw new Exception('No existe o no ha sido posible hallar el destino con identificador '.$pedido['destino']);
    }

    $registroCMS .= renderizarDestinoString($destino);

    $registroCMS .= '    N000000000'; //codicion de pago, gestion cobranza e importe

    $registroCMS .= renderizarTotalizacionInsumos($pedido);
    $registroCMS .= 'MSPDN  ';//cliente,tipo de pedido y grupo de mercaderia

    $registroCMS .= $fechaString;


    $registroCMS .= '00000000000000000000000000000000000    000000000000000000          00000000000000';


    $respuesta['cms']['contenido'] .= $registroCMS.FIN_DE_LINEA;
    $respuesta['cms']['totalRegistros'] += 1;
}


/**
 * Analiza el los insumos a enviar en el pedido y renderiza la informacion del mismo segun la especificacion del DMS
 * @param $respuesta
 * @param $pedido
 * @throws Exception si hubo algun error
 */
function añadirLineasDMS(&$respuesta,$pedido){
    $registrosDMS = '';

    //Se arma el 1er segmento del registro comun a todos los registros
    $registroDMS1erSegmento = extraerNumeroDePedidoString($pedido);
    $registroDMS1erSegmento .= 'MS0001';

    //Se arma el 3er segmento, comun a todos los registros
    //cantidad farm, lote, obs, usuario, fechas,despacho,..
    $registroDMS3erSegmento ='0000000000000                                                          00000000000000                  00000000';


    //se itera por cada insumo y se construye el registro
    foreach($pedido['insumos'] as $insumo){
        $registrosDMS.= $registroDMS1erSegmento;

        //Lineas propias por registro
        $registrosDMS.= renderizarCodigoInsumo($insumo);
        $registrosDMS.= '000000';    //ubicacion
        $registrosDMS.= str_pad($insumo['cantidad'], 13, '0', STR_PAD_LEFT);

        $registrosDMS.= $registroDMS3erSegmento.FIN_DE_LINEA;

        //se incrementa el totalizador
        $respuesta['dms']['totalRegistros'] += 1;
    }

    $respuesta['dms']['contenido'] .= $registrosDMS;
}


/**
 * Añade los registros totalizadores tanto al CMS como al DMS
 * @param $respuesta
 * @throws Exception si hubo algun error
 */
function añadirRegistroTotalizador(&$respuesta){
    //totalizacion a string
    $totalCMSString = str_pad($respuesta['cms']['totalRegistros'], 5, '0', STR_PAD_LEFT);
    $totalDMSString = str_pad($respuesta['dms']['totalRegistros'], 4, '0', STR_PAD_LEFT);

    //agregado al final del archivo
    $respuesta['cms']['contenido'] .= 'FIN DE ARCHIVO - CANTIDAD TOTAL DE REGISTROS: '.$totalCMSString.FIN_DE_LINEA;
    $respuesta['dms']['contenido'] .= 'FIN DE ARCHIVO - CANTIDAD TOTAL DE REGISTROS: '.$totalDMSString.FIN_DE_LINEA;
}


/**
 * Añade los nombres a los archivos
 * @param $respuesta
 * @throws Exception si hubo algun error
 */
function añadirNombresArchivos(&$respuesta){
    $fechaString = date('md'); //MMDD

    $numeroDistribucionString = str_pad($respuesta['numeroDistribucion'], 3, '0', STR_PAD_LEFT);

    //agregado a las correspondientes estructuras
    $respuesta['cms']['nombre'] .= 'CMS'.$fechaString.$numeroDistribucionString;
    $respuesta['dms']['nombre'] .= 'DMS'.$fechaString.$numeroDistribucionString;
}


/**
 * Valida la integridad del pedido previo a ser renderizado.
 * Se lanza una excepcion en caso de detectar algun caso
 * @param $pedido
 * @throws Exception si hubo algun error
 */
function validarPedido($pedido){
    // Se valida que el pedido tenga insumos definidos
    if(!key_exists('insumos',$pedido) || !$pedido['insumos']){
        throw new Exception('El pedido N°'.$pedido['numero'].' no posee insumos definidos (se encuentra vacio).');
    }
}
//
//  MAIN:   Procesamiento del Formulario
//

//response para devolver al cliente
$respuesta = [
    'error' => false
];


try{
    if(isset($_POST['pedidos']) && isset($_POST['numeroDistribucion'])){
        $respuesta['pedidos'] = $_POST['pedidos'];
        $respuesta['numeroDistribucion'] = $_POST['numeroDistribucion'];

        //Inicializacion de las estructruas auxiliares: representacion en texto plano de los datos
        $respuesta['cms'] = ['contenido'=>'', 'nombre'=>'', 'totalRegistros'=>0];
        $respuesta['dms'] = ['contenido'=>'', 'nombre'=>'', 'totalRegistros'=>0];

        foreach ($respuesta['pedidos'] as $pedido){
            validarPedido($pedido);
            añadirLineaCMS($respuesta, $pedido);
            añadirLineasDMS($respuesta, $pedido);
        }

        añadirRegistroTotalizador($respuesta);
        añadirNombresArchivos($respuesta);
    }


}
catch(Exception $exception){
    $respuesta['error'] = true;
    $respuesta['mensaje'] = 'Hubo un error al procesar los datos. Motivo: '.$exception->getMessage();
}

//DEBUG
echo json_encode($respuesta);

?>