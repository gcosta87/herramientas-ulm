<?php include_once('../core/config.php'); ?>
<?php include_once('../core/header.php'); ?>
    <div class="page-header">
        <div class="row">
            <div class="col-md-12">
                <h1 id="buttons"><i class="fa fa-file fa-fw" aria-hidden="true"></i> Generador de archivos <small>Asistente para generar archivos según la especificación!</small></h1>
            </div>
        </div>
    </div>
    <h2>Tipo de archivo</h2>
    <div class="row">
        <div class="col-md-3 col-md-offset-2">
            <h3><i class="fa fa-sign-in"></i> Ingreso</h3>
            <p>Genere un archivo de ingreso de OCA.</p>
            <p><a href="/archivosOCA/generadorRMS.php" class="btn btn-primary" role="button"> <i class="fa fa-file-text"></i> RMS</a></p>
        </div>
        <div class="col-md-3 col-md-offset-1">
            <h3><i class="fa fa-sign-out"></i> Egreso</h3>
            <p>Genere los archivos de una distribución por OCA.</p>
            <p><a href="/archivosOCA/generadorCMSDMS.php" class="btn btn-primary" role="button"><i class="fa fa-file-text"></i> CMS+DMS</a></p>
        </div>
    </div>
<?php include_once('../core/footer.php'); ?>
