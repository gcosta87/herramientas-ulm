<?php
/**
 * Pequeño servicio para hacer consultas ajax y retornar respuestas JSON
 *
 */

include_once('../core/kernel.php');
$consulta= $_GET['consulta'];
$respuesta = ['error'=>false];   //Estructura donde se almacena el resultado de la query a ejecutar

try{

    $resultado = null;
    switch ($consulta){
        case 'insumosConcretosSegunPrograma':     $programa= $_GET['programa'];
            $resultado = $db->select("
                                        select  v_i.codigo_oca as codigo_oca,
                                                v_i.nombre  as nombre,
                                                v_i.presentacion  as presentacion,
                                                v_i.proveedor  as proveedor,
                                                d.total as cantidad

                                        from 	(select * from INSUMOS_PROGRAMA where programa = $programa) ip
                                                inner join v_insumos v_i
                                                on (ip.insumo = v_i.id)
                                                inner join stock_oca_distribucion d
                                                on (v_i.codigo_oca = d.codigo_oca)

                                        order by v_i.nombre,v_i.presentacion, v_i.proveedor
                        ");
            break;
        case 'eliminarUltimoStockOCA':  $fechaStock= $_GET['fecha'];
            $resultado = $db->select("
                                       delete
                            from stock_oca_discriminado
                            where fecha = '$fechaStock'
            ");
            $resultado = $db->select("
                                       delete
                                from stock_oca
                            where fecha = '$fechaStock'
            ");
        default:
            throw new Exception('Invocacion erronea!');
            break;
    }

    $respuesta['data'] = $resultado;

}
catch(Exception $exception){
    $resultado['error'] = true;
    $resultado['mensaje'] = 'Ha habido un error. Motivo: '.$exception->getMessage();
}
echo json_encode($respuesta);
