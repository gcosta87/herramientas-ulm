<?php include_once('../core/config.php'); ?>
<?php include_once('../core/header.php'); ?>
<div class="bs-docs-section">
    <div class="page-header">
        <div class="row">
            <div class="col-md-12">
                <h1 id="buttons"><i class="fa fa-file-text fa-fw" aria-hidden="true"></i> Validador OCA <small>Validación de archivos generados manualmente!</small></h1>
            </div>
        </div>
    </div>

    <h2>Análisis <small>Se determinará si el archivo cumple con la especificación</small></h2>
    <div class="row">

        <div class="col-md-11 col-md-offset-1">
            <input id="archivo" name="archivo" type="file" class="file-loading"/>
        </div>
        <div class="col-md-offset-10 col-md-2">
            <span data-toggle="tooltip" title="Ver detalles del proceso de analisis" data-placement="top"><a nohref role="button" class="btn btn-default" data-toggle="collapse" data-target="#detallesAnalisis">Que se analizará</a></span>
        </div>
        <div class="col-md-12">
            <div id="detallesAnalisis" class="collapse">
                <h2>Detalles del Analisis <small>Los elementos que se analizan</small></h2>
                <ul>
                    <li>Terminacion de lineas conforme a sistemas Windows (CR LF).</li>
                    <li>Codificacion de caracteres (UTF8).</li>
                    <li>Registros conforme a la especificacion correspondiente:
                        <ul>
                            <li>Longitudes de linea</li>
                            <li>Tipos de campos (numericos, alfabeticos y/o alfanumericos) y sus delimitaciones (desde/hasta)</li>
                            <li>Fin de archivo expresado correctamente: expresado segun corresponda y verificando cantidades de registros</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<style>
    #resultado_observaciones ol li {
        margin-bottom: 8px;
    }

    #resultado_observaciones ol li tt {
        background-color: #e0e0e5;
        padding: 2px 4px;
        font-family: monospace, sans-serif;
    }
</style>
    <div id="resultados" class="collapse">
        <h2>Resultados del análisis</h2>

        <h3>Informacion básica</h3>
        <strong>Nombre</strong>: <span id="archivo_nombre" class="spanVariable"></span><br/>
        <strong>Tipo</strong>: <span id="archivo_tipo" class="spanVariable"></span>: <span id="archivo_longitud" class="spanVariable"></span> caracteres de longitud para el registro (<a href="#" id="archivo_link_referencia" title="Link de referencia a la página del validador de registros" data-toggle="tooltip" data-placement="top" target="_blank"><i class="fa fa-external-link"></i></a>)<br/>
        <strong>Cantidad de registros</strong>: <span id="archivo_registros" class="spanVariable"></span><br/>
        <strong>Cantidad de lineas</strong>: <span id="archivo_lineas" class="spanVariable"></span><br/>

        <h3>Resultados</h3>
        <strong>Resultado</strong>: <span id="resultado_leyenda" class="spanVariable label"></span><br/>
        <strong>Observaciones</strong>:<br/>
        <div id="resultado_observaciones"></div>
    </div>
<script>

    /**
     * Se restablecen los componentes
     */
    function restablecerComponentes(){
        //limpieza del componente
        setTimeout(function(){
            //Restablecimiento de Krajee
            $("#archivo").fileinput('refresh');
        },250);

        $('#detallesAnalisis').collapse('hide');
    }


    $(document).on('ready', function() {
        $("#archivo").fileinput({
            uploadUrl: '/archivosOCA/validarAction.php',
            maxFilePreviewSize: 10240,
            language: 'es',
            allowedFileExtensions: ['txt'],
            showCancel:false,
            showCaption: false,
            showClose:false,
            dropZoneTitle:'Arrastre algún <strong>CMS</strong>, <strong>DMS</strong> o <strong>RMS</strong>',
            browseOnZoneClick: true,
            showBrowse: false
        });

        //El usr selecciono un archivo
        $('#archivo').on('fileselect', function(event, numFiles, label) {
            //Restablecimiento de panel de resultados
            $('#resultados').collapse('hide');
            $('.spanVariable').text('');

            $('#resultado_leyenda').removeClass('label-danger');
            $('#resultado_leyenda').removeClass('label-success');

            $('#resultado_observaciones').html('');
        });

        //Una vez subido el archivo
        $("#archivo").on('fileuploaded', function(event, data, previewId, index) {
            var form = data.form, files = data.files, extra = data.extra, response = data.response, reader = data.reader;
            restablecerComponentes();

            //Seteo del panel resultados
            $('#archivo_nombre').text(response.archivo.nombre);

            $('#archivo_tipo').text(response.archivo.tipo);
            $('#archivo_longitud').text(response.archivo.longitud);
            $('#archivo_link_referencia').prop('href',response.archivo.link_referencia);

            $('#archivo_lineas').text(response.archivo.lineas);
            $('#archivo_registros').text(response.archivo.registros);

            if(response.resultado.errores == true){
                $('#resultado_leyenda').addClass('label-danger');
            }
            else{
                $('#resultado_leyenda').addClass('label-success');
            }
            $('#resultado_leyenda').text(response.resultado.leyenda);


            var observacionesHTML = '<ol>';
            response.resultado.observaciones.forEach(function(item, index){
                observacionesHTML+= '<li>' + item+ '</li>';
            });
            observacionesHTML+='</ol>';
            $('#resultado_observaciones').html(observacionesHTML);

            //Se despliega un alerta con la leyenda
            if(response.alerta){
                setTimeout(function(){
                    alertify.notify(response.alerta.mensaje,response.alerta.tipo,7);
                },3000);
            }

            //Se muestra el panel
            $('#resultados').collapse('show');
        });
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

</script>
<?php include_once('../core/footer.php'); ?>