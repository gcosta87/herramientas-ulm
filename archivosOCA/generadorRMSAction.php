<?php

const FIN_DE_LINEA = "\r\n";
/**
 * @param string $datosArchivo "string" donde se va generando el archivo
 * @return string renglon de cabecera
 */
function generarCabecera(&$datosArchivo){
    $fechaString = date('d-F-y');
    $arrMesesEnIngles=[
        'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
    ];
    $arrMesesAcortadosEspaniol=[
        'Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'
    ];

    $fechaString = str_replace($arrMesesEnIngles,$arrMesesAcortadosEspaniol, $fechaString);

    $datosArchivo = "fecha de proceso ".$fechaString.FIN_DE_LINEA;
}

/**
 * Define el renglon en base al array con todos los datos los datos requeridos. El formato esta en el "Main"
 * @param string $datosArchivo "string" donde se va generando el archivo
 * @param array $registro
 * @return string Renglon renderizado
 */
function generarRenglon(&$datosArchivo, $registro = []){
    $datosArchivo.= str_pad($registro['pedido']['numero'].$registro['pedido']['anio'],10,'0',STR_PAD_LEFT).
        '090'.$registro['insumo'].
        str_pad($registro['lote'], 15,'0',STR_PAD_LEFT).
        '00000000                  00000000                    '.
        str_pad($registro['cajas'], 13,'0',STR_PAD_LEFT).
        'DIS'.FIN_DE_LINEA;
}


/**
 * Retorna el ultimo registro del archivo (para indicar su fin).
 * En este caso esta prefijado porque es constante
 * @param string $datosArchivo "string" donde se va generando el archivo
 * @return string
 */
function generarRegistroTotalizador(&$datosArchivo){
    $datosArchivo.='****Fin del Reporte****'.FIN_DE_LINEA;
}

//
//  MAIN:   Procesamiento del Formulario
//
if(isset($_POST['pedido_numero'])){
    $registro = [];

    $registro['pedido']['numero'] = $_POST['pedido_numero'];
    $registro['pedido']['anio'] = $_POST['pedido_anio'];

    $datosArchivo = '';

    generarCabecera($datosArchivo);

    $arrInsumos = $_POST['insumo'];
    $arrCajas = $_POST['cajas'];
    for($i=0; $i<count($arrCajas); $i++){
        $registro['insumo'] = $arrInsumos[$i];
        $registro['cajas']  = $arrCajas[$i];
        $registro['lote']  = $i.''.$i.''.$i;     //Se inventa

        generarRenglon($datosArchivo,$registro);
    }
    generarRegistroTotalizador($datosArchivo);

    //Se genera la respuesta: un archivo de text
    $nombreArchivo = 'RMS'.date('md').str_pad($_POST['ingreso'],3,'0',STR_PAD_LEFT); //RMSMMDD###

    header("Content-type: text/plain");
    header("Content-Disposition: attachment; filename=$nombreArchivo.txt");
    print $datosArchivo;

}

?>