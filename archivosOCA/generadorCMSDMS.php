<?php
include_once('../core/kernel.php');
include_once('../core/config.php');
include_once('../core/header.php');
use Models\Programa;
use Models\PNUDDestino;
?>
<style xmlns="http://www.w3.org/1999/html">
    input.insumo {
        text-transform: uppercase;
    }

    div.insumo, div.cabecera{
        margin-bottom: 10px;
    }

    .btnOperacion{
        margin-left: 15px;
    }
    .pedido{
        margin-bottom: 50px;
    }
    .pedido h3.panel-title{
        margin-bottom: 10px;
    }

    .segmentos .segmento{
        padding: 20px 10px;

    }

    .segmentos .segmento .cabecera{
        margin-bottom: 15px;
    }


    .segmentos .segmento .nombre{
        color: darkgrey;
        font-size:1.3em;
        text-transform: capitalize;
        letter-spacing: 2px;
    }
</style>


    <div class="page-header">
        <div class="row">
            <div class="col-md-12">
                <h1 id="buttons"><i class="fa fa-file fa-fw" aria-hidden="true"></i> Generador de archivos <small>Asistente para generar archivos según la especificación!</small></h1>
            </div>
        </div>
    </div>
    <h2>CMS+DMS <small>Genere el par de archivos requerido para un egreso</small></h2>
    <p>Defina los distintos destinos y los insumos a enviar.</p>
    <p>Se generará el correspondiente CMS+DMS conforme la especificacion con fecha actual y no requerirá validarse.</p>
<?php
    try{
        $destinos   = PNUDDestino::where('activo','1')->orderby('nombre')->get();
        $programas  = Programa::orderby('nombre')->get();

?>
    <form class=" form-horizontal" method="POST" action="generadorCMSDMSAction.php" target="_blank" name="formulario">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Configuracion básica</h3>
            </div>
            <div class="panel-body">

                <div class="form-group">
                    <label for="configuracion_numeroDistribucion" class="col-sm-5 control-label">Número de Distribución del día</label>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" id="configuracion_numeroDistribucion" name="numeroDistribucion" placeholder="1" min="1" max="10"  required/>
                        <span class="help-block">Afecta solamente al nombrado de los archivos CMS y DMS.</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="configuracion_numeroPedido" class="col-sm-5 control-label">Numero de Pedido</label>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" id="configuracion_numeroPedido" name="numeroDePedido"placeholder="12345" min="1" max="999999"  required/>
                        <span class="help-block">Enumera de forma automática todos los pedidos que se definan.</span>
                    </div>
                </div>

            </div>
        </div>
        <div id="contenedorPedidos">

        </div>

        <div class="row">
            <div class="col-md-3">
                <a role="button" class="btn btn-success" title="Añadir un nuevo pedido a la distribucion"  data-toggle="tooltip" data-placement="top" id="btnAnadirPedido">Añadir pedido</a>
            </div>
        </div>

        <br/>
        <br/>
        <div class="row">
            <div class="col-md-3 col-md-offset-5">
                <a role="button" class="btn btn-info disabled" id="btnTotalizarInsumos" title="Brinda informacion sobre los insumos que se intentan distribuir" data-toggle="tooltip" data-placement="top"><i class="fa fa-fw fa-table"></i> Totalizar insumos</a>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-2">
                <button role="button" type="submit" class="btn btn-primary" id="btnGenerarArchivo"><i class="fa fa-fw fa-file-o"></i> Generar archivos</button>
            </div>
            <div class="col-md-3">
                <button role="button" type="reset" class="btn btn-inverse btn-sm" title="Limpia todos los pedidos definidos y restablece el formulario"  data-toggle="tooltip" data-placement="right" id="btnLimpiarFormulario">Limpiar</button>
            </div>
        </div>
    </form>
        <!-- Template -->
        <div class="panel panel-default pedido template" id="template">
            <div class="panel-heading">
                <a role="button" class="btn btn-xs btn-danger pull-right btnEliminarPedido btnOperacion" title="Elimina el pedido"><i class="fa fa-fw fa-times-circle"></i></a>
                 <!-- <a role="button" class="btn btn-xs btn-default pull-right btnCopiarPedido btnOperacion" title="Hace una copia del pedido"><i class="fa fa-fw fa-clone"></i></a> -->
                <h3 class="panel-title"> <i class="fa fa-columns fa-fw" aria-hidden="true"></i>
                    Pedido #<span class="numeroDePedido">(sin definir)</span></h3>
                    <input type="hidden" name="pedido[numeroDePedido][]" value="-1"/>
                <div class="row">

                    <div class="col-md-4">
                        <i class="fa fa-map-marker fa-fw"></i>
                        <select name="pedido[destino][]" data-placeholder="Seleccione un destino" class="select" required>
                            <option value=""></option>
                            <?php
                            foreach($destinos as $destino){
                                echo '<option value="'.$destino->id.'">'.$destino->nombre.'</option>';
                            }
                            ?>
                        </select>
                    </div>

                    <div class="col-md-4 col-md-offset-4">
                        <i class="fa fa-users fa-fw"></i>
                        <select name="pedido[programa][]" data-placeholder="Seleccione un programa" style="width:200px;" class="select">
                            <?php
                            foreach($programas as $programa){
                                echo '<option value="'.$programa->id.'">'.$programa->nombre.'</option>';
                            }
                            ?>
                        </select>
                        <a role="button" class="btn btn-sm btn-info btnAnadirSegmento" title="Añadir segmento de pedido asociado a programa">Añadir</a>
                    </div>
                </div>
            </div>
            <ul class="list-group segmentos">
                <!-- Inyeccion de segmentos -->
            </ul>
        </div>
        <!-- Fin template -->


        <!-- Template segmento-->
        <li class="list-group-item segmento template" id="templateSegmento">
            <input type="hidden" value="-1" name="pedido[programa_id][]"/>
            <div class="row">
                <div class="col-md-4 cabecera">
                    <span class="text-left nombre">Programa</span>
                    <span class="badge" title="cantidad de insumos">0</span>
                </div>
                <div class="col-md-3 col-md-offset-5">
                    <a role="button" class="btn btn-xs btn-danger pull-right btnEliminarSegmento" title="Elimina el segmento"><i class="fa fa-fw fa-times"></i></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 insumos">
                    <div class="alert alert-warning fade in"  role="alert">
                        <strong>Añada insumos a distribuir!</strong> sino elimine este segmento.
                    </div>
                    <!-- Inyeccion de renglones/insumos -->
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-md-offset-6">
                    <a role="button" class="btn btn-success btnAnadirInsumo" title="Añadir insumo a distribuir"><i class="fa fa-plus fa-fw" ></i></a>
                </div>
            </div>
        </li>
        <!-- Fin Template segmento-->


        <!-- Template Insumo -->
        <div class="row insumo template" id="templateInsumo">
            <div class="col-md-6 col-md-offset-1">
                <div class="form-group">
                    <select class="form-control insumo select" name="pedido[renglon][insumo][]" data-placeholder="Seleeccione un insumo"  required>
                    </select>
                </div>
            </div>
            <div class="col-md-1 col-md-offset-1">
                <div class="form-group">
                    <input type="number" class="form-control" name="pedido[renglon][cajas][]" placeholder="123" min="1" max="99999" required/>
                </div>
            </div>
            <div class="col-md-1 col-md-offset-1">
                <a role="button" class="btn btn-danger btnEliminarInsumo" title="Eliminar reglon" ><i class="fa fa-trash fa-fw" ></i></a>
            </div>
        </div>
        <!-- Fin Template Insumo -->

        <!-- Modal para mostrar detalles de los insumos -->
        <div class="modal fade" id="modalTotalizadorInsumos" tabindex="-1" role="dialog" aria-labelledby="modalTotalizadorInsumos">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Totalizacion de insumos</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-striped">
                            <thead><tr><th>Insumo</th><th>Apariciones <span class="fa fa-fw fa-question-circle-o" title="Cantidad de veces que el insumo ha sido utilizado para distribuir" data-placement="top" data-toggle="tooltip"></span></th><th>Cantidad distribuida <span class="fa fa-fw fa-question-circle-o" title="Cantidad distribuida sobre el total del stock" data-placement="top" data-toggle="tooltip"></span></th></tr></thead>
                            <tbody>

                            </tbody>
                            <tfoot><tr><th>Insumo</th><th>Apariciones</th><th>Cantidad total</th></tr></tfoot>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

<script>
    //Se refrescan los numeros de renglon
    function enumerarPedidos(){
        var numeroDePedido = $('#configuracion_numeroPedido').val();
        if(numeroDePedido != ''){
            var i = numeroDePedido;
            $('.pedido span.numeroDePedido').each(function(index,elemento) {

                $(elemento).text(i);
                $(elemento).parent().siblings('input[name="pedido[numeroDePedido][]"]').val(i);
                i++;
            });
        }
        else{
            $('.pedido span.numeroDePedido').text('Sin definir')
        }
    }

    $(function(){
        // EVENTOS DE PEDIDOS
        $('#btnAnadirPedido').click(function(e){
            $templateNueva = $('#template').clone(true,true);

            $templateNueva.removeAttr('id');

            $templateNueva.appendTo('#contenedorPedidos');
            $templateNueva.show();

            //Inicializar bloque de pedido
            $templateNueva.find('select').each(function(index){inicializarSelector($(this));});
            enumerarPedidos();
            
            //habilitamos el boton para totalizar
            $('#btnTotalizarInsumos').removeClass('disabled');
        });

        $('.btnCopiarPedido').click(function(e){

            var $pedidoOriginal = $(this).closest('.pedido');
/* ORIGINAL
            $pedidoCopiado = $pedidoOriginal.clone(true);
            $pedidoCopiado.appendTo('#contenedorPedidos');

            //restablecimiento de destino
            //FIXME Chosen no funciona

            $pedidoCopiado.find('select').removeClass("chzn-done").removeAttr("id").css("display", "block").next().remove();
            //$pedidoCopiado.find('select').each(function(index){$(this).chosen('destroy');});

            $pedidoCopiado.find('select').each(function(index){inicializarSelector($(this));});
            $pedidoOriginal.find('select').chosen('destroy');
            $pedidoOriginal.find('select').each(function(index){inicializarSelector($(this));});
*/

            //Alternativa
            //Se crea un nuevo pedido
            $('#btnAnadirPedido').trigger('click');
            var $pedidoNuevo =  $('#contenedorPedidos .pedido:last');

            //Se copia el o los segmentos del orignal y se inyectan al nuevo
            $pedidoNuevo.find('ul.segmentos').remove();
            var $segmentosClonados = $pedidoOriginal.find('ul.segmentos').clone(true);
            $pedidoNuevo.append($segmentosClonados);

            $segmentosClonados.find('select').each(function(index){inicializarSelector($(this));});
            enumerarPedidos();
        });

        $('.btnEliminarPedido').click(function(e) {
            $(this).closest('.pedido').remove();
            enumerarPedidos();
        });


        //  EVENTOS DE SEGMENTOS

        $('.btnAnadirSegmento').click(function(e) {
            var $pedido = $(this).closest('.pedido');
            $templateNueva = $('#templateSegmento').clone(true,true);

            $templateNueva.removeAttr('id');

            $templateNueva.appendTo($pedido.children('.segmentos'));
            $templateNueva.show();

            //seteo del id y nombre del programa al segmento
            var $programa = $(this).closest('.pedido').find('select[name="pedido[programa][]"] option:selected');

            $templateNueva.find('input[type="hidden"]').val($programa.val());
            $templateNueva.find('span.nombre').text($programa.text());

        });



        $('.btnEliminarSegmento').click(function(e) {
            $(this).closest('.segmento').remove();
        });

        $('#configuracion_numeroPedido').change(function(e){
            enumerarPedidos();
        });


        // EVENTOS DE INSUMOS/RENGLONES

        //Boton para añadir otro registro
        $('.btnAnadirInsumo').click(function(e){
            var $segmento = $(this).closest('.segmento');
            var $contenedorInsumos = $segmento.find('.insumos');

            $contenedorInsumos.find('.alert').hide();

            $templateNueva = $('#templateInsumo').clone(true,true);
            $templateNueva.removeAttr('id');
            $templateNueva.appendTo($contenedorInsumos);
            $templateNueva.show();

            var programaId = $segmento.find('input[type="hidden"]').val();
            renderizarSelectorInsumo($contenedorInsumos,programaId);

            //actualizo el contador
            $contador = $segmento.find('.badge');
            $contador.text(parseInt($contador.text()) + 1);

            //Hago foco en el input insumo recien añadido
            $contenedorInsumos.find('input[name="pedido[renglon][insumo][]"]:last').focus();
        });

        $('.btnEliminarInsumo').click(function(e){
            var $segmento = $(this).closest('.segmento');
            var $contenedorInsumos = $segmento.find('.insumos');

            // Se elimina el renglon del insumo
            $(this).closest('.insumo').remove();

            // Actualizo el contador
            $contador = $segmento.find('.badge');
            $contador.text(parseInt($contador.text()) - 1 );

            //Si no hay mas insumos, se muestra el alert
            if($contenedorInsumos.find('.insumo').length == 0){
                $contenedorInsumos.find('.alert').show();
            }
        });


        $('#btnLimpiarFormulario').click(function(e){
            document.forms[0].reset();
            $('.pedido').not('#template').remove();
        });

        /**
         * Se despliega un modal brindando informacion de los insumos a distribuir
         */
        $('#btnTotalizarInsumos').click(function(e){
            var pedidos = analizarFormulario();

            if(pedidos){
                var insumosSeleccionados = [];
                $('.insumo select option:selected').each(function(index){
                    $opcion = $(this);
                    insumosSeleccionados[$opcion.val()] = {'nombre': $opcion.text().substr(18),'cantidad':0,'ocurrencias':0 ,'stock':$opcion.data('cantidad')};
                });



                pedidos.forEach(function(pedido,index){
                    pedidos[index].insumos.forEach(function(insumo, index){
                        insumosSeleccionados[insumo.insumo].cantidad += insumo.cantidad;
                        insumosSeleccionados[insumo.insumo].ocurrencias += 1;
                    });
                });


                //Convierto el array asociativo (key=>value) a uno de valores (value), para luego ordenarlo por nombre el array por nombre
                var insumosSeleccionadosOrdenado = [];
                Object.keys(insumosSeleccionados).forEach(function(key,index){insumosSeleccionadosOrdenado.push(insumosSeleccionados[key])});

                insumosSeleccionadosOrdenado.sort(function(a,b){
                    if(a.nombre < b.nombre){
                        return -1;
                    }
                    else{
                        return 1;
                    }
                });

                var tablaHTML = '';
                insumosSeleccionadosOrdenado.forEach(function(insumoSeleccionado, index){
                    tablaHTML+='<tr><td>'+insumoSeleccionado.nombre+'</td><td>'+insumoSeleccionado.ocurrencias+'</td>';
                    tablaHTML+='<td><span class="label label-'+(insumoSeleccionado.cantidad <= insumoSeleccionado.stock ? 'success':'danger')+'">'+insumoSeleccionado.cantidad+' / '+insumoSeleccionado.stock+'</span></td></tr>';
                });

                $modal = $('#modalTotalizadorInsumos');

                $modal.find('.modal-body tbody').html(tablaHTML);
                $modal.modal('show');
            }
        });

        $('#btnGenerarArchivo').click(function(e){
            e.preventDefault();
            var pedidos = analizarFormulario(); //lanza un aviso si faltan completar datos, y devuelve null en ese caso

            if(pedidos){
                var numeroDistribucion = $('#configuracion_numeroDistribucion').val();

                $.post('/archivosOCA/generadorCMSDMSAction.php', {
                    'pedidos': pedidos,
                    'numeroDistribucion': numeroDistribucion
                })
                .done(function (data) {
                    var respuesta = JSON.parse(data);

                    if (!respuesta.error) {
                        //Pequeño bugfix ante browser que no genera los 2 archivos
                        setTimeout(function(){
                                renderizarArchivoDescargable(respuesta.cms);
                            },150
                        );

                        setTimeout(function(){
                                renderizarArchivoDescargable(respuesta.dms);
                            },600
                        );
                    }
                    else {
                        alertify.error('No se pudo cargar los insumos asociados al programa!. Motivo: ' + respuesta.mensaje);
                    }
                });
            }
        });


        /**
         * Analiza que el formulario se haya completado correctamente y genena una estructura representando la distribución.
         */
        function analizarFormulario(){
            var pedidos = null;

            //Se invoca a la validacion nativa y minima del formulario
            if($('form')[0].reportValidity()) {
                pedidos = [];
                $pedidos = $('.pedido').not('#template').each(function (index, pedidoHTML) {
                    var $pedido = $(pedidoHTML);
                    var pedido = {
                        'numero': $pedido.find('input[name="pedido[numeroDePedido][]"]').val(),
                        'destino': $pedido.find('select[name="pedido[destino][]"]').val(),
                        'insumos': []
                    };

                    $pedido.find('div.insumo').each(function (index, insumo) {
                        var $insumo = $(insumo);

                        var renglon = {
                            'insumo': $insumo.find('select').val(),
                            'cantidad': parseInt($insumo.find('input[type="number"]').val())
                        };

                        pedido.insumos.push(renglon);
                    });

                    pedidos.push(pedido);
                });


                //Validacion de destinos repetidos
                var validarDestinos = {};
                pedidos.forEach(function(pedido,index){
                    var destino = pedidos[index].destino;
                    if(!validarDestinos.hasOwnProperty(destino)){
                        validarDestinos[destino] = true;
                    }
                    else{
                        alertify.error('Se han definido destinos repetidos. Se ha detectado repetencion en el pedido N° '+pedido.numero+' !.', 15);
                        pedidos = null;
                    }
                });

                //Se retorna la coleccion satisfactoria
                return pedidos;
            }
            else{
                alertify.error('Hay campos que no fueron correctamente completados!.', 15);
            }
        }

        //
        //  Inicializaciones
        //
        $('#template').hide();
        $('#templateSegmento').hide();
        $('#templateInsumo').hide();

        $('#configuracion_numeroDistribucion').focus();

        $('[data-toggle="tooltip"]').not('.template').tooltip();
    });

    /**
     * Renderiza un link apuntando a un contenido base64 que es el "archivo" contenido en la estrucutra
     */
    function renderizarArchivoDescargable(data){
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data.contenido));
        element.setAttribute('download', data.nombre);

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);
    }

    function inicializarSelector(selector){

        if(selector instanceof String){
            selector = $(selector);
        }

        var valorOrig = selector.val();
        selector.chosen({
            no_results_text: "No hay resultados... :(",
            allow_single_deselect: true
        });
        selector.val(valorOrig);
        selector.chosen('update')
    }


    /**
     * inicializa el selector recientemente insertado en el contentedor de insumos: busca los datos correspondientes  a las opciones e incializa el chosen.
     * Adicionalmente se configura el input numerico
     *
     * @param $contenedorInsumos jQuery Object apuntado al contentedor de insumos del segmento donde se disparó el evento
     * @param programaId
     */
    function renderizarSelectorInsumo($contenedorInsumos,programaId){
        var $selector = $contenedorInsumos.find('select:last');

        $.get("/archivosOCA/consultasAjax.php?consulta=insumosConcretosSegunPrograma&programa=" + programaId, function(data, status){
            respuesta = JSON.parse(data);

            if(!respuesta.error){
                opcionesHTML = '<option value="" selected></option>';

                $.each(respuesta.data, function(index, insumo){
                    opcionesHTML += '<option value="' + insumo.codigo_oca + '" data-cantidad="'+insumo.cantidad+'">'+insumo.codigo_oca+' - ' + insumo.nombre + ' ' + insumo.presentacion + '</option>';
                });

                //renderizado del selector
                $selector.html(opcionesHTML);
                inicializarSelector($selector);

                $selector.change(function(){
                    var $inputCantidad = $(this).closest('.row').find('input[type="number"]');

                    //Si ha sido correctamente identificado se procede a actualizar su placeholder
                    if($inputCantidad){
                        var cantidadMaxima = $(this).find('option:selected').data('cantidad');
                        $inputCantidad.prop('placeholder',cantidadMaxima);
                    }
                });
            }
            else{
                alertify.error('No se pudo cargar los insumos asociados al programa!. Motivo: '+respuesta.mensaje);
            }

        });
    }

</script>
<?php
    }
    catch(Exception $e){
        echo '<h2>Se ha producido un error <small>Es un bajón!!!  <i class="fa fa-frown-o"></i></small></h2><br/><strong>Motivo</strong>: '.$e->getMessage();
    }

    include_once('../core/footer.php');
?>
