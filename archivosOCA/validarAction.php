<?php
/**
 * Script que valida los archivos de la especificacion de OCA:
 *      CMS
 *      DMS
 *      RMS (no implementado)
  */

//  CONSTANTES
//

const ENCODINGS_VALIDO = 'UTF-8';   //UTF-8 Sin BOM , si variante con BOM da error.
const ENCODINGS= ['UTF-8','ASCII', 'ISO-8859-1', 'ISO-8859-2', 'ISO-8859-3', 'ISO-8859-4','ISO-8859-5', 'ISO-8859-6', 'ISO-8859-7', 'ISO-8859-8', 'ISO-8859-9', 'ISO-8859-10','ISO-8859-13', 'ISO-8859-14', 'ISO-8859-15', 'ISO-8859-16','Windows-1251', 'Windows-1252', 'Windows-1254'];

const CABECERA_UTF8BOM = 'efbbbf';  //Los 3 primeros caracteres hexadecimales de una cabecera con BOM

//Debe terminar segun Windows (CR LF/ 0x0D 0x0A)
const REGEX_FIN_LINEA = '/\x0d\x0a$/m';





//Registro totalizador: ubicado al final del archivo, indicando la cantidad de registros declarados
const REGEX_REGISTRO_TOTALIZADOR_CMS = '/^FIN DE ARCHIVO - CANTIDAD TOTAL DE REGISTROS: [0-9]{5}$/';
const REGEX_REGISTRO_TOTALIZADOR_DMS = '/^FIN DE ARCHIVO - CANTIDAD TOTAL DE REGISTROS: [0-9]{4}$/';
const REGEX_REGISTRO_TOTALIZADOR_RMS = '/^[*]{4}Fin del Reporte[*]{4}$/';


const REGEX_REGISTRO_CABECERA_RMS = '/^fecha de proceso [0-9]{2}-(?>Ene|Feb|Mar|Abr|May|Jun|Jul|Ago|Sep|Oct|Nov|Dic)-[0-9]{2}$/';



//Logitudes de los registros
const LONGITUD_REGISTRO_CMS = 418;
const LONGITUD_REGISTRO_DMS = 170;
const LONGITUD_REGISTRO_RMS = 113;

const REGEX_REGISTRO_CMS = '/^(?=.{418}$)
(?<Nro_de_Pedido>[0-9]{16})
(?<Letra>[M]{1})
(?<Identificacion>[S]{1})
(?<Origen>[0-9]{4})
(?<Division>[0-9]{3})
(?<Tipo_Transaccion>[0-9]{6})
(?<Fecha_de_pedido>[0-9]{8})
(?<Fecha_de_aprobacion>[0-9]{8})
(?<Cod_Cliente_destinatario>[0-9]{6})
(?<Cuit_Destinatario>[0-9]{15})
(?<Razon_Social_destinatario>.{30})
(?<Localidad_Destinatario>.{30})
(?<Codigo_Postal_Destinatario>[0-9]{5})
(?<Deposito_Reparto>[0-9]{4})
(?<Prioridad>.{4})
(?<Observacion01>.{30})
(?<Observacion02>.{30})
(?<Observacion03>.{30})
(?<Observacion04>.{30})
(?<domicilio_entrega>.{30})
(?<condicion_pago>.{4})
(?<gestion_cobranza>.{1})
(?<importe_gestion_cobranza>[0-9]{9})
(?<items_pedido>[0-9]{4})
(?<Total_unidades_pedido>[0-9]{13})
(?<Grupo_cliente>.{2})
(?<tipo_pedido>.{3})
(?<grupo_mercaderia>.{2})
(?<fecha_pactada_entrega>[0-9]{8})
(?<origen_remito>[0-9]{4})
(?<numero_remito>[0-9]{8})
(?<valor_declaro_seguro>[0-9]{9})
(?<total_kilos_pedido>[0-9]{7})
(?<total_bultos>[0-9]{7})
(?<tipo_comprobante>.{4})
(?<numero_comprobante_orig>.{8})
(?<numero_guia>[0-9]{10})
(?<usuario_proceso>.{10})
(?<fecha_proceso>[0-9]{8})
(?<hora_proceso>[0-9]{6})
$/ixm';


const REGEX_REGISTRO_RMS = '/^(?=.{113}$)
(?<Nro_de_Remito>[0-9]{10})
(?<Codigo_producto>090.{15})
(?<Lote>.{15})
(?<Fecha_Vto>[0-9]{8})
(?<Codigo_Aduana>.{2})
(?<Numero_Despacho>.{16})
(?<Fecha_Despacho>[0-9]{8})
(?<Lote_importado>.{20})
(?<Cantidad>[0-9]{13})
(?<Estado>(DIS|CUA))
$/ixm';


const REGEX_REGISTRO_DMS = '/^(?=.{170}$)
(?<Nro_de_Pedido>[0-9]{16})
(?<Letra>[M]{1})
(?<Identificacion>[S]{1})
(?<Origen>[0-9]{4})
(?<Codigo_de_producto>090[0-9A-Z]{15})
(?<Ubicacion>[0-9]{6})
(?<Cantidad_pedida>[0-9]{13})
(?<Cantidad_FARM>[0-9]{13})
(?<Lote_especifico>.{15})
(?<Observaciones_lote>.{30})
(?<Tipo_de_producto>.{2})
(?<Estado_linea>.{1})
(?<Usuario_de_proceso>.{10})
(?<Fecha_de_proceso>[0-9]{8})
(?<Hora_de_proceso>[0-9]{6})
(?<Codigo_de_aduana>.{2})
(?<Numero_de_despacho>.{16})
(?<Fecha_de_nacionalizacion>[0-9]{8})
$/ixm';

//  FUNCIONES
//

/**
 * Determina el tipo de archivo OCA, y delega el analisis a la funcion correspondiente.
 * Adicionalmente setea algunos datos al response
 * @param $uploadFile
 * @param $response
 */
function analisis($uploadFile,&$response){
    $nombreArchivo = $uploadFile['name'];

    $response['archivo']['nombre']=$nombreArchivo;
    //Inicializacion de campos default a todos los tipos de archivos
    $response['resultado']['observaciones'] = [];
    $response['archivo']['lineas']  = '(no calculadas)';
    $response['archivo']['tipo'] = '(no analizado)';


    $erroresDeAnalisis = true;

    //determinacion del tipo de archivo segun el nombre del mismo
    switch(substr($nombreArchivo,0,3)){
        case 'CMS':     $erroresDeAnalisis = analizarCMS($uploadFile,$response);
                        $response['archivo']['tipo'] = 'CMS';
                        $response['archivo']['longitud'] = LONGITUD_REGISTRO_CMS;
                        $response['archivo']['link_referencia'] = 'https://regex101.com/r/UYvXDl/1';
                        break;

        case 'DMS':     $erroresDeAnalisis = analizarDMS($uploadFile,$response);
                        $response['archivo']['tipo'] = 'DMS';
                        $response['archivo']['longitud'] = LONGITUD_REGISTRO_DMS;
                        $response['archivo']['link_referencia'] = 'https://regex101.com/r/hh4TX6/1';
                        break;

        case 'RMS':     $erroresDeAnalisis = analizarRMS($uploadFile,$response);
                        $response['archivo']['tipo'] = 'RMS';
                        $response['archivo']['longitud'] = LONGITUD_REGISTRO_RMS;
                        $response['archivo']['link_referencia'] = 'https://regex101.com/r/1zEluZ/1';
                        break;

        default:        $response['error'] = '<strong>Tipo de archivo no reconocido: </strong> Solo se permiten CMS, DMS o RMS.';
    }

    return  $erroresDeAnalisis;
}

/**
 * Analiza lo minimo de un archivo: encoding y el fin de linea utilizado
 * @param $linea string,linea del archivo a analizar
 * @param $response array, linea del archivo a analizar
 * @param $linea bool, linea del archivo a analizar
 * @return bool
 */
function analisisBasico($linea, &$response, &$errores){

    $cabeceraDeArchivo = substr(bin2hex($linea),0,6); //Se obtienen los 3 primeros caracteres convertidos a hexadecimal. Si se corresponden con los de una cabecera UTF8-BOM se reporta el error
    if($cabeceraDeArchivo == CABECERA_UTF8BOM){
        $response['resultado']['observaciones'][] = 'La codificacion es inválida!. Se está usando una version más reciente  de la codificación utilizada por la especificacion. Puede solucionar esto cambiandola con el Editor de texto Notepad++ (Menu Codificacion > "Codificar en UTF8").';
        $errores =  true;
    }

    if(mb_detect_encoding($linea, ENCODINGS) != ENCODINGS_VALIDO){
        $response['resultado']['observaciones'][] = 'La codificacion es invalida!. Esperada '.ENCODINGS_VALIDO.', obtenida: '.mb_detect_encoding($linea, ENCODINGS);
        $errores =  true;
    }

    if(!preg_match(REGEX_FIN_LINEA,$linea)){
        $response['resultado']['observaciones'][] = 'El fin de linea no es correcto, debe usarse el fin de linea de Windows (CR LF).';
        $errores = true;
    }

}

/**
 * Funcion comun para varios de los archivos de la especificacion de OCA
 * @param $uploadFile archivo temporal subido
 * @param $response
 * @param $criteroio array: Array donde se establece el criterio/ configuracion a tener en cuenta para evaluar el archivo. Ejemplo:
 *      [
 *          'registro'=> [
 *              'longitud' => 100,
 *              'regex'=> '/abc/'       //regex correspondiente a un registro del archivo
*            ],
 *          'registroTotalizador' => [
 *              'regex' => '/abc/'    //regex correspondiente al ultimo registro del archivo que totaliza
 *          ],
 *          //el sig array es opcional
 *          'registroCabecera' =>[
 *              'regex' => '/abc/'  //regex
 *          ]
*       ]
 */
function analizarArchivo($uploadFile, &$response, $criterio = array()){
    $errores = false;

    $archivo = fopen($uploadFile['tmp_name'],"r");

    $linea = fgets($archivo);

    //Se analiza el archivo, y se guardan en response y errores cualquier posilbe observacion
    analisisBasico($linea,$response,$errores);

    if($errores){
        return true;
    }

    $numeroDeLinea = 0;
    $cantidadDeRegistros = 0;   //cantidad de registros independientemente de que si estan bien o no escritos
    $posicionDeRegistroTotalizador = -1;
    $registroTotalizador = '';

    $analisisDeCabecera = (array_key_exists('registroCabecera',$criterio))? true:false;

    do{
        //TODO / FIXME se borran los ultimos dos bytes de la linea: asi se sabe la longitud y se salvan las regex que fallan ante este tipo de EOL (se requieren probar modificadores,...)
        $lineaProcesable = substr($linea,0,-2);

        $numeroDeLinea++;
        if(($numeroDeLinea == 1) && $analisisDeCabecera){
            if(!preg_match($criterio['registroCabecera']['regex'],$lineaProcesable)){
                $errores = true;
                $response['resultado']['observaciones'][] = 'El registro cabecera (RMS) no coincide con el de la especificacion!.';
            }
        }
        else{
            //Analisis de registro: Si no es el "registro totalizador" se analiza
            if(!preg_match('/(FIN\s*DE\s*ARCHIVO|Fin\s*del\s*Reporte|[*]{4}|CANTIDAD\s*TOTAL\s*DE\s*REGISTROS)/i',$lineaProcesable)){
                if(!preg_match('/^\s*$/',$linea)){
                    $cantidadDeRegistros++;

                    if(!preg_match_all($criterio['registro']['regex'],$lineaProcesable )){
                        $errores = true;
                        $longitudLinea = strlen($lineaProcesable);
                        //Se analiza si tiene una longitud <> a la esperada y se aclara
                        if($longitudLinea <> $criterio['registro']['longitud']){
                            $response['resultado']['observaciones'][] = 'La linea #'. $numeroDeLinea .' no es correcta conforme con la especificacion: posee una longitud de '.$longitudLinea.' caracteres, siendo que se se espera '.$criterio['registro']['longitud'].'!.<br/><tt title="fragmento de linea #'.$cantidadDeRegistros.' (longitud de linea '. $longitudLinea.')">'.substr($lineaProcesable,0,150).'...</tt>';
                        }
                        else{
                            $response['resultado']['observaciones'][] = 'La linea #'. $numeroDeLinea .' no es correcta conforme con la especificacion!.<br/><tt title="fragmento de linea #'.$cantidadDeRegistros.' (longitud de linea '. $longitudLinea.')">'.substr($lineaProcesable,0,150).'...</tt>';
                        }
                    }
                }
                else{
                    $response['resultado']['observaciones'][] = 'La linea #'. $numeroDeLinea .' está vacia y/o posee espacios en blanco!.';
                    $errores = true;
                }
            }
            else{
                //Se cosidera la presencia del registro totalizador
                $posicionDeRegistroTotalizador = $numeroDeLinea;
                $registroTotalizador  = $lineaProcesable;
                //Si es registro totalizador se espera que concuerde con la especificacion
                if(!preg_match($criterio['registroTotalizador']['regex'], $registroTotalizador)){
                    $errores = true;
                    $response['resultado']['observaciones'][] = 'El registro totalizador (CMS/DMS) o "Fin de reporte" (RMS) no coincide con el de la especificacion.';
                }
            }
        }

        $linea = fgets($archivo);
    } while (!feof($archivo));

    fclose($archivo);

    //Se valida que efectivamente el registro totalizador haya existido y que haya sido la ultima linea del archivo
    //Existio?
    if($posicionDeRegistroTotalizador == -1){
        $errores = true;
        $response['resultado']['observaciones'][] = 'Falta el registro totalizador (CMS/DMS) o "Fin de reporte" (RMS).';
    }
    else{
        if($posicionDeRegistroTotalizador != $numeroDeLinea){
            $errores = true;
            $response['resultado']['observaciones'][] = 'El registro totalizador (CMS/DMS) o "Fin de reporte" (RMS) no ha sido ubicado en la ultima linea!.';
        }
        else{
            //Hay que controlar cantidades?
            if($criterio['registroTotalizador']['controlDeCantidades']){
                preg_match('/[0]*([0-9]+)/',$registroTotalizador,$match);   //TODO que la regex correspondiente tenga un grupo de captura :P
                $cantidadTotalizada = intval($match[1]);
                if(($cantidadTotalizada <> ($cantidadDeRegistros))){
                    $errores = true;
                    $response['resultado']['observaciones'][] = 'El registro totalizador ("FIN DE ARCHIVO - ....") indica una cantidad de registros erronea: totalizados (por el Sistema) '.$cantidadDeRegistros.', indicados '.$cantidadTotalizada;

                    $response['alerta']['tipo']     ='default';
                    $response['alerta']['mensaje']  ='Flaco, no es mi culpa que no sepas sumar!. :P';
                }
            }
        }
    }

    $response['archivo']['lineas'] = $numeroDeLinea;
    $response['archivo']['registros'] = $cantidadDeRegistros;

    return $errores;
}


/**
 * Analiza un CMS
 * @param $uploadFile
 * @param $response
 * @return boolean true si posee errores, false caso contrario
 */
function analizarCMS($uploadFile, &$response){
    $criterio = [
        'registro'=> [
            'longitud'  => LONGITUD_REGISTRO_CMS,
            'regex'     => REGEX_REGISTRO_CMS
        ],
        'registroTotalizador' => [
            'regex'                 =>  REGEX_REGISTRO_TOTALIZADOR_CMS,
            'controlDeCantidades'   =>  true
        ]
    ];

   return analizarArchivo($uploadFile, $response, $criterio);
}

/**
 * Analiza un CMS
 * @param $uploadFile
 * @param $response
 * @return boolean true si posee errores, false caso contrario
 */
function analizarDMS($uploadFile, &$response){
    $criterio = [
        'registro'=> [
            'longitud'  => LONGITUD_REGISTRO_DMS,
            'regex'     => REGEX_REGISTRO_DMS
        ],
        'registroTotalizador' => [
            'regex'                 =>  REGEX_REGISTRO_TOTALIZADOR_DMS,
            'controlDeCantidades'   =>  true
        ]
    ];

    return analizarArchivo($uploadFile, $response, $criterio);
}

/**
 * Analiza un CMS
 * @param $uploadFile
 * @param $response
 * @return boolean true si posee errores, false caso contrario
 */
function analizarRMS($uploadFile, &$response){
    $criterio = [
        'registro'=> [
            'longitud'  => LONGITUD_REGISTRO_RMS,
            'regex'     => REGEX_REGISTRO_RMS
        ],
        'registroTotalizador' => [
            'regex'                 =>  REGEX_REGISTRO_TOTALIZADOR_RMS,
            'controlDeCantidades'   =>  false
        ],
        'registroCabecera' => [
            'regex'   =>  REGEX_REGISTRO_CABECERA_RMS
        ]
    ];

    return analizarArchivo($uploadFile, $response, $criterio);
}


/**
 * Tareas generales a llevar a cabo trás el analisis
 * @param $uploadFile
 */
function postAnalisis($uploadFile,&$response, $erroresDeAnalisis){
    //Si hubo errores se formatea el response
    if($erroresDeAnalisis){
        $response['resultado']['errores']=true;
        $response['resultado']['leyenda']='Erroneo';
    }
    else{
        $response['resultado']['errores']=false;
        $response['resultado']['leyenda']='Correcto';
    }

    //Eliminacion del archivo temporal
    unlink($uploadFile['tmp_name']);
}


//  MAIN
//

//Se carga el archivo enviado por el usuario
$uploadFile = $_FILES['archivo'];

$response = [
    'error' => '',  //Utilizado para enviar mensajes al FileUploader Krajee: se muestra al usuario dentro de la caja de Drag&Drop

    'mensaje' => '',    // Uso interno
];
//Se analiza el tipo de Archivo y se analiza segun su caso


$erroresDeAnalisis = analisis($uploadFile,$response);
postAnalisis($uploadFile,$response,$erroresDeAnalisis);

echo json_encode($response);
?>