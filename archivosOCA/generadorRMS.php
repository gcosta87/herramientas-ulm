<?php
include_once('../core/kernel.php');
include_once('../core/config.php');
include_once('../core/header.php');
use Models\Insumo;
?>
<style>
    input.insumo {
        text-transform: uppercase;
    }

    div.renglon, div.cabecera{
        margin-bottom: 25px;
    }

    span.numeroRenglon{
        color: #ccced2;
        letter-spacing: 3px;
        font-size:1.6em;
    }
</style>


    <div class="page-header">
        <div class="row">
            <div class="col-md-12">
                <h1 id="buttons"><i class="fa fa-file fa-fw" aria-hidden="true"></i> Generador de archivos <small>Asistente para generar archivos según la especificación!</small></h1>
            </div>
        </div>
    </div>

    <div class="alert alert-info alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4>Puede resultarte util...</h4>
        <p>Consultar la nomina de insumos para buscar codigos o bien utilizar el asistente para cuando se requiere "inventar" uno nuevo (ante cambios de presentación,etc..)</p>
        <p style="text-align: center;">
            <a role="button" class="btn btn-default btn-sm" target="_blank" href="/stockoca/nomina.php">Consultar nomina</a>
            <a role="button" class="btn btn-primary btn-sm"  style="margin-left: 50px;" target="_blank" href="/extrasOCA/asistenteDeCodigos.php">Usar asistente de Códigos PNUD</a>
        </p>
    </div>

    <h2>RMS <small>Genere un archivo de ingreso</small></h2>
    <p>Defina los distintos renglones del archivo junto con su numero de remito/año.</p>
    <p>Se generará un RMS conforme la especificacion con fecha actual y no requerirá validarse.</p>
    <form class="form-inline" method="POST" action="generadorRMSAction.php" target="_blank" name="formulario">
        <div class="row cabecera">
            <div class="col-md-3 col-md-offset-8">
                <div class="form-group">
                    # <input type="number" class="form-control"  name="ingreso" min="1" max="50" placeholder="1" data-toggle="tooltip" data-placement="top"  title="Numero de ingreso del día (afecta al nombre del archivo)" required/>
                </div>
            </div>
        </div>
        <div class="row cabecera">
            <div class="col-md-3 col-md-offset-8">
                <div class="form-group">
                    <input type="number" class="form-control"  name="pedido_numero" min="1" max="99999" placeholder="1234" data-toggle="tooltip" data-placement="left"  title="N° de remito (OCF)"  required/> /
                    <input type="number" class="form-control"  name="pedido_anio" min="18" max="19" value="18" required/>
                </div>
            </div>
        </div>
        <hr style="margin-bottom: 25px;"/>
        <div id="renglones">
            <div class="row renglon">
                <div class="col-md-1">
                    <span class="numeroRenglon">#<span class="numeracion"></span></span>
                </div>
                <div class="col-md-6">
                        <input class="ms-insumo-selector form-control"  name="insumo[]" style="width: 100%;" />
                </div>
                <div class="col-md-1 col-md-offset-1">
                    <div class="form-group">
                        <input type="number" class="form-control" name="cajas[]" placeholder="2500" min="1" max="99999" title="Cantidad de cajas" required/>
                    </div>
                </div>
                <div class="col-md-2 col-md-offset-1">
                    <button role="button" class="btn btn-danger" title="Eliminar reglon"  data-toggle="tooltip" data-placement="top"><i class="fa fa-trash fa-fw" ></i></button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-md-offset-6">
                <button role="button" class="btn btn-success" title="Añadir otro renglon de ingreso"  data-toggle="tooltip" data-placement="right"><i class="fa fa-plus fa-fw" ></i></button>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-2">
                <button role="button" type="submit" class="btn btn-primary" id="btnGenerarArchivo">Generar archivo</button>
            </div>
            <div class="col-md-3">
                <button role="button" type="reset" class="btn btn-inverse btn-sm" data-toggle="tooltip" data-placement="right" title="Limpia todos los datos ingresados" onclick="limpiar()">Limpiar</button>
            </div>
        </div>
    </form>

<?php
    $insumosOCASugeridos = $db->select('
        select	codigo_oca as codigoOCA, nombre as insumo, presentacion as presentacion, proveedor as proveedor
        from	v_insumos v_i
        order by	nombre, presentacion
    ');
?>

<script src="/assets/MagicSuggest/magicsuggest-min.js"></script>
<script>
    //  Variables Globales
    //
    var cantidadDeRegistros = 1;

    //  HTML requerido para insertar un nuevo renglon
    var codigoHTMLRenglon =
    '        <div class="row renglon">       '+
    '            <div class="col-md-1">     '+
    '                <span class="numeroRenglon">#<span class="numeracion"></span></span> '+
    '            </div>        '+
    '            <div class="col-md-6">      '+
    '                    <input class="ms-insumo-selector form-control"  name="insumo[]" style="width: 100%;"  />'+

    '            </div>'+
    '            <div class="col-md-1 col-md-offset-1">'+
    '                <div class="form-group">'+
    '                    <input type="number" class="form-control" name="cajas[]" placeholder="2500" min="1" max="99999" required/>'+
    '                </div>'+
    '            </div>'+
    '            <div class="col-md-2 col-md-offset-1">'+
    '                <button role="button" class="btn btn-danger" title="Eliminar reglon"  data-toggle="tooltip" data-placement="top"><i class="fa fa-trash fa-fw" ></i></button>'+
    '            </div>'+
    '        </div>';

    //Se refrescan los numeros de renglon
    function enumerarRenglones(){
        var i =1;
        $('span.numeroRenglon .numeracion').each(function(index,elemento) {
            $(elemento).text(i++);
        });
    }

    function refrescarComponentes(){
        //Se re-inicializan los tooltips
        $('[data-toggle="tooltip"]').tooltip();

        //Boton para eliminar el registro actual
        $('button.btn-danger').unbind('click');
        $('button.btn-danger').click(function(e){
            e.preventDefault();

            // Se elimina el registro actual salvo que sea el unico
            if(cantidadDeRegistros > 1){
                $(this).closest('.renglon').remove();
                cantidadDeRegistros--;
                enumerarRenglones();
            }
            else{
                alertify.notify('Che flaco, es el único registro...<br/>¿Qué querés borrar? :D','warning',6);
            }
        });
        enumerarRenglones();
    }


    function inicializarSeleccionadorInsumos(selector){
        var $ms = $(selector).magicSuggest({
            data: <?php echo json_encode($insumosOCASugeridos);?>,
            valueField: 'codigoOCA',
            displayField: 'insumo',
            groupBy: 'insumo',
            renderer: function(data){
                return '<div class="ms-insumo">' +
                    '<div class="name">' + data.insumo + '( '+data.codigoOCA+')</div>' +
                    '<div style="clear:both;"></div>' +
                    '<div class="prop">' +
                    '<div class="lbl">Presentacion : </div>' +
                    '<div class="val">' + data.presentacion + '</div>' +
                    '</div>' +
                    '<div class="prop">' +
                    '<div class="lbl">Proveedor : </div>' +
                    '<div class="val">' + data.proveedor + '</div>' +
                    '</div>' +
                    '<div style="clear:both;"></div>' +
                    '</div>';
            },
            selectionRenderer: function(data){
                return data.insumo +' ('+data.codigoOCA +')';
            },
            maxSelection: 1,    //Maxima seleccion de elementos o introduccion de datos
            maxSelectionRenderer: function(v){
                return 'Solo es posible seleccionar un insumo';
            },

            minChars:3,         //Cantidad minima de caracteres para desplegar las sugerencias
            minCharsRenderer: function(v){
                return 'Ingrese un par de caracteres para buscar';
            },
            noSuggestionText: 'No hay insumos que concuerden con la busqueda :(',
            placeholder: 'Busque un insumo o tipee un Codigo OCA',
            required: true
        });

        //Validacion via evento dado que la validacion (vregex) via config posee restricciones
        $($ms).on('selectionchange', function(){
            if(this.getSelection().length > 0){
                if(! this.getSelection()[0].codigoOCA.match(/[A-Z0-9]{15}/)){
                    alertify.error("El codigo de OCA es invalido. Se espera que sea escrito con mayúsculas y que posea 15 caracteres.", 15);
                }
            }

        });
    }

    $(function(){
        //Boton para añadir otro registro
        $('button.btn-success').click(function(e){
            e.preventDefault();

            $('#renglones').append(codigoHTMLRenglon);

            inicializarSeleccionadorInsumos('.ms-insumo-selector');
            cantidadDeRegistros++;

            refrescarComponentes();

            //Hago foco en el input insumo recien añadido
            $('#renglones .renglon input[name="insumo[]"]:last').focus();
        });

        refrescarComponentes();

        inicializarSeleccionadorInsumos('.ms-insumo-selector');

        $('input[name="ingreso"]').focus();
    });

    /**
     * Limpia/resetea el formulario
     */
    function limpiar(){
        document.forms[0].reset();

        //reset de magicsuggest
        //FIXME
        $('.ms-insumo-selector');



    }
</script>
<?php include_once('../core/footer.php'); ?>
