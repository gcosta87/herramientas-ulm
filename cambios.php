<?php include_once('core/config.php'); ?>
<?php include_once('core/header.php'); ?>
    <div class="page-header">
        <div class="row">
            <div class="col-md-12">
                <h1 id="buttons"><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i> Cambios en el Sistema <small>Los cambios aplicados al sistema</small></h1>
            </div>
        </div>
    </div>
    <h2>Historial de cambios <small>Basado en el registro del <a href="https://gitlab.com/gcosta87/herramientas-ulm" target="_blank">repositorio de la nube</a></small></h2>
<p class="lead">Se muestran los últimos 50 cambios registrados que se aplicaron al servidor que actualmente se está accediendo.</p>

<?php
    $gitLog = shell_exec('git log --decorate=short --no-merges  --date=short -n 50');
    $gitBranch = shell_exec('git rev-parse --abbrev-ref HEAD ');   //obtiene el nombre de la rama a la cual se esta apuntando
if($gitLog && $gitBranch){
    $gitlogFormateado = $gitLog;
    $gitBranch = trim($gitBranch);

    //Se limpia lo mostrado por el log
    //Eliminacion del detalle del numero de commmit
    $gitlogFormateado = preg_replace('/commit [abcdef0-9]+ \([^\)]+\)/','', $gitlogFormateado);
    $gitlogFormateado = preg_replace('/commit [abcdef0-9]+/','', $gitlogFormateado);
    $gitlogFormateado = preg_replace('/Author: .*/','', $gitlogFormateado);
    $gitlogFormateado = preg_replace('/Date:\s+([0-9-]+)/','</pre></dd></dl></div><div class="gitLogHistorico"><dl><dt>Fecha</dt><dd>\1</dd><dt>Comentarios</dt><dd><pre>', $gitlogFormateado);
    $gitlogFormateado = preg_replace('/\n\n\n/','', $gitlogFormateado);
    $gitlogFormateado = preg_replace('/<pre>\n\n/','<pre>', $gitlogFormateado);

    //Se averigua si hay cambios sin commitear, y por ende haya posibles nuevas funcionalidades
    $cambiosSinCommitear = shell_exec('git status --porcelain --branch');
    if (substr_count( $cambiosSinCommitear, "\n" ) > 1){    //Si hay cambios se retornan 2 o + lineas
        echo '
    <div class="alert alert-info fade in" role="alert">
        <strong>El servidor posee cambios sin registrar!</strong><br/>
       Es posible que haya nuevas funcionalidades o cambios que aun no han sido «registrados formalmente» en el repositorio de la nube.
      </div>
      ';
    }

    echo '<p>Se está trabajando con los cambios recibidos de la rama <span class="label label-'.(($gitBranch == 'master')? 'success':'warning').'">'.$gitBranch.'</span></p>';
    //Se elimina el primer bloque vacio de gitligHistorico
    echo substr($gitlogFormateado,24);
}
else{
    echo '
    <div class="alert alert-warning alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4>No se pudo recuparar el historial de cambios!</h4>
        <p>Esta situacion debe ser reportada para que sea solucionada.</p>
      </div>
      ';
}
?>
    </div>
<?php include_once('core/footer.php'); ?>
