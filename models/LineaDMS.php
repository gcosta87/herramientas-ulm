<?php
namespace Models;

use Illuminate\Database\Eloquent\Model;

class LineaDMS extends Model{
    protected $table = 'LINEA_DMS';
    protected $fillable = ['codigo_oca','cantidad','fecha_archivo','referencia'];
    public $timestamps = false;


    public final function insumo(){
        return $this->belongsTo('Models\Insumo','codigo_oca','codigo_oca');
    }
}