<?php
namespace Models;

use Illuminate\Database\Eloquent\Model;

class PNUDInsumo extends Model{
    protected $table = 'pnud_insumos';
    protected $fillable = ['nombre_oca','nombre_ft','codigo_oca_corto','stock_mensual_unidades'];
    public $timestamps = false;


    public final function programas(){
       return $this->belongsToMany('Models\Programa', 'INSUMOSGENERICOS_PROGRAMA', 'pnud_insumo', 'programa');
    }

    public final function insumos(){
        return $this->hasMany('Models\Insumo', 'pnud_insumo', 'id');
    }

    public function __toString(){
        return $this->nombre_oca;
    }
}