<?php
namespace Models;

use Illuminate\Database\Eloquent\Model;

class Insumo extends Model{
    protected $table = 'insumos';
    protected $fillable = ['codigo_oca', 'codigo_oca_corto', 'presentacion', 'proveedor', 'pnud_insumo'];
    public $timestamps = false;


    public final function PNUDpresentacion(){
        return $this->belongsTo('Models\PNUDPresentacion', 'presentacion','id');
    }

    public final function PNUDproveedor(){
        return $this->belongsTo('Models\PNUDProveedor', 'proveedor', 'id');
    }

    public final function PNUDInsumo(){
        return $this->belongsTo('Models\PNUDInsumo', 'pnud_insumo', 'id');
    }

    public final function stockOCA(){
        return $this->hasMany('Models\StockOCA', 'codigo_oca', 'codigo_oca');
    }

    public final function programas(){
        return $this->belongsToMany('Models\Programa', 'INSUMOS_PROGRAMA', 'insumo', 'programa');
    }



    public final function lineasDMS(){
        return $this->hasMany('Models\LineaDMS','codigo_oca','codigo_oca');
    }


    public final function lineasRMS(){
        return $this->hasMany('Models\LineaRMS','codigo_oca','codigo_oca');
    }



    public final function __toString()    {
        return $this->codigo_oca;
    }
}