<?php
namespace Models;

use Illuminate\Database\Eloquent\Model;

class StockOCADistribucion extends Model{
    protected $table = 'stock_oca_distribucion';
    protected $fillable = ['codigo_oca','total'];
    public $timestamps = false;
    protected $primaryKey = 'codigo_oca';



    public final function insumos(){
        return $this->belongsTo('Models\Insumo', 'codigo_oca', 'codigo_oca');
    }

    public final function __toString(){
        return $this->codigo_oca .'('.$this->total.')';
    }

}