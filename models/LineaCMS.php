<?php
namespace Models;

use Illuminate\Database\Eloquent\Model;

class LineaCMS extends Model{
    protected $table = 'LINEA_CMS';
    protected $fillable = ['pedido_numero','destino','fecha_archivo','referencia'];
    public $timestamps = false;


    public final function destino(){
        return $this->belongsTo('Models\PNUDDestino','destino','id');
    }

}