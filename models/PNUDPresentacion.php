<?php
namespace Models;

use Illuminate\Database\Eloquent\Model;

class PNUDPresentacion extends Model{
    protected $table = 'pnud_presentaciones';
    protected $fillable = ['nombre','multiplicador'];
    public $timestamps = false;


    public final function insumos(){
        return $this->hasMany('Models\Insumo', 'presentacion', 'id');
    }

    public final function __toString(){
        return $this->nombre ;
    }
}