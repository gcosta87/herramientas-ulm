<?php
namespace Models;

use Illuminate\Database\Eloquent\Model;

class Programa extends Model{
    protected $table = 'PROGRAMAS';
    protected $fillable = ['nombre'];
    public $timestamps = false;

    public final function PNUDinsumos(){
        return $this->belongsToMany('Models\PNUDInsumo', 'INSUMOSGENERICOS_PROGRAMA', 'programa', 'pnud_insumo');
    }

    public final function insumos(){
        return $this->belongsToMany('Models\Insumo', 'INSUMOS_PROGRAMA', 'programa', 'insumo');
    }


    public final function __toString(){
        return $this->nombre;
    }
}