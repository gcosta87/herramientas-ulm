<?php
namespace Models;

use Illuminate\Database\Eloquent\Model;

class StockOCA extends Model{
    protected $table = 'stock_oca';
    protected $fillable = ['codigo_oca','total','fecha'];
    public $timestamps = false;



    public final function insumos(){
        //->belongsTo('Entidad', 'local_fk', 'id')
        return $this->belongsTo('Models\Insumo', 'codigo_oca', 'codigo_oca');
    }


    public final function __toString(){
        return $this->codigo_oca .'('.$this->total.')';
    }

}