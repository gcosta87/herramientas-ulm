<?php
namespace Models;

use Illuminate\Database\Eloquent\Model;

class PNUDDestino extends Model{
    protected $table = 'pnud_destinos';
    protected $fillable = ['nombre','nombre_corto','localidad','nombre_ft','activo','direccion'];
    public $timestamps = false;

    public final function lineasCMS(){
        return $this->hasMany('Models\LineaCMS','destino','id');
    }


    public final function __toString(){
        return $this->nombre.'('.$this->localidad.')';
    }


    public final function isActivo(){
        return ($this->activo == true);
    }
}