<?php
namespace Models;

use Illuminate\Database\Eloquent\Model;

class PNUDProveedor extends Model{
    protected $table = 'pnud_proveedores';
    protected $fillable = ['nombre'];
    public $timestamps = false;


    public final function insumos(){
        return $this->hasMany('Models\Insumos', 'proveedor', 'id');
    }


    public final function __toString(){
        return $this->nombre;
    }
}