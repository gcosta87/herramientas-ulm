#/bin/bash
# Procesa los CMS,segun el rango de fechas, y formatea el resultado en un formato compatible con CSV

#	VARIABLES/CONSTANTES
#
#Formatos de fehcas AAAAMMDD
FECHA_DESDE='20170207';	
FECHA_HASTA='20170312'; 

PATH_ARCHIVOS='/c/Users/Usuario/Documents/trabajo/2017/PNUD-OCA';
#A�O actual para definir el campo fecha_archivo
ANO_ACTUAL='2017';


#	MAIN
#
cd "$PATH_ARCHIVOS";

#Cabecera del  archivo
echo "pedido_numero,destino,fecha_archivo,referencia";

#find . -name "CMS*" -type f -newermt $FECHA_DESDE \! -newermt $FECHA_HASTA | while read archivo;
find . -name "CMS*" -type f | while read archivo;
do
	#Se obtiene el campo referencia (MMDD###)
	referencia=`basename "$archivo"  | cut -c4-10`;

	# Se obtiene la fecha en formato legible en funcion de la referencia
	fecha_archivo=`echo "$referencia" | sed -r -e "s:^([0-9]{2})([0-9]{2}).*:$ANO_ACTUAL-\1-\2:g"`;

	datosRenglon=`grep -E '^[0-9]' "$archivo" | cut -c1-16,48-53 | sed -r -e "s:^([0-9]{16}):\1,:" | sed -r -e 's:^0+::' -e 's:,0+:,:' | sed -r -e "s:$:,$fecha_archivo,$referencia:g"`;
	echo "$datosRenglon";
done