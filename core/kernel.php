<?php
require '../vendor/autoload.php';
require 'db.php';

use \Illuminate\Container\Container as Container;
use \Illuminate\Support\Facades\Facade as Facade;

/**
 * Parche para darle un contexto requerido por Eloquent
*/
$app = new Container();
$app->singleton('app', 'Illuminate\Container\Container');
Facade::setFacadeApplication($app);

/***
 * Instancia de la BD para ejecutar querys crudas de SQL
 */
$db = $capsule->getConnection();