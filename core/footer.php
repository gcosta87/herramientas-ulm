    </div> <!-- fin contenedor -->
    <footer>
        <div class="row">
            <div class="col-sm-offset-2 col-sm-6 col-md-offset-6 col-md-4 text-right" >
                <p style="font-size:110%;" class="text-success">
                    <strong>Herramientas ULM</strong><br/>
                    <i>Herramientas para el equipo de la ULM</i><br/>
                    <a href="https://gitlab.com/gcosta87/herramientas-ulm" target="_blank"><small>es Software Libre, podés mejoralo! <i class="fa fa-code"></i> <i class="fa fa-code-fork"></i> <i class="fa fa-gitlab"></i></small></a><br/><br/>
                </p>
            </div>
            <div class="col-sm-4 col-md-2 text-left text-primary" style="font-size: 85%;color: #1c94c4">
                <t><strong>Dirección Provincial de Programas Sanitarios</strong><br/>
                    <small>Ministerio de Salud de Bs As</small></t>
            </div>
        </div>
    </footer>
</div> <!-- fin container -->
<!-- bootstrap -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- Choseen -->
<script src="/assets/chosen/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select'           : {allow_single_deselect:true,no_results_text:'No hubo coincidencias! :('},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
        '.chosen-select-width'     : {width:"95%"}
    };

    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
</body>
</html>
