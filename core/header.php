<?php include_once('config.php'); ?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Herramientas ULM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link rel='shortcut icon' type='image/png' href='/assets/favicion.png' />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="screen"/>
    <link rel="stylesheet" href="/assets/Flatly/bootstrap.min.css"/>
    <link rel="stylesheet" href="/assets/font-awesome/css/font-awesome.min.css"/>

    <link rel="stylesheet" href="/assets/DataTables/DataTables-1.10.13/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" href="/assets/DataTables/Buttons-1.2.4/css/buttons.bootstrap.min.css"/>

    <!-- Choosen -->
    <link rel="stylesheet" href="/assets/chosen/chosen.css" />

    <!-- Alertify -->
    <link rel="stylesheet" href="/assets/alertifyjs/css/alertify.min.css" />
    <link rel="stylesheet" href="/assets/alertifyjs/css/themes/bootstrap.min.css" />
    <script src="/assets/alertifyjs/alertify.min.js"></script>

    <link rel="stylesheet" href="/assets/estilo.css"/>

    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="/assets/DataTables/DataTables-1.10.13/js/jquery.dataTables.min.js"></script>
    <script src="/assets/DataTables/Buttons-1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="/assets/DataTables/Buttons-1.2.4/js/buttons.bootstrap.min.js"></script>
    <script src="/assets/DataTables/Buttons-1.2.4/js/buttons.html5.min.js"></script>
    <script src="/assets/DataTables/Buttons-1.2.4/js/buttons.print.min.js"></script>

<!-- Krajee FileInput -->
    <link href="/assets/krajeeFileInput/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
    <!-- <link href="/assets/krajeeFileInput/themes/explorer/theme.css" media="all" rel="stylesheet" type="text/css"/> -->
    <script src="/assets/krajeeFileInput/js/fileinput.js" type="text/javascript"></script>
    <script src="/assets/krajeeFileInput/js/locales/es.js" type="text/javascript"></script>
<!-- FIN Krajee FileInput -->
<!-- MagicSuggest -->
    <link href="/assets/MagicSuggest/magicsuggest-min.css" rel="stylesheet">

  </head>
  <body>
    <div class="navbar navbar-default navbar-fixed-top navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <a href="/" class="navbar-brand">Herramientas ULM</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Stock OCA <span class="caret"></span></a>
              <ul class="dropdown-menu">
                 <li><a href="/stockoca/" title="Reporte del Stock en OCA"><i class="fa fa-tasks fa-fw" aria-hidden="true"></i>Stock en OCA</a></li>
                 <li class="disabled"><a href="/stockoca/stockulm.php" title="Stock calculado de la ULM sobre el depósito de OCA"><i class="fa fa-list-ul fa-fw" aria-hidden="true"></i>Stock ULM</a></li>
                 <li><a href="/stockoca/stockdistribucion.php" title="Stock en OCA utilizado para la distribución"><i class="fa fa-tasks fa-fw" aria-hidden="true"></i>Stock Distribución </a></li>
                 <li role="separator" class="divider"></li>
                 <li><a href="/stockoca/nomina.php" title="Nomina de insumos"><i class="fa fa-product-hunt fa-fw" aria-hidden="true"></i>Nomina de insumos</a></li>
                 <li role="separator" class="divider"></li>
                 <li><a href="/stockoca/destinos.php" title="Gestión de los distintos destinos"><i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>Destinos</a></li>
                 <li><a href="/stockoca/insumos.php" title="Gestión de los distintos Insumos genericos"><i class="fa fa-medkit fa-fw" aria-hidden="true"></i>Insumos</a></li>
                 <li><a href="/stockoca/presentaciones.php" title="Gestión de las distintas presentaciones"><i class="fa fa-briefcase fa-fw" aria-hidden="true"></i>Presentaciones</a></li>
                 <li><a href="/stockoca/programas.php" title="Gestión de los distintos programas"><i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>Programas</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Reportes <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="/programas/" title="Reportes estadísticos de los diversos programas"><i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>Programas</a></li>
                <li><a href="/destinos" title="Reportes estadísticos de los destinos"><i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>Destinos</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/reportes/vencimientos.php" title="Reportes sobre lotes próximos a vencer"><i class="fa fa-exclamation-triangle fa-fw" aria-hidden="true"></i>Reporte de Vencimientos</a></li>
                <li><a href="/reportes/vencimientosSimplificado.php" title="Reportes sobre insumos próximos a vencer"><i class="fa fa-exclamation-triangle fa-fw" aria-hidden="true"></i>Reporte de Vencimientos Simplificado</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/reportes/stockOCASimplificado.php" title="Reporte simplificado del Stock en OCA"><i class="fa fa-tasks fa-fw" aria-hidden="true"></i>Stock en OCA Simplificado</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Archivos OCA <span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="themes">
                <li><a href="/archivosOCA/validar.php" title="Valide archivos de OCA generados manualmente"><i class="fa fa-file-text fa-fw" aria-hidden="true"></i> Validador OCA</a></li>
                <li><a href="/archivosOCA/validarCMSDMS.php" title="Valide el par CMS+DMS"><i class="fa fa-check-square fa-fw" aria-hidden="true"></i> Validador CMS+DMS</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/archivosOCA/interpretarMMS.php" title="Interprete y analice el contenido de un MMS"><i class="fa fa-file-text fa-fw" aria-hidden="true"></i> Interprete de MMS</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/archivosOCA/generador.php" title="Generador de Archivos OCA"><i class="fa fa-file fa-fw" aria-hidden="true"></i> Generador de archivos</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/extrasOCA/asistenteDeCodigos.php" title="Asistente para crear y decodificar codigos PNUD"><i class="fa fa-magic fa-fw" aria-hidden="true"></i> Asistente de Codigos PNUD</a></li>
              </ul>
            </li>
          </ul>


          <ul class="nav navbar-nav" style="margin-left:10px; border-left:2px solid rgba(255,255,255,.5);">
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Melchor Romero<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="/melchorRomero/stock.php" title="Ver el Stock de Melchor Romero"><i class="fa fa-tasks fa-fw" aria-hidden="true"></i> Stock</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/melchorRomero/cambiosEnInventario.php" title="Analice y compare los cambios en los inventarios de Stock"><i class="fa fa-search fa-fw" aria-hidden="true"></i> Cambios en Inventarios</a></li>
              </ul>
            </li>
          </ul>


            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes"> <i class="fa fa-cogs fa-fw" title="Sistema"></i> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="https://gitlab.com/gcosta87/herramientas-ulm/wikis/manual-del-Usuario" target="_blank" title="Manual del Sistema"><i class="fa fa-book fa-fw"></i> Manual</a></li>
                  <li><a href="/cambios.php" title="Cambios registrados en el Sistema"><i class="fa fa-clock-o fa-fw"></i> Cambios</a></li>
                </ul>
              </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="container">
<?php
      //Se valida que el usr este usando Chrome/Chromium
      $chequeoNavegadorCompatible= (preg_match('/(Chromium|Chrome)/',$_SERVER['HTTP_USER_AGENT']) === 0);

      //Se valida que el usr no este navegando en el dominio destinado para "desarrolo"
      $chequeoDominioDesarrollo= (preg_match('/'.$config[DOMINIO_DESARROLLO].'/i',$_SERVER['HTTP_HOST']) === 1);

      if($chequeoNavegadorCompatible || $chequeoDominioDesarrollo){
        echo '           <div style="margin-top:65px;">';
        if($chequeoNavegadorCompatible){
?>
            <div class="alert alert-warning alert-dismissible fade in" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              <h4>No estas usando Chrome!</h4>
              <p>Este sistema fue desarrollado para ese navegador, te recomendamos que accedes con el o con su alternativa
                libre Chromium.</p>
            </div>
<?php
        }
        if($chequeoDominioDesarrollo){
?>
      <div class="alert alert-info alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4>Estas usando la versión desarrollo!</h4>
        <p>De esta forma podés conocer las nuevas funcionalidades, pero esto implica que pueda haber cosas que no funcionen bien (porque están en desarrollo).</p>
      </div>
<?php
         }
        echo '           </div>';
      }
?>
      <div class="contenedor">