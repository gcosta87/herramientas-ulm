<?php
//Archivo de configuracion de variables de entorno

//  CONSTANTES DISPONIBLES
const PATH_CARPETA_COMPARTIDA   = 'PATH_CARPETA_COMPARTIDA';
const PATH_CMSDMS               = 'PATH_CMSDMS';
const DOMINIO_DESARROLLO        = 'DOMINIO_DESARROLLO';


$config = array();

//Path a la raiz de la carpeta compartida por la ULM
$config[PATH_CARPETA_COMPARTIDA] = 'smb://pcgon/trabajo';
$config[PATH_CMSDMS] = $config['PATH_CARPETA_COMPARTIDA'].'/2017/OCA/CMS+DMS';

//pseudo Dominio utilizado en la version desarrollo del sistema
$config[DOMINIO_DESARROLLO] = 'herramientas.desarrollo';

//Google MAPs API KEY
$config['google_maps_key']= 'AIzaSyDJboTIcc1pnwZQsRQWtBofO1ZvU_5cphU';
?>