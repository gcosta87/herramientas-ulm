<?php
use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule();

$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => 'servidor',
    'port'      => '3307',
    'database'  => 'ulm_extras',
    'username'  => 'herramientasulm',
    'password'  => 'desarrolloulm',
    'charset'   => 'utf8',
    'collation' => 'utf8_general_ci',
    'prefix'    => '',
    'strict'    => false,
    'engine'    => null
]);

$capsule->setAsGlobal();

$capsule->bootEloquent();
