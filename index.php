<?php include_once('core/header.php'); ?>
    <div class="jumbotron" style="margin: 100px auto 60px auto;">
        <img src="/assets/logo.png" alt="Herramientas ULM" class="img-thumbnail center-block">
    </div>
    <h1>Herramientas ULM <small> Conjunto de utilidades para el equipo!</small></h1>
    <br/>
    <div class="row" >
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Stock OCA</h3>
                    <p>Visualización del stock mantenido por la ULM y el reportado por la Operador Lógistico. Gestion de entidades relacionadas (presentaciones, programa, destinos, insumos,...).</p>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body ">
                    <h3>Reportes</h3>
                    <p>Generación de reportes desde la visión de los distintos programas, destinos (Regiones Sanitarias) e insumos.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row" >
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Archivos OCA <small>Archivos CMS, DMS y RMS</small></h3>
                    <p>Validación de especificacion de los archivos OCA. Validación de la integridad e interpretación de los datos en CMS+DMS (distribuciones). Interpretación de los MMS (recepción de ingresos). Asistentes para la generación de archivos.</p>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Extras OCA</h3>
                    <p>Asistente de generación e interpretación de códigos PNUD. Otros.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row" >
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Melchor Romero <small>Stock del depósito</small></h3>
                    <p>Stock del Depósito. Deteción de cambios en el Stock (basado en archivos CSV)</p>
                </div>
            </div>
        </div>

    </div>

<?php include_once('core/footer.php'); ?>