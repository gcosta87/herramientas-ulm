<?php
include_once('../core/kernel.php');
include_once('../core/config.php');
include_once('../core/header.php');

use Models\PNUDDestino;

?>

        <div class="page-header">
          <div class="row">
            <div class="col-md-12">
              <h1 id="buttons">
                <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i> Destinos <small>Reportes sobre los diversos destinos</small>
              </h1>
            </div>
          </div>
        </div>

<?php

try{
    $destinoId = $_GET['id'];
    $destino = PNUDDestino::find($destinoId);
?>

<a href="/destinos/" role="button" class="btn btn-primary btn-sm pull-right" title="Volver a la página de selección del destino">Volver</a>
<h2>Reporte del destino <strong><?php echo $destino->nombre_corto.' ('.$destino->localidad.')';?></strong></h2>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#graficoDistribuciones" aria-controls="graficoDistribuciones" role="tab" data-toggle="tab">Gráfico de Distribuciones</a></li>
        <li role="presentation"><a href="#ultimaDistribucion" aria-controls="ultimaDistribucion" role="tab" data-toggle="tab">Última distribuición</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="graficoDistribuciones">
            <div class="row">
                <div class="col-md-12">
                    <h3>Gráfico de distribuiciones <small>Basado en los datos del operador logístico</small></h3>
                    <p>Se contrasta la cantidad de <strong>cajas</strong> de los insumos distruidos en los últimos 30 días con los del período anterior (60 días).</p>
                    <div  style="max-width: 2100px; overflow-x: scroll;  overflow-y: hidden">
                        <div id="graficoDeDistribuiciones" style="height: 600px;">
                            <div class="alert alert-warning alert-dismissible fade in hidden" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">×</span></button>
                                <strong>No hay datos!</strong>.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="ultimaDistribucion">
            <p>Se detalla la última distribución (últimos 30 días) recibida por el destino.</p>
            <table id="tablaDetalleDestinos">
                <thead>
                <tr>
                    <th>Insumo</th>
                    <th>Unidades</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Insumo</th>
                    <th>Unidades</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
<script>
    $('#tablaDetalleDestinos').DataTable({
        language: {
            url: '/assets/DataTables/Spanish.json'
        },
        "dom": 'lfrtip',
        "pageLength": 25,
        "ajax":    '/stockoca/detalleModal.php?consulta=tablaDetalleDistribucionDestino&destinoId=<?php echo $destinoId;?>',
        "columns": [
            { "data": "insumo"},
            { "data": "unidades"}
        ]
    });
</script>

<?php
    $reporteUltimasDistribuiciones= $db->select("
                                        select	30dias.insumo,
                                                30dias.unidades as unidades_30dias,
                                                IFNULL(60dias.unidades,0) as unidades_60dias
                                        from 
                                            (
                                                select	dms.codigo_oca as codigo_oca,
                                                        v_i.nombre as insumo,
                                                        -- (dms.cantidad * v_i.presentacion_multiplicador) as unidades,
                                                        (dms.cantidad) as unidades,
                                                        '30 dias' as leyenda
                                                        
                                        
                                                from	pnud_destinos destino
                                                        left join LINEA_CMS cms
                                                        on (destino.id = $destinoId and cms.destino = destino.id )
                                                        left join LINEA_DMS dms
                                                        on (dms.pedido_numero = cms.pedido_numero)
                                                        left join v_insumos v_i
                                                        on (dms.codigo_oca = v_i.codigo_oca)
                                        
                                                where
                                                    cms.fecha_archivo between (current_date() - INTERVAL 30 day) and (current_date())
                                            ) 30dias
                                            left join
                                            (
                                                select	dms.codigo_oca as codigo_oca,
                                                        v_i.nombre as insumo,
                                                        -- (dms.cantidad * v_i.presentacion_multiplicador) as unidades,
                                                        (dms.cantidad) as unidades,
                                                        '60 dias' as leyenda
                                        
                                                from	pnud_destinos destino
                                                        left join LINEA_CMS cms
                                                        on (destino.id = $destinoId and cms.destino = destino.id )
                                                        left join LINEA_DMS dms
                                                        on (dms.pedido_numero = cms.pedido_numero)
                                                        left join v_insumos v_i
                                                        on (dms.codigo_oca = v_i.codigo_oca)
                                        
                                                where
                                                    cms.fecha_archivo between (current_date() - INTERVAL 60 day) and (current_date() - INTERVAL 31 day)
                                            ) 60dias
                                            on (30dias.codigo_oca = 60dias.codigo_oca)
                                            
                                        order by 30dias.insumo 

    ");
?>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<?php
    if(count($reporteUltimasDistribuiciones) >= 1){
?>
    <script type="text/javascript">
        google.charts.load("current", {packages:['corechart']});
        google.charts.setOnLoadCallback(renderizarGraficos);

        function renderizarGraficos() {
            //Datos del grafico stock
            var datosUltimasDistribuiciones = google.visualization.arrayToDataTable([
                ['Insumo', '30 Dias', '60 Días'],
<?php

    $datos='';
    foreach ($reporteUltimasDistribuiciones as $registro){
        $datos.="['".$registro->insumo."',".$registro->unidades_30dias.",".$registro->unidades_60dias. '], ';
    }
    //Se remueve el ultimo ",
    $datos=substr($datos,0,-2);
    echo $datos;

?>
            ]);
            var opcionesUltimasDistribuciones= {
                title: 'Valores de la última distribucion comparada con la anterior',
                vAxis: {title: 'Cantidad de cajas'},
                hAxis: {title: 'Insumos distribuidos'},
                width: <?php echo count($reporteUltimasDistribuiciones)*85;?>
            };
            var chartUltimasDistribuiciones = new google.visualization.AreaChart(document.getElementById('graficoDeDistribuiciones'));
            chartUltimasDistribuiciones.draw(datosUltimasDistribuiciones, opcionesUltimasDistribuciones);
        }
    </script>
<?php
        }
        else{
            echo "
                <script>
                    $('#graficoDeDistribuiciones .alert').removeClass('hidden');
                </script>
            ";
        }
    }
    catch(Exception $e){
        echo '<h2>Se ha producido un error <small>Es un bajón!!!  <i class="fa fa-frown-o"></i></small></h2><br/><strong>Motivo</strong>: '.$e->getMessage();
    }

    include_once('../core/footer.php');
?>
