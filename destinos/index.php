<?php
include_once('../core/kernel.php');
include_once('../core/config.php');
include_once('../core/header.php');

use Models\PNUDDestino;

?>

        <div class="page-header">
          <div class="row">
            <div class="col-md-12">
              <h1 id="buttons">
                <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i> Destinos <small>Reportes sobre los diversos destinos</small>
              </h1>
            </div>
          </div>
        </div>

<h2>Selección del destino <small>Eliga sobre que destino se realizará el reporte</small></h2>
<?php
    try{
        $destinos = PNUDDestino::where('activo','1')->orderby('nombre')->get();

        echo "<div class='list-group'>";
        foreach ($destinos as $destino){
            echo    '<a href="/destinos/reporte.php?id='.$destino->id.'" class="list-group-item">';
            echo    '    <h4 class="list-group-item-heading">'.$destino->nombre_corto.' <small sytle="margin-left:20px;">'.$destino->localidad.'</small></h4>';
            echo    '    <p class="list-group-item-text">'.$destino->nombre.'</p>';
            echo    '</a>';
        }
        echo '</div>';
    }
    catch(Exception $e){
        echo '<h2>Se ha producido un error <small>Es un bajón!!!  <i class="fa fa-frown-o"></i></small></h2><br/><strong>Motivo</strong>: '.$e->getMessage();
    }

    include_once('../core/footer.php');
?>
