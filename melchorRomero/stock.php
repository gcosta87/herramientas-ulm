<?php include_once('../core/kernel.php'); ?>
<?php include_once('../core/config.php'); ?>
<?php include_once('../core/header.php'); ?>

<div class="page-header">
  <div class="row">
    <div class="col-md-12">
      <h1 id="buttons">
        <i class="fa fa-tasks fa-fw" aria-hidden="true"></i> Stock MR <small>Reporte de Stock en el Depósito de Melchor Romero</small>
      </h1>
    </div>
  </div>
</div>
<?php

try{

    //fecha del reporte
    $fecha = $db->select('
                      select date_format(fecha_stock_mr,\'%d/%m/%Y\') as fecha
                      from configuracion
              ')[0]->fecha;
?>

<h2><i class="fa fa-briefcase"></i> Insumos al <?php echo $fecha;?> <small>Visualización insumos concretos</small></h2>
<div class="pull-right">
    <label class="checkbox-inline text-success">
        <input type="checkbox" id="cbSoloConStock" title="Si se activa se excluiran todos aquellos insumos que no cuenten con stock"/> Mostrar solo insumos con stock
    </label>
</div>
<div class="row">
    <div class="col-md-12">
        <table width="60%" id="Tabla">
            <thead>
                <tr>
                    <th><abbr data-toggle="tooltip" data-placement="top" title="Pseudo-Identificador generado dinamicamente para indentificar la combinacion de Id generico + Env Hosp. + Programa">Id Concreto</abbr></th>
                    <th><abbr data-toggle="tooltip" data-placement="top" title="Pseudo-Identificador generado dinamicamente para identificar un insumo + envase primario">Id Genérico</abbr></th>
                    <th>Insumo</th>
                    <th>Envase Primario</th>
                    <th>Envase Hospitalario</th>
                    <th>Unidades</th>
                    <th>Programa</th>
                    <th data-filtrar="false"><abbr data-toggle="tooltip" data-placement="top" title="Indica la cantidad de filas/registros que se agruparon. Tambien se incluye el numero de fila">Registros</abbr></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Id Concreto</th>
                    <th>Id Genérico</th>
                    <th>Insumo</th>
                    <th>Envase Primario</th>
                    <th>Envase Hospitalario</th>
                    <th>Unidades</th>
                    <th>Programa</th>
                    <th>Cant de Registros</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<script src="/assets/funciones.js"></script>
<script>
    var configuracion = {
        "ajax": {   // Llamada ajax parametrizada: se realiza el request pasando parametros via GET
            "url": '/melchorRomero/ajax.php?consulta=stock_mr_concreto',
            "data": function(data){
                data.conStock= $('#cbSoloConStock').prop('checked');
            }
        },
        "columns": [
            { "data": "id_concreto", "width":"20"},
            { "data": "id_generico", "width":"15"},
            { "data": "insumo","width":"250"},
            { "data": "envase_primario","width":"150"},
            { "data": "envase_hospitalario","width":"150"},
            { "data": "sum_stock_actual","width":"40"},
            { "data": "programa","width":"150"}
        ],
        "autoWidth": false,
        "order": [[ 3, "asc" ]],
        "columnDefs": [
            {
                "targets": 7,
                "render": function ( data, type, row ) {
                    return row.cantidad_registros + ' <i class="fa fa-info-circle"  title="Fila: '+ row.filas_involucradas+'"></i>';
                }
            }
        ]
    };

    DataTableULM('#Tabla','Melchor Romero - Stock',configuracion);

    //Cada vez que se activa en el checkbox se llama al refresh del datatable
    $( '#cbSoloConStock' ).on( 'click', function(){
        datatableULMAjaxRefresh();
    });

    $(function(){
        setTimeout(function(){
            $('[data-toggle="tooltip"]').tooltip();
        },1500);
    });
</script>

<?php
    }
    catch(Exception $e){
        echo '<h2>Se ha producido un error <small>Es un bajón!!!  <i class="fa fa-frown-o"></i></small></h2><br/><strong>Motivo</strong>: '.$e->getMessage();
    }

include_once('../core/footer.php');
?>
