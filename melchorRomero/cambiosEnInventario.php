<?php
include_once('../core/kernel.php');
include_once('../core/config.php');
include_once('../core/header.php');
use Models\Insumo;
?>
<style>
    input.insumo {
        text-transform: uppercase;
    }

    div.renglon, div.cabecera{
        margin-bottom: 20px;
    }

    span.numeroRenglon{
        color: #ccced2;
        letter-spacing: 3px;
        font-size:1.6em;
    }
</style>


    <div class="page-header">
        <div class="row">
            <div class="col-md-12">
                <h1 id="buttons"><i class="fa fa-search fa-fw" aria-hidden="true"></i> Cambios en el Inventario <small>Analice y compare los cambios del Excel</small></h1>
            </div>
        </div>
    </div>



<div class="alert alert-warning alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    <h4>Los archivos que se van a analizar deben estar en formato CSV (MS-DOS)!</h4>
    <p>Para generarlos con el Excel, uno debe abrir la hoja que le interesa y e ir al Menu <strong>Archivo</strong> (o Boton Office en 2007/2010) > <strong>Guardar Como</strong> > <strong>Otros Formatos</strong> y seleccionar como valor del Tipo "<strong>CSV (MS-DOS)</strong>".</p>
</div>

<h2>Inventario de Melchor Romero <small>Se analizará los cambios que sufieron los registros</small></h2>
<form name="formulario" action="/melchorRomero/cambiosEnInventarioAction.php" method="post" enctype="multipart/form-data">
    <div class="row">

        <div class="col-md-6">
            <input id="archivoViejo" name="archivoViejo" type="file"  class="file-loading"/>
        </div>
        <div class="col-md-6 ">
            <input id="archivoNuevo" name="archivoNuevo" type="file"  class="file-loading"/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1 col-md-offset-5">
            <input type="submit" id="btnAnalizar" role="button" class="btn btn-success btn-lg" value="Analizar" style="margin-top: 50px;"/>
        </div>
    </div>
</form>
<div class="row">
    <div class="col-md-offset-10 col-md-2">
        <span data-toggle="tooltip" title="Ver detalles del proceso de analisis" data-placement="top"><a nohref role="button" class="btn btn-default" data-toggle="collapse" data-target="#detallesAnalisis">¿Qué se analizará?</a></span>
    </div>
    <div class="col-md-12">
        <div id="detallesAnalisis" class="collapse">
            <h2>Detalles de la comparación <small>Resultados que se mostrarán</small></h2>
            <p>Por cada registro leido, se lo identifica, indistintamente de como ha sido <strong title="Se ignora el uso de mayusculas, minusculas, acentos y espacios extra, entre otros caracteres más. Los lotes definidos como S/N y/o S/L se tratan como iguales." data-toggle="tooltip" data-placement="top">definido/escrito</strong> los siguientes campos: Programa, Presentacion, Envase primario, Envase hospitalario  y Lote (se ignora el vencimiento). Se intenta generar un identificador (practicamente unico) que se lo llama <strong>huella concreta</strong>.</p>
            <p>Utilizando las huellas de ambos archivos se reportarán los insumos que cumplan con algunas de estas condiciones: </p>
            <ul>
                <li>Insumos presentes en ambos archivos y que posean diferencias en el stock.</li>
                <li>Insumos que sean exclusivos de alguno de los archivos (no esté presente en el otro).</li>
                <li>Insumos que presentan inconsistencias:
                    <ul>
                        <li>Repetidos: insumos que han sido definido más de una vez.</li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<style>
    dd{
        margin-bottom:5px;
    }
</style>
<div id="resultados" class="collapse">
    <h2>Resultados del análisis</h2>
    <div id="observaciones"></div>
    <h3>Informacion básica</h3>
    <div clas="row">
        <div class="col-sm-5 col-sm-offset-1">
            <h4>Archivo Viejo</h4>
            <dl>
                <dt>Nombre:</dt>
                <dd><span id="archivo_viejo_nombre" class="spanVariable"></span></dd>
                <dt>Registros:</dt>
                <dd><span id="archivo_viejo_registros" class="spanVariable"></span></dd>
                <dt>Huellas presentes:</dt>
                <dd><span id="archivo_viejo_huellas" class="spanVariable"></span></dd>
            </dl>
        </div>
        <div class="col-sm-5 col-sm-offset-1">
            <h4>Archivo Nuevo</h4>
            <dl>
                <dt>Nombre:</dt>
                <dd><span id="archivo_nuevo_nombre" class="spanVariable"></span></dd>
                <dt>Registros:</dt>
                <dd><span id="archivo_nuevo_registros" class="spanVariable"></span></dd>
                <dt>Huellas presentes:</dt>
                <dd><span id="archivo_nuevo_huellas" class="spanVariable"></span></dd>
            </dl>
        </div>
    </div>
    <br/>
    <h3>Diferencias <small><span class="spanVariable" id="diferencias_cantidad">0</span> registros</small></h3>
    <div id="diferencias">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Programa</th>
                    <th>Presentacion</th>
                    <th>Envase primario</th>
                    <th>Envase secundario</th>
                    <th>Lote</th>
                    <th>Stock viejo</th>
                    <th>Stock nuevo</th>
                    <th>Diferencia</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Programa</th>
                    <th>Presentacion</th>
                    <th>Envase primario</th>
                    <th>Envase secundario</th>
                    <th>Lote</th>
                    <th>Stock viejo</th>
                    <th>Stock nuevo</th>
                    <th>Diferencia</th>
                </tr>
            </tfoot>
            <tbody></tbody>
        </table>
    </div>
    <br/><br/>

    <h3>Exclusivos <small><span class="spanVariable" id="exclusivos_cantidad">0</span> registros</small></h3>
    <div id="exclusivos">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Archivo</th>
                <th>Programa</th>
                <th>Presentacion</th>
                <th>Envase primario</th>
                <th>Envase secundario</th>
                <th>Lote</th>
                <th>Stock </th>
                <th>Fila</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Archivo</th>
                <th>Programa</th>
                <th>Presentacion</th>
                <th>Envase primario</th>
                <th>Envase secundario</th>
                <th>Lote</th>
                <th>Stock </th>
                <th>Fila</th>
            </tr>
            </tfoot>
            <tbody></tbody>
        </table>
    </div>
    <br/><br/>

    <h3>Inconsistencias <small><span class="spanVariable" id="inconsistencias_cantidad">0</span> registros</small></h3>
    <div id="inconsistencias">
        <h4>Repetidos <small>Huellas concretas detectadas en el mismo archivo</small></h4>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Archivo</th>
                <th>Programa</th>
                <th>Presentacion</th>
                <th>Lote</th>
                <th>Stocks</th>
                <th>Filas</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Archivo</th>
                <th>Programa</th>
                <th>Presentacion</th>
                <th>Lote</th>
                <th>Stocks</th>
                <th>Filas</th>
            </tr>
            </tfoot>
            <tbody></tbody>
        </table>
    </div>
    <br/><br/>

</div>

<script>
    //GLOBALES requeridas
    //Datatables
    var $tablaDiferencias = null;
    var $tablaExclusivos = null;
    var $tablaInconsistencias = null;


    /**
     * Se restablecen los componentes
     */
    function restablecerComponentes(){
        //limpieza del componente
        setTimeout(function(){
            //Restablecimiento de Krajee
            $('#archivoViejo').fileinput('refresh');
            $('#archivoNuevo').fileinput('refresh');
        },500);


        $('#detallesAnalisis').collapse('hide');
    }

    function configurarFileInput(selectorJQuery, leyenda){
        $(selectorJQuery).fileinput({
            language: 'es',
            allowedFileExtensions: ['csv'],
            browseClass: "btn btn-primary",
            browseLabel: leyenda,
            showCancel:false,
            showCaption: false,
            showUpload: false,
            showClose:false,
            required: true
        });
    }

    $(document).on('ready', function() {
        configurarFileInput('#archivoViejo', 'Seleccionar CSV de Stock Viejo');
        configurarFileInput('#archivoNuevo', 'Seleccionar CSV de Stock Nuevo');

        //El usr selecciono un archivo
        $("#archivoViejo,#archivoNuevo").on('fileselect', function(event, numFiles, label) {
            //Restablecimiento de panel de resultados
            $('#resultados').collapse('hide');
            $('.spanVariable').text('');

            $('#observaciones').html('');

            //Si han sido inicializadas las datatable, se las destruye y se limpian los datos
            if($tablaDiferencias != null){
                $tablaDiferencias.destroy();
                $tablaExclusivos.destroy();
                $tablaInconsistencias.destroy();

                $tablaDiferencias = null;
                $tablaExclusivos = null;
                $tablaInconsistencias = null;

                $('#diferencias table tbody').html('');
                $('#exclusivos table tbody').html('');
                $('#inconsistencias table tbody').html('');
            }
            //reinicializacion de contadores
            $('#diferencias_cantidad').html('');
            $('#exclusivos_cantidad').html('');
            $('#inconsistencias_cantidad').html('');

        });



        $('form[name="formulario"]').submit(function(e) {
            e.preventDefault();

            var postData = new FormData(this);

            $.ajax({
                url : $(this).attr('action'),
                type: $(this).attr('method'),
                data : postData,
                processData: false,
                contentType: false,
                success:function(data, textStatus, jqXHR){
                    var respuesta = JSON.parse(data);
                    restablecerComponentes();

                    if(!respuesta.error){
                        renderizarReporteDiferencias(respuesta.reportes.diferencias);
                        renderizarReporteExclusivos(respuesta.reportes.exclusivos);
                        renderizarReporteInconsistencias(respuesta.reportes.inconcistencias);

                        //actualizo los datos de los archivos
                        $('#archivo_viejo_nombre').text(respuesta.archivos.viejo.nombre);
                        $('#archivo_viejo_registros').text(respuesta.archivos.viejo.registros);
                        $('#archivo_viejo_huellas').text(respuesta.archivos.viejo.huellas);

                        $('#archivo_nuevo_nombre').text(respuesta.archivos.nuevo.nombre);
                        $('#archivo_nuevo_registros').text(respuesta.archivos.nuevo.registros);
                        $('#archivo_nuevo_huellas').text(respuesta.archivos.nuevo.huellas);
                    }
                    else{
                        //Se indica que hubo un error al procesar los archivos (posible no cumplimiento de especificacion,etc..)
                        var mensajeError = '<div class="alert alert-danger alert-dismissible fade in" role="alert">'+
                            '<h4>Hubo un error al procesar los archivos!</h4>'+
                            '<p>'+respuesta.mensaje+'</p>'+
                            '</div>';

                        $('#observaciones').html(mensajeError);
                    }

                    //Se muestra el panel
                    $('#resultados').collapse('show');

                },
                error: function(jqXHR, textStatus, errorThrown){
                    //error "critico"
                    alertify.error('Ocurrio un error al enviar los archivos al servidor :(...');
                }
            });
        });
    });



    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });


/**
 *
 * @param respuesta
 */
function renderizarReporteDiferencias(reporte){
    var contador = 0;
    var tbodyHTML = '';

    reporte.forEach(function(elemento, index){
        contador++;
        var insumo = elemento.insumo;

        var registro = '<tr>';

        registro+='<td>'+insumo.programa+'</td>';
        registro+='<td>'+insumo.presentacion+'</td>';
        registro+='<td>'+insumo.envase_primario+'</td>';
        registro+='<td>'+insumo.envase_hospitalario+'</td>';
        registro+='<td>'+insumo.lote+'</td>';
        registro+='<td><span title="Fila #'+elemento.viejo.linea+'">'+elemento.viejo.stock+'</span></td>';
        registro+='<td><span title="Fila #'+elemento.nuevo.linea+'">'+elemento.nuevo.stock+'</span></td>';

        registro+='<td>'+(parseInt(elemento.viejo.stock) - parseInt(elemento.nuevo.stock))+'</td>';

        registro+= '</tr>';

        tbodyHTML+=registro;
    });

    $('#diferencias_cantidad').html(contador);
    $('#diferencias table tbody').html(tbodyHTML);

    //inicializacion del datable
    $tablaDiferencias =  $('#diferencias table').DataTable({
        language: {
            url: '/assets/DataTables/Spanish.json'
        },
        "pageLength": 25
    });
}


/**
 * Renderiza la linea
 * @param string contexto "nuevo" o "viejo"
 * @param elemento
 */
function renderizarFilaReporteExclusivo(contexto,elemento){
    var registro = '<tr>';

    registro+='<td>'+ contexto +'</td>';
    registro+='<td>'+ elemento.insumo.programa +'</td>';
    registro+='<td>'+ elemento.insumo.presentacion +'</td>';
    registro+='<td>'+ elemento.insumo.envase_primario +'</td>';
    registro+='<td>'+ elemento.insumo.envase_hospitalario +'</td>';
    registro+='<td>'+ elemento.insumo.lote +'</td>';
    registro+='<td>'+ elemento.datos.stock +'</td>';
    registro+='<td>'+ elemento.datos.linea +'</td>';

    registro+= '</tr>';

    return registro;
}


function renderizarReporteExclusivos(reporte){
    var contador = 0;
    var tbodyHTML = '';

    reporte.nuevo.forEach(function(elemento, index){
        contador++;
        tbodyHTML+=renderizarFilaReporteExclusivo('nuevo',elemento);
    });

    reporte.viejo.forEach(function(elemento, index){
        contador++;
        tbodyHTML+=renderizarFilaReporteExclusivo('viejo',elemento);
    });

    $('#exclusivos_cantidad').html(contador);
    $('#exclusivos table tbody').html(tbodyHTML);

    //inicializacion del datable
    $tablaExclusivos = $('#exclusivos table').DataTable({
        language: {
            url: '/assets/DataTables/Spanish.json'
        },
        "pageLength": 10
    });
}




/**
 * Renderiza la linea
 * @param string contexto "nuevo" o "viejo"
 * @param elemento
 */
function renderizarFilaReporteInconsistenciasRepetidos(contexto,elemento){
    var registro = '<tr>';

    var stocks = elemento.datos.map(function(dato){return dato.stock;}).join();
    var lineas  = elemento.datos.map(function(dato){return dato.linea;}).join();

    registro+='<td>'+ contexto +'</td>';
    registro+='<td>'+ elemento.insumo.programa +'</td>';
    registro+='<td>'+ elemento.insumo.presentacion +'</td>';
    registro+='<td>'+ elemento.insumo.lote +'</td>';
    registro+='<td>'+ stocks +'</td>';
    registro+='<td>'+ lineas+'</td>';

    registro+= '</tr>';

    return registro;
}


function renderizarReporteInconsistencias(reporte){
    var contador = 0;
    var tbodyHTML = '';

    reporte.repetidos.nuevo.forEach(function(elemento, index){
        contador++;
        tbodyHTML+=renderizarFilaReporteInconsistenciasRepetidos('nuevo',elemento);
    });

    reporte.repetidos.viejo.forEach(function(elemento, index){
        contador++;
        tbodyHTML+=renderizarFilaReporteInconsistenciasRepetidos('viejo',elemento);
    });

    $('#inconsistencias_cantidad').html(contador);
    $('#inconsistencias table tbody').html(tbodyHTML);

    //inicializacion del datable
    $tablaInconsistencias = $('#inconsistencias table').DataTable({
        language: {
            url: '/assets/DataTables/Spanish.json'
        },
        "pageLength": 10
    });
}

</script>

<?php include_once('../core/footer.php'); ?>
