<?php
/**
 * Se procesan el archivo y fecha enviada desde el formualrio de importacion (index.php)
 */
include_once('../core/kernel.php');
include_once('../core/config.php');
include '../vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';
include '../vendor/phpoffice/phpexcel/Classes/PHPExcel/Shared/Date.php';




//
//  CONSTANTES
//
const  ARCHIVO_CSV_DELIMITADOR = ';';

const ARCHIVO_VIEJO = 'viejo';
const ARCHIVO_NUEVO = 'nuevo';

//  Cabecera del rango de campos que se leeran. Se utiliza para validar
const CABECERA_PARCIAL_CSV_REGEX =  "/^(PALL?ETT?|(LUGAR )?ALMACENAM);PRESENTACION;FECHA INGRESO;LABORATORIO;PROGRAMA;DIRECCION;ENVASE PRIMARIO;ENVASE HOSPITALARIO;TEMPERATURAS;LOTE - PTDA;VTO;TOTAL;(TOTAL MES;)?STOCK ACTUAL;/";

//  Indices originales de la cabecera del CSV
const   REGISTRO_CSV_PALETT     			= 0;
const   REGISTRO_CSV_PRESENTACION			= 1;
const   REGISTRO_CSV_FECHA_INGRESO		    = 2;
const   REGISTRO_CSV_LABORATORIO			= 3;
const   REGISTRO_CSV_PROGRAMA				= 4;
const   REGISTRO_CSV_DIRECCION			    = 5;
const   REGISTRO_CSV_ENVASE_PRIMARIO		= 6;
const   REGISTRO_CSV_ENVASE_HOSPITALARIO	= 7;
const   REGISTRO_CSV_TEMPERATURAS	        = 8;
const   REGISTRO_CSV_LOTE_PTDA	            = 9;
const   REGISTRO_CSV_VTO		            = 10;
const   REGISTRO_CSV_TOTAL		            = 11;
const   REGISTRO_CSV_TOTAL_MES	            = 12;   // campo solo presente en excels "diarios", en los de meses anteriores no existe y su lugar lo ocupa STOCK_ACTUAL
const   REGISTRO_CSV_STOCK_ACTUAL	        = 13;


//  Indices del registro creado a partir de la lectura del excel
const   REGISTRO_ARRAY_LINEA				= 0;
const   REGISTRO_ARRAY_PROGRAMA				= 1;
const   REGISTRO_ARRAY_PRESENTACION			= 2;
const   REGISTRO_ARRAY_LABORATORIO			= 3;
const   REGISTRO_ARRAY_ENVASE_PRIMARIO		= 4;
const   REGISTRO_ARRAY_ENVASE_HOSPITALARIO	= 5;
const   REGISTRO_ARRAY_LOTE_PTDA	        = 6;
const   REGISTRO_ARRAY_VTO		            = 7;
const   REGISTRO_ARRAY_STOCK_ACTUAL	        = 8;

const   REGISTRO_HUELLA_HG      = 0;
const   REGISTRO_HUELLA_HC      = 1;
const   REGISTRO_HUELLA_STOCK   = 2;
const   REGISTRO_HUELLA_LINEA   = 3;


//
//  FUNCIONES
//

/**
 * Establece la respuesta para retornar.
 * @param array $respuesta
 * @param string $mensaje motivo del error
 */
function generarRespueastaErronea(&$respuesta = array(),$mensaje){
    $respuesta['error'] = true;
    $respuesta['mensaje'] = $mensaje;
}

/**
 * Valida que la cabecera concuerde con lo esperado.
 * @param String $registroCabeceraString
 * @throws Exception en caso de que sea invalida
 */
function validarCabecera($registroCabeceraString = ""){
    if(!preg_match(CABECERA_PARCIAL_CSV_REGEX, $registroCabeceraString)){
        throw new Exception('La cabecera no concuerda con la esperada');
    }
}

/**
 * A partir del texto original que se utilizara para generar el codigo se lo limpia
 * @param $string
 * @return string sanitizado
 */
function sanitizarStringFuente($string){
    $stringSanitizado = strtolower($string);

    //TODO analizar datos reales para sustituir otros posibles caracteres utiles (para identificar duplicados)
    $stringSanitizado = str_replace(
        ['Ñ','Á','É','Í','Ó','Ú','á','é','í','ó','ú',' '],
        ['ñ','a','e','i','o','u','a','e','i','o','u',''],
        $stringSanitizado
    );

    return $stringSanitizado;
}
/**
 * Se genera el "Codigo Insumo Generico" (HG): Hexadecimal de longitud 5 a partir de la PRESENTACION + PROGRAMA;
 * @param array $registroCSV
 * @return string codigo
 */
function generarHuellaGenerico($registroCSV =array()){
    $string = sanitizarStringFuente($registroCSV[REGISTRO_ARRAY_PROGRAMA].$registroCSV[REGISTRO_ARRAY_PRESENTACION]);
    return strtoupper(substr(md5($string),-5));
}

/**
 * Se genera el "Codigo Insumo Concreto" (HC): Hexadecimal de longitud 8 a partir de HG + LABORATORIO+ ENVASE PRIMARIO+ENVASE HOSPITALARIO + LOTE - PTDA
 * @param array $registroCSV
 * @param string $HG
 * @return string
 */
function generarHuellaConcreta($registroCSV = array(), $HG=""){
    $loteSanitizado = str_replace(['S/N','S/L'],'',strtoupper($registroCSV[REGISTRO_ARRAY_LOTE_PTDA]));
    $string = $HG.sanitizarStringFuente($registroCSV[REGISTRO_ARRAY_LABORATORIO].$registroCSV[REGISTRO_ARRAY_ENVASE_PRIMARIO].$registroCSV[REGISTRO_ARRAY_ENVASE_HOSPITALARIO].$loteSanitizado);
    return strtoupper(substr(md5($string),-8));

}

/**
 * Valida que la linea sea un registro valido
 * @param string $lineaCSV
 * @return boolean true caso que sea valida, false caso contrario
 */
function validarLineaCSV($lineaCSV = ""){
    //verifica que al menos tenga presentacion (segundo campo)
    return (preg_match('/^;;/',$lineaCSV) !== 1 );
}


/**
 * @param array $registroCSV
 * @return array registro huella
 */
function generarRegistroHuella($registroCSV = array()) {
    $HG = generarHuellaGenerico($registroCSV);

    return [
        $HG,
        generarHuellaConcreta($registroCSV,$HG),
        $registroCSV[REGISTRO_ARRAY_STOCK_ACTUAL],
        $registroCSV[REGISTRO_ARRAY_LINEA]
    ];
}

/***
 *
 * @param String $identificador nombre para identificar en el procesamiento posterior. Concuerda con una de las constantes ARCHIVO_XXXX
 * @param $uploadFile
 * @param array $estructura
 * @throws Exception ante casos de error
 */
function procesarArchivoCSV($identificador="",$uploadFile,&$estructura = []){
    $archivoSubido  = $uploadFile['tmp_name'];

    //Se registra el nombre original del archivo
    $estructura[$identificador]['data']['nombre']=$uploadFile['name'];

    //Los registros de los CSV porque
    $registrosCSV = [];
    $huellas = [];
    try{
        $lineasCSV = file($archivoSubido, FILE_SKIP_EMPTY_LINES);

        //Registro Cabecera
        $cabeceraCSV  = $lineasCSV[2];
        $estructura[$identificador]['cabecera'] = $cabeceraCSV;

        //Se valida la cabecera
        validarCabecera($cabeceraCSV);

        //Si existe la presencia del campo "TOTAL MES" se debera excluir del procesamiento
        $campoTotalMes = (preg_match('/;\s*TOTAL\s+MES\s*;/i', $cabeceraCSV) === 1);

        for ($numeroFila = 3; $numeroFila  < count($lineasCSV); $numeroFila++){

            $lineaCSV = utf8_encode($lineasCSV[$numeroFila]);
            if(validarLineaCSV($lineaCSV)) {
                $registroCSVOriginal = explode(ARCHIVO_CSV_DELIMITADOR, $lineaCSV);

                //Se cargan los campos comunes a los CSV
                $registroCSV = [
                    $numeroFila+1,
                    trim($registroCSVOriginal[REGISTRO_CSV_PROGRAMA]),
                    trim($registroCSVOriginal[REGISTRO_CSV_PRESENTACION]),
                    trim($registroCSVOriginal[REGISTRO_CSV_LABORATORIO]),
                    trim($registroCSVOriginal[REGISTRO_CSV_ENVASE_PRIMARIO]),
                    trim($registroCSVOriginal[REGISTRO_CSV_ENVASE_HOSPITALARIO]),
                    trim($registroCSVOriginal[REGISTRO_CSV_LOTE_PTDA]),
                    trim($registroCSVOriginal[REGISTRO_CSV_VTO]),
                ];

                //Si posee el campo extra, se leera el stock actual en su lugar correcto, sino este ultimo campo ocupa el lugar del campo extra
                if($campoTotalMes){
                    $registroCSV[]=intval(trim($registroCSVOriginal[REGISTRO_CSV_STOCK_ACTUAL]));
                }
                else{
                    $registroCSV[]=intval(trim($registroCSVOriginal[REGISTRO_CSV_TOTAL_MES]));
                }

                $registrosCSV[] = $registroCSV;
                $huella     = generarRegistroHuella($registroCSV);
                $huellas[]     = $huella;

                $registroCSV['hc']=$huella[REGISTRO_HUELLA_HC];

                //Se carga en la estructura la data para presentar posteriormente (de manera global):
                //Se unifican los datos indistintamente el origen
                $estructura['datos'][$huella[REGISTRO_HUELLA_HC]]=[
                    'hc'=>$huella[REGISTRO_HUELLA_HC],
                    'hg'=>$huella[REGISTRO_HUELLA_HG],
                    'programa'=>$registroCSV[REGISTRO_ARRAY_PROGRAMA],
                    'laboratorio'=>$registroCSV[REGISTRO_ARRAY_LABORATORIO],
                    'presentacion'=>$registroCSV[REGISTRO_ARRAY_PRESENTACION],
                    'envase_primario'=>$registroCSV[REGISTRO_ARRAY_ENVASE_PRIMARIO],
                    'envase_hospitalario'=>$registroCSV[REGISTRO_ARRAY_ENVASE_HOSPITALARIO],
                    'lote'=>$registroCSV[REGISTRO_ARRAY_LOTE_PTDA]
                ];
            }
            unset($lineaCSV);
        }
    }
    catch(Exception $e) {
        unlink($uploadFile['tmp_name']);
        throw new Exception('Error al leer el archivo "'.pathinfo($archivoSubido,PATHINFO_BASENAME).'": '.$e->getMessage());
    }

    //Eliminacion del archivo temporal
    unlink($uploadFile['tmp_name']);


    $estructura[$identificador]['registros'] = $registrosCSV;
    $estructura[$identificador]['huellas'] = $huellas;

    $estructura[$identificador]['data']['registros'] = count($registrosCSV);


}

/**
 * Retorna una estructura que facilita la comparacion. [ HC=>[['indice'=> n,'stock'=>n],...],...]
 * @param array $huellas []
 * @return array [ HC=>[['indice'=> n,'stock'=>n], ...], ...]
 */
function generarEstructuraComparable($huellas = array() ){
    $estructraComparable = array();

    for($indice=0;$indice < count($huellas); $indice++){
        $huella = $huellas[$indice];

        //TODO analizar si se descarta el indice por desuso
        $datoStock =  ['indice'=>$indice, 'stock'=> $huella[REGISTRO_HUELLA_STOCK], 'linea'=> $huella[REGISTRO_HUELLA_LINEA]];

        $hc = $huella[REGISTRO_HUELLA_HC];

        if(array_key_exists($hc,$estructraComparable)){
            //huella hc repetido: solo se deja sentado la cantidad de repetidos
            $estructraComparable[$hc]['stocks'][]= $datoStock;
            $estructraComparable[$hc]['repetidos']++;
        }
        else{
            //inicializacion..
            $estructraComparable[$hc]=[
                'repetidos'  => 0,
                'stocks'    => [$datoStock]
            ];
        }
    }

    return $estructraComparable;
}



//
//  FUNCIONES PARA PROCESAR HUELLAS CONCRETAS
//

/**
 * Analiza las HC que presentan repeticion, y actualiza en el HCData los campos relacionados (unicas y repetidas)
 * @param string $contexto constante ARCHIVO_VIEJO o ARCHIVO_NUEVO.
 * @param array $datosComparables obtenido de la invocacion a #generarEstructuraComparable.
 * @param array $HCData obtenido de la invocacion #procesarHCPresentes.
 */
function procesarHCRepetidas($contexto = ARCHIVO_VIEJO, $datosComparables = [], &$HCData = []){
    foreach($datosComparables as $HC => $datoComparable){

        if($datoComparable['repetidos'] === 0){
            $HCData[$contexto]['unicas'][] = $HC;
        }
        else{
            $HCData[$contexto]['repetidas'][] = $HC;
        }
    }
}


/**
 * Procesa las HC presentes en ambas estrucutras comparables (resultante de la la invocacion al metodo #generarEstructuraComparable() )
 * @param array $datosComparablesViejos
 * @param array $datosComparablesNuevos
 * @param $HCDatos
 */
function procesarHCPresentes($datosComparablesViejos = [], $datosComparablesNuevos = []){
    $HCData = [
        ARCHIVO_VIEJO => [
            'todas'         => array_keys($datosComparablesViejos),

            'repetidas'     => [],                     // hc que presentan repetecion
            'unicas'        => [],                     // hc que no presentan repetecion

            'exclusivas'    => []                      // hc exclusivas de este contexto, no estando presentes en el otro
        ],

        ARCHIVO_NUEVO => [
            'todas'         => array_keys($datosComparablesNuevos),

            'repetidas'     => [],                     // hc que presentan repetecion
            'unicas'        => [],                     // hc que no presentan repetecion

            'exclusivas'    => []                      // hc exclusivas de este contexto, no estando presentes en el otro
        ],

        'compartidas'       => []                      //HC presentes en ambos
    ];

    //HC Compartidas
    $HCData['compartidas'] = array_intersect($HCData[ARCHIVO_VIEJO]['todas'], $HCData[ARCHIVO_NUEVO]['todas']);

    //HC Exclusivas: se calculan sobre la differencia de Todas - Compartidas.
    $HCData[ARCHIVO_VIEJO]['exclusivas'] = array_diff($HCData[ARCHIVO_VIEJO]['todas'], $HCData['compartidas']);
    $HCData[ARCHIVO_NUEVO]['exclusivas'] = array_diff($HCData[ARCHIVO_NUEVO]['todas'], $HCData['compartidas']);

    //Repetidas y unicas
    procesarHCRepetidas(ARCHIVO_VIEJO,$datosComparablesViejos,$HCData);
    procesarHCRepetidas(ARCHIVO_NUEVO,$datosComparablesNuevos,$HCData);

    return $HCData;
}


/**
 * Analiza la estructura y reporta las diferencias y otras observaciones halladas
 * @param array $estructura
 * @param array $respuesta para devolver al
 */
function analizarDatos(&$estructura = array(),&$respuesta = array()){
    $datosViejos = generarEstructuraComparable($estructura[ARCHIVO_VIEJO]['huellas']);
    $datosNuevos = generarEstructuraComparable($estructura[ARCHIVO_NUEVO]['huellas']);

    //Estructura auxiliar para almacenar las Huellas Concretas y facilitar su analisis
    $HCData = procesarHCPresentes($datosViejos,$datosNuevos);
    $reportes = [
        'diferencias' =>[],
        'exclusivos' =>[
            ARCHIVO_NUEVO => [],
            ARCHIVO_VIEJO => []
        ],
        'inconcistencias' =>[
            'repetidos' => [
                ARCHIVO_NUEVO => [],
                ARCHIVO_VIEJO => []
            ],
        ],
    ];

    // Reportes de insumos que presentan diferencias
    foreach($HCData['compartidas'] as $HCComparatida){
        $datoViejo = $datosViejos[$HCComparatida]['stocks'][0];
        $datoNuevo = $datosNuevos[$HCComparatida]['stocks'][0];

        if($datoViejo['stock'] !== $datoNuevo['stock']){
            $reportes['diferencias'][] = [
                'insumo'        => $estructura['datos'][$HCComparatida],
                ARCHIVO_VIEJO   => $datoViejo,
                ARCHIVO_NUEVO   => $datoNuevo
            ];
        }
    }

    // Reporte de insumos  exclusivos en ambos archivos
    if($HCData[ARCHIVO_VIEJO]['exclusivas'] || $HCData[ARCHIVO_NUEVO]['exclusivas']){
        foreach($HCData[ARCHIVO_VIEJO]['exclusivas'] as $HCExclusiva){
            $reportes['exclusivos'][ARCHIVO_VIEJO][] = [
                'insumo'        => $estructura['datos'][$HCExclusiva],
                'datos'   => $datosViejos[$HCExclusiva]['stocks'][0]
            ];
        }

        foreach($HCData[ARCHIVO_NUEVO]['exclusivas'] as $HCExclusiva){
            $reportes['exclusivos'][ARCHIVO_NUEVO][] = [
                'insumo'    => $estructura['datos'][$HCExclusiva],
                'datos'     => $datosNuevos[$HCExclusiva]['stocks'][0]
            ];
        }
    }

    // Reporte de insumos repetidos  en ambos archivos
    if($HCData[ARCHIVO_VIEJO]['repetidas'] || $HCData[ARCHIVO_NUEVO]['repetidas']){
        foreach($HCData[ARCHIVO_VIEJO]['repetidas'] as $HCRepetido){
            $reportes['inconcistencias']['repetidos'][ARCHIVO_VIEJO][] = [
                'insumo'=> $estructura['datos'][$HCRepetido],
                'datos' => $datosViejos[$HCRepetido]['stocks']
            ];
        }
        foreach($HCData[ARCHIVO_NUEVO]['repetidas'] as $HCRepetido){
            $reportes['inconcistencias']['repetidos'][ARCHIVO_NUEVO][] = [
                'insumo'=> $estructura['datos'][$HCRepetido],
                'datos' => $datosNuevos[$HCRepetido]['stocks']
            ];
        }
    }

    $respuesta['reportes'] =$reportes;

    //actualizacion de la data de ambos archivos
    $estructura[ARCHIVO_NUEVO]['data']['huellas'] = count($HCData[ARCHIVO_NUEVO]['todas']);
    $estructura[ARCHIVO_VIEJO]['data']['huellas'] = count($HCData[ARCHIVO_VIEJO]['todas']);

};



//
// MAIN
//

//Respuesta para el frontEnd
$respuesta = [
    'error' => false
];

//Estructura para analisis las diferencias
$estructura = [];

//lectura de params
$archivoViejo = $_FILES['archivoViejo'];
$archivoNuevo = $_FILES['archivoNuevo'];   //$uploadFile['name'];  //Nombre original

try {
    ini_set('memory_limit', '120M');
    ini_set('max_execution_time', '3600');

    //Se chequea posible envio del form sin archivos (ante doble click o doble submit...)
    if(!empty($archivoNuevo['name']) || !empty($archivoViejo['name'])){
        procesarArchivoCSV(ARCHIVO_VIEJO,$archivoViejo, $estructura);
        procesarArchivoCSV(ARCHIVO_NUEVO,$archivoNuevo, $estructura);


        analizarDatos($estructura,$respuesta);

        //Se añade a la respuesta los nombres de los archivos
        $respuesta['archivos'][ARCHIVO_VIEJO] = $estructura[ARCHIVO_VIEJO]['data'];
        $respuesta['archivos'][ARCHIVO_NUEVO] = $estructura[ARCHIVO_NUEVO]['data'];
    }
    else{
        throw new Exception('Faltan cargar los archivos!');
    }

}
catch(Exception $exception){
    generarRespueastaErronea($respuesta,$exception->getMessage());
}

echo json_encode($respuesta);

