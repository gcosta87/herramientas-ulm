<?php
/**
 * Pequeño script que retorna las respuestas para los modales y/o Datatables
 * Basicamente se nuclea las posibles consultas (indicadas via parametro GET) en este script y se generaliza la respuesta segun el caso.
 * /detalleModal.php?consulta=$__CONSULTA__&paramX=a,...
 */
include_once('../core/kernel.php');
$consulta=$_GET['consulta'];
$resultado = array();   //Estructura donde se almacena el resultado de la query a ejecutar

switch ($consulta){
    case 'stock_mr_concreto':   $conStock= (isset($_GET['conStock'])? ($_GET['conStock'] == 'true') :false);
                                $resultado = $db->select('
                                    SELECT		id_concreto,id_generico, presentacion as insumo,envase_primario,envase_hospitalario,
                                                sum(stock_actual) as sum_stock_actual,
                                                programa,
                                                count(fila) as cantidad_registros, group_concat(fila) as filas_involucradas
                                                
                                    FROM		mr_stock_desnormalizado
                                    group by	id_concreto, presentacion,programa,envase_primario,envase_hospitalario
                                    '.
                                    ($conStock? ' having 		sum_stock_actual > 0' : '').
                                    ' order by	presentacion,envase_hospitalario
                               ');
                                break;
}

//Respuesta como la espera el Datatable
$respuesta = ['data' => $resultado];
echo json_encode($respuesta);
