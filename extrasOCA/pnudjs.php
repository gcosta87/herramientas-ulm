<?php include_once('../core/config.php'); ?>
<?php include_once('../core/header.php'); ?>

        <div class="page-header">
          <div class="row">
            <div class="col-md-12">
              <h1 id="buttons">
                <i class="fa fa-medkit fa-fw" aria-hidden="true"></i> PNUD.js <small>Complemento del navegador para mejorar la interfaz del PNUD</small>
              </h1>
            </div>
          </div>
        </div>
        <div class="alert alert-warning">
          <strong>Es un borrador!</strong>
          <br/>No hay nada implementado aún... Se necesita hacer pruebas y un pequeño analisis!
        </div>
        <div class="well">
          <h1>Funciones:</h1>
          <ul>
            <li>Lograr que el PNUD sea utilizable a través del Google Chrome (siempre y cuando sea posible).</li>
            <li>Mejorar algunos componentes de su interfaz, como son los selectores (<select><option>A</option><option>B</option><option>C</option></select>).</li>
          </ul>
        </div>

        <h2>Instalacion</h2>
        <p>Se requiere seguir los siguientes pasos:</p>
        <div class="row">
          <div class="col-md-6">
            <div class="well well-sm clearfix">
              <h4>01 - Instalar complemento para Google Chrome!</h4>
              <p>bla bla blaaa</p>
              <a href="https://chrome.google.com/webstore/detail/dhdgffkkebhmkfjojejmpbldmpobfkfo" class="btn btn-primary btn-sm pull-right" target="_blank" role="button">Añadir extensión al Google Chrome</a>
            </div>
          </div>

          <div class="col-md-6">
            <div class="well well-sm clearfix">
              <h4>02 - Instalar PNUD.js</h4>
              <p>bla bla blaaa</p>
              <a href="http://herramientas.ulm/data/pnudjs/PNUD.user.js" class="btn btn-primary btn-sm pull-right"  role="button">Instalar <i class="fa fa-medkit"></i> PNUD.js</a>
            </div>
          </div>

        </div>



<?php include_once('../core/footer.php'); ?>
