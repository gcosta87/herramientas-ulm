<?php
include_once('../core/kernel.php');
include_once('../core/config.php');
include_once('../core/header.php');
use Models\PNUDInsumo;
use Models\PNUDPresentacion;
use Models\PNUDProveedor;
?>
<div class="page-header">
    <div class="row">
        <div class="col-md-12">
            <h1 id="buttons"><i class="fa fa-magic fa-fw" aria-hidden="true"></i> Asistente de Codigos PNUD <small>Generar e interpretar códigos PNUD</small></h1>
        </div>
    </div>
</div>
<h2><i class="fa fa-pencil-square-o"></i> Generar código <small>Codificar</small></h2>
<p class="lead">Genere un código en base a los valores provistos.</p>
<form class="form-horizontal" name="formularoCodificacion">
    <div class="form-group">
        <label class="col-sm-2 control-label">Insumo</label>
        <div class="col-sm-10">
            <select id="selectorInsumo" data-placeholder="Seleccione un insumo.." class="select">
                <option value="________"></option>
<?php
                $insumos = PNUDInsumo::orderBy('nombre_oca')->get();
                foreach ($insumos as $insumo){
                    echo '<option value="'.$insumo->codigo_oca_corto.'">'.$insumo->nombre_oca.'</option>';
                }
?>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Presentacion</label>
        <div class="col-sm-10">
            <select id="selectorPresentacion" data-placeholder="Seleccione una presentación.." class="select">
                <option value="__"></option>
<?php
                $insumos = PNUDPresentacion::orderBy('nombre')->get();
                foreach ($insumos as $insumo){
                    echo '<option value="'.str_pad($insumo->id,2,0,STR_PAD_LEFT).'">'.$insumo->nombre.'</option>';
                }
?>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Programa (ficticio) PNUD</label>
        <div class="col-sm-10">
            <select id="selectorPrograma" data-placeholder="Seleccione un programa..." class="select">
                <option value="__"></option>
                <option value="14">PMI</option>
                <option value="16">Patologías Prevalentes</option>
            </select>
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-2 control-label">Proveedor</label>
        <div class="col-sm-10">
            <select id="selectorProveedor" data-placeholder="Seleccione un proveedor.." class="select">
                <option value="___"></option>
<?php
                $insumos = PNUDProveedor::orderBy('nombre')->get();
                foreach ($insumos as $insumo){
                    echo '<option value="'.str_pad($insumo->id,3,0,STR_PAD_LEFT).'">'.$insumo->nombre.'</option>';
                }
?>
            </select>
        </div>
    </div>
    <br/>

    <div class="row">
        <label class="col-sm-2 col-sm-offset-2 control-label">Código generado</label>
        <div class="col-sm-4">
            <input class="form-control" type="text" id="codigoGenerado" size="15" style="font-family: monospace; font-size:155%;padding:2px;letter-spacing: 3px;" readonly value=""/>
        </div>
        <div class="col-sm-2 ">
            <a role="button" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Copia el codigo al portapapeles" onclick="copiar()"><i class="fa fa-clipboard fa-fw"></i></a>
        </div>
    </div>

    <br/>
    <br/>
    <br/>

    <div class="row">
        <div class="col-sm-2 col-sm-offset-4">
            <a role="button" class="btn btn-primary"  onclick="generarCodigo()"><i class="fa fa-pencil-square-o fa-fw"></i> Generar</a>
        </div>
        <div class="col-sm-1">
            <a role="button" class="btn btn-danger btn-sm" onclick="reInicializarSelectores()">Restablecer</a>
        </div>
    </div>
</form>

<br/><br/>
<h2><i class="fa fa-eyedropper"></i> Interpretar código <small>Decodificar</small></h2>
<p class="lead">Escriba un código PNUD para conocer su interpretación.</p>
<form class="form-horizontal" name="formularoDecodificacion">
    <div class="form-group">
        <label class="col-sm-2 col-sm-offset-2 control-label">Código PNUD</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="codigo" maxlength="15" size="15" style="text-transform: uppercase;font-family: monospace; font-size:155%;letter-spacing: 3px;padding: 2px;" placeholder="M3C4NS0F14C0000"/>
        </div>
    </div>
    <br/>
    <div class="row container-fluid">

        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">Insumo</div>
                <div class="panel-body" id="decodificar_insumo"></div>
            </div>
        </div>

        <div class="col-sm-4 col-sm-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Presentacion</div>
                <div class="panel-body" id="decodificar_presentacion"></div>
            </div>
        </div>

        <div class="col-sm-4 col-sm-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Programa PNUD</div>
                <div class="panel-body" id="decodificar_programa"></div>
            </div>
        </div>

        <div class="col-sm-4 col-sm-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Proveedor</div>
                <div class="panel-body" id="decodificar_proveedor"></div>
            </div>
        </div>

    </div>
    <br/>
    <br/>

    <div class="row">
        <div class="col-sm-2 col-sm-offset-4">
            <a role="button" class="btn btn-primary" onclick="interpretarCodigo()"><i class="fa fa-eyedropper fa-fw"></i> Interpretar</a>
        </div>
        <div class="col-sm-1">
            <a role="button" class="btn btn-danger btn-sm" onclick="reInicializarInterpetacion()">Restablecer</a>
        </div>
    </div>
</form>






<script>
//
//  CODIFICACION
//
function inicializarSelector(selector){
    $(selector).chosen({
        no_results_text: "No hay resultados... :(",
        allow_single_deselect: true
    });
}

/**
 * Se inicializa los selectores
 */
function inicializarSelectores(){
    inicializarSelector('#selectorInsumo');
    inicializarSelector('#selectorProveedor');
    inicializarSelector('#selectorPresentacion');
    inicializarSelector('#selectorPrograma');
}

/**
 * Se reinicializan los Selectores con Choseen asociados
 */
function reInicializarSelectores(){
    $('#selectorInsumo').val('________').trigger("chosen:updated");
    $('#selectorProveedor').val('___').trigger("chosen:updated");
    $('#selectorPresentacion').val('__').trigger("chosen:updated");
    $('#selectorPrograma').val('__').trigger("chosen:updated");

    $('#codigoGenerado').val('');
}


/**
 * Muestra el codigo generado a partir de los valores de los selectores
 */
function generarCodigo() {
    var codigo = $('#selectorInsumo').val() + $('#selectorPresentacion').val() + $('#selectorPrograma').val() + $('#selectorProveedor').val() ;
    $('#codigoGenerado').val(codigo);
}

function copiar(){
    $('#codigoGenerado').select();
    document.execCommand("Copy");
}



//
//  DECODIFICACION
//

function reInicializarInterpetacion() {
    $('#codigo').val('');

    $('#decodificar_insumo').text('');
    $('#decodificar_presentacion').text('');
    $('#decodificar_programa').text('');
    $('#decodificar_proveedor').text('');
}

function interpretarCodigo() {
    var codigo = $('#codigo').val();

    var resultadoInsumo         = $('#selectorInsumo option[value="'+codigo.substring(0,8)+'"]').text();
    var resultadoPresentacion   = $('#selectorPresentacion option[value="'+codigo.substring(8,10)+'"]').text();
    var resultadoPrograma       = $('#selectorPrograma option[value="'+codigo.substring(10,12)+'"]').text();
    var resultadoProveedor      = $('#selectorProveedor option[value="'+codigo.substring(12,15)+'"]').text();



    $('#decodificar_insumo').text(resultadoInsumo == '' ? '(desconocido)' : resultadoInsumo);
    $('#decodificar_presentacion').text(resultadoPresentacion == '' ? '(desconocida)' : resultadoPresentacion);;
    $('#decodificar_programa').text(resultadoPrograma == '' ? '(desconocido)' : resultadoPrograma);
    $('#decodificar_proveedor').text(resultadoProveedor == '' ? '(desconocido )' : resultadoProveedor);
}
    $(function(){
        inicializarSelectores();
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<?php include_once('../core/footer.php'); ?>
