// ==UserScript==
// @name         PNUD.js
// @namespace    http://herramientas.ulm/
// @website      http://herramientas.ulm/pnudjs/
// @updateURL    http://herramientas.ulm/data/pnudjs/PNUD.meta.js
// @downloadURL  http://herramientas.ulm/data/pnudjs/PNUD.user.js
// @version      0.04
// @description  Mejora de la Interfaz del PNUD-OCA
// @author       Gon
// @iconURL      http://herramientas.ulm/data/pnudjs/pnud.png
// @require      https://code.jquery.com/jquery-2.1.4.min.js
// @require      https://code.jquery.com/ui/1.12.1/jquery-ui.js
// @resource     estilo http://herramientas.ulm/data/pnudjs/estilo.css
// @resource     jqueryui_css http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css
// @noframes
// @match        http://ias01.ms.gba.gov.ar:7778/logistica/*
// @grant        GM_notification
// @grant        GM_addStyle
// @grant        GM_log
// @grant        GM_getResourceText
// ==/UserScript==

////////////////////////////
// CONSTANTES
////////////////////////////
const URL_LOGIN = '/logistica/login.jsp';
const GMAIL_LINK_COMPOSE= 'https://mail.google.com/mail/?view=cm&fs=1&to=gonzalez.pedro@oca.com.ar&su=Insumos';
const MENU_HTML =' \
    <div id="pnud-panel">\
        <div class="pnud-header">\
            <i class="fa fa-medkit"></i> PNUD.js\
        </div>\
        <div id="pnud-opciones">\
            <a href="'+GMAIL_LINK_COMPOSE+'" target="_blank" class="opcion" title="Enviar mail a Pedro (via Gmail)"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>\
            <a href="#" class="opcion" title="Copiar tabla al portapapeles"><i class="fa fa-table" aria-hidden="true"></i></a>\
        </div>\
        <br style="clear: both;">\
    </div>';

////////////////////////////
//  FUNCIONES
////////////////////////////

/**
 inicializacion requerida: añadimiento de estilo,etc...
 */
function init(){
    $('head').append('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">');
    GM_addStyle(GM_getResourceText('estilo'));
    GM_addStyle(GM_getResourceText('jqueryui_css'));

    if(window.location.pathname == URL_LOGIN){
        GM_notification("Activado");
    }
    $('body').append(MENU_HTML);
}




////////////////////////////
// MAIN
////////////////////////////

(function() {
    init();
})();