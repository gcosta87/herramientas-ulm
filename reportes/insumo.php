<?php
include_once('../core/kernel.php');
include_once('../core/config.php');
include_once('../core/header.php');
use Models\Insumo;
?>
        <div class="page-header">
          <div class="row">
            <div class="col-md-12">
              <h1 id="buttons">
                <i class="fa fa-database fa-fw" aria-hidden="true"></i> Insumo <small>Detalles de la información registrada en la Base de Datos</small>
              </h1>
            </div>
          </div>
        </div>

<?php
    $insumoId = $_GET['codigoULM'];
    $insumo  = Insumo::find($insumoId);
try{
?>
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#informacion" aria-controls="informacion" role="tab" data-toggle="tab">Información</a></li>
    <li role="presentation"><a href="#stock" aria-controls="stock" role="tab" data-toggle="tab">Stock</a></li>
    <li role="presentation"><a href="#movimientos" aria-controls="movimientos" role="tab" data-toggle="tab">Movimientos</a></li>
    <li role="presentation"><a href="#relacionados" aria-controls="relacionados" role="tab" data-toggle="tab">Relacionados</a></li>
</ul>
<div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="informacion">
        <h2>Información básica</h2>
        <table class="table table-striped table-hover" >
            <tr>
                <td width="150px;"><strong>Codigo ULM:</strong></td>
                <td><?php echo $insumo->id;?></td>
            </tr>
            <tr>
                <td width="150px;"><strong>Codigo OCA:</strong></td>
                <td><?php echo $insumo->codigo_oca;?></td>
            </tr>
            <tr>
                <td width="150px;"><strong>Insumo genérico:</strong></td>
                <td><?php echo $insumo->PNUDInsumo->nombre_oca;?> <small style="margin-left: 20px;">(<?php echo $insumo->codigo_oca_corto;?>)</small></td>
            </tr>
            <tr>
                <td width="150px;"><strong>Presentacion:</strong></td>
                <td><?php echo $insumo->PNUDpresentacion->nombre;?></td>
            </tr>
            <tr>
                <td width="150px;"><strong>Programa:</strong></td>
                <td>
                    <?php
                    foreach($insumo->programas as $programa){
                        echo '<a style="margin-right: 10px;"href="/programas/reporte.php?id='.$programa->id.'" title="Ver reporte del programa">'.$programa.'</a>';
                    }
                    ?>
                </td>
            </tr>
        </table>

    </div>

    <div role="tabpanel" class="tab-pane fade in " id="stock">

        <h2>Stock <small>Información asociada al stock</small></h2>
<?php
            $stockULM = $db->select("
                  select	stock as cajas, ifnull(stock_unidades,'(sin info)') as unidades
                  from	    v_stock_ulm_actual
                  where     insumo_id = $insumoId
            ");

            $stockOCA = $db->select("
                  select	stock as cajas, ifnull(stock_unidades,'(sin info)') as unidades
                  from	    v_stock_oca_actual
                  where     insumo_id = $insumoId
            ");
?>

        <div class="row">
            <div class="col-md-4 col-md-offset-1 valign">
                <div class="panel panel-default estadisticas">
                    <div class="panel-body">
                        <div class="cifras">
                        <?php
                            if($stockULM) {
                                $stockULM = $stockULM[0];
                        ?>
                            <span class="cifra" style="color: dodgerblue !important;" data-toggle="tooltip"
                                  data-placement="top" title="Cajas"><?php echo $stockULM->cajas; ?></span>
                            /
                            <span class="cifra" data-toggle="tooltip" data-placement="top"
                                  title="Unidades"><?php echo $stockULM->unidades; ?></span>

                        <?php
                            }
                            else{
                                echo 'Sin info';
                            }
                        ?>
                        </div>
                    </div>
                    <div class="panel-footer">Stock ULM <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Stock calculado al dia de hoy"></i></div>
                </div>
            </div>

            <div class="col-md-4 col-md-offset-2 valign">
                <div class="panel panel-default estadisticas">
                    <div class="panel-body">
                        <div class="cifras">
                        <?php
                            if($stockOCA) {
                                $stockOCA = $stockOCA[0];
                        ?>
                            <span class="cifra" style="color: dodgerblue !important;" data-toggle="tooltip" data-placement="top" title="Cajas"><?php echo $stockOCA->cajas;?></span>
                            /
                            <span class="cifra" data-toggle="tooltip" data-placement="top" title="Unidades"><?php echo $stockOCA->unidades;?></span>
                        <?php
                            }
                            else{
                                echo 'Sin info';
                            }
                        ?>
                        </div>
                    </div>
                    <div class="panel-footer">Stock OCA <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="El último reportado por OCA"></i></div>
                </div>
            </div>
        </div>
<br/>
        
        <h2>Histórico de Stock OCA <small>Reportados por el Operador logistico</small></h2>
<?php
       $stockHistoricoOCA = $insumo->stockOCA()->orderBy('fecha','desc')->take(10)->get();
        if($stockHistoricoOCA->count() > 0){
?>
        <p>Últimos 10 registros:</p>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Fecha</th>
                <th>Cajas</th>
            </tr>
            </thead>
            <?php

            foreach ($stockHistoricoOCA as $stockHistorico) {
                echo '<tr>';
                echo '    <td width="100px;">' . $stockHistorico->fecha . '</td>';
                echo '    <td width="100px;">' . $stockHistorico->total . '</td>';
                echo '</tr>';
            }
            ?>
        </table>

<?php

        }
        else{
            echo '<p>No hay información en el stock reportado por OCA.</p>';
        }
?>

        <br/>
        <h2>Valores (históricos) de Stock ULM <small>Valores utilizados para el calculo del stock actual</small></h2>
        <p>El valor mas reciente se utiliza para calcular el stock actual en base a los movimientos del insumo (a partir de esa fecha).</p>
<?php
       $stockHistoricoULM = $db->select("
            select  fecha as fecha,
                    total as total                    
            from	stock_ulm_oca
            where   insumo = $insumoId 
            order by fecha desc
       ");
        if($stockHistoricoULM){
?>
        <p>Últimos 10 registros:</p>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Fecha</th>
                <th>Cajas</th>
            </tr>
            </thead>
            <?php

            foreach ($stockHistoricoULM as $stockHistorico) {
                echo '<tr>';
                echo '    <td width="100px;">' . $stockHistorico->fecha . '</td>';
                echo '    <td width="100px;">' . $stockHistorico->total . '</td>';
                echo '</tr>';
            }
            ?>
        </table>
<?php
        }
        else{
            echo '
                <div class="alert alert-warning alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>No hay información en el Stock ULM!</strong> Debería agregarselo si es un insumo nuevo o activo (utilizado en los ultimos meses).
                </div>
            ';
        }
?>
        
    </div>
    <div role="tabpanel" class="tab-pane fade in" id="movimientos">

        <h2>Movimientos <small>Información registrada en los DMS/RMS</small></h2>
<?php
            $lineasDMS = $insumo->lineasDMS()->orderBy('fecha_archivo','desc')->take(50)->get();
            $lineasRMS = $insumo->lineasRMS()->orderBy('fecha_archivo','desc')->take(15)->get();

?>
        <div class"row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-sign-in"></i> Últimos 15 ingresos <small>(Lineas RMS)</small></div>
                    <div class="panel-body">
                        <table class="dataTableMovimiento">
                            <thead>
                                <tr>
                                    <th>Cantidad</th>
                                    <th>Fecha</th>
                                    <th>Archivo</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
                            foreach ($lineasRMS as $registro){
                                echo '<tr>';
                                echo '    <td>'.$registro->cantidad. '</td>';
                                echo '    <td>'.$registro->fecha_archivo. '</td>';
                                echo '    <td>RMS'.$registro->referencia. '</td>';
                                echo '</tr>';
                            }
?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-sign-out"></i> Últimos 50 egresos <small>(Lineas DMS)</small></div>
                    <div class="panel-body">
                        <table class="dataTableMovimiento">
                            <thead>
                            <tr>
                                <th>Pedido</th>
                                <th>Cantidad</th>
                                <th>Fecha</th>
                                <th>Archivo</th>
                            </tr>
                            </thead>
                            <tbody>
<?php
                            foreach ($lineasDMS as $registro){
                                echo '<tr>';
                                echo '    <td>'.$registro->pedido_numero. '</td>';
                                echo '    <td>'.$registro->cantidad. '</td>';
                                echo '    <td>'.$registro->fecha_archivo. '</td>';
                                echo '    <td>DMS'.$registro->referencia. '</td>';
                                echo '</tr>';
                            }
?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </div>
    </div>

    <div role="tabpanel" class="tab-pane fade in " id="relacionados">
        <h2>Relacionados <small>Insumos similares</small></h2>
<?php
    $presentacionesAlternativas = $db->select("
        select	id as insumo_id,
                codigo_oca as codigo_oca,
                presentacion as presentacion
        from 	v_insumos
        where	pnud_insumo_id = (select pnud_insumo_id from v_insumos where id = $insumoId)
                and
                id <> $insumoId
        order by presentacion
    ");

    $similares = $db->select("
        select	id as insumo_id,
                codigo_oca as codigo_oca,
                nombre as insumo,
                presentacion as presentacion
        from 	v_insumos
        where	id <> $insumoId
                and 
                pnud_insumo_id <> (select pnud_insumo_id from v_insumos where id = $insumoId)
                and
                (	nombre = trim((select nombre from v_insumos where id = $insumoId))
                    or
                    nombre like concat('%',substring_index((select nombre from v_insumos where id = $insumoId),' ',1),'%')
                )
        order by presentacion

    ");

?>
    <h3>Presentaciones alternativas <small>Otras presentaciones del insumo genérico</small></h3>
    <table class="table table-striped table-hover" width="60%">
        <thead>
            <tr>
                <th>Codigo ULM</th>
                <th>Codigo OCA</th>
                <th>Presentacion</th>
                <th>Analizar</th>
            </tr>
        </thead>
<?php
    foreach ($presentacionesAlternativas as $alternativa) {
        echo '<tr>';
        echo '    <td width="100px">' .$alternativa->insumo_id.'</td>';
        echo '    <td width="150px">' .$alternativa->codigo_oca.'</td>';
        echo '    <td width="250px">' .$alternativa->presentacion.'</td>';
        echo '    <td width="50px"><a role="button" title="Analizar la información del insumo registrada en la base de datos" href="/reportes/insumo.php?codigoULM='.$alternativa->insumo_id.'"><i class="fa fa-database"></a></td>';

        echo '</tr>';
    }

?>
    </table>

    <br/>
    <h3>Insumos similares <small>Busqueda aproximada de insumos con nombre similar</small></h3>
    <div class="alert alert-warning alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <strong>Esta función es experimetal!</strong> No es perfecta, solo hace busquedas simples. Para mejores resultados es preferible buscar a mano <i class="fa fa-smile-o" aria-hidden="true"></i>
    </div>
    <table class="table table-striped table-hover" width="60%">
        <thead>
            <tr>
                <th>Codigo ULM</th>
                <th>Codigo OCA</th>
                <th>Nombre</th>
                <th>Presentacion</th>
                <th>Analizar</th>
            </tr>
        </thead>
<?php
    foreach ($similares as $similar) {
        echo '<tr>';
        echo '    <td width="100px">' .$similar->insumo_id.'</td>';
        echo '    <td width="150px">' .$similar->codigo_oca.'</td>';
        echo '    <td width="250px">' .$similar->insumo.'</td>';
        echo '    <td width="250px">' .$similar->presentacion.'</td>';
        echo '    <td width="50px"><a role="button" title="Analizar la información del insumo registrada en la base de datos" href="/reportes/insumo.php?codigoULM='.$similar->insumo_id.'"><i class="fa fa-database"></a></td>';

        echo '</tr>';
    }

?>
    </table>

    </div>

</div>  <!-- fin del conjunto de tabs-->


<script>
    $(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('.dataTableMovimiento').DataTable({
            language: {
                url: '/assets/DataTables/Spanish.json'
            },
            "dom": 'lfrtip',
            "pageLength": 10,
        });
    });
</script>
<?php
    }
    catch(Exception $e){
        echo '<h2>Se ha producido un error <small>Es un bajón!!!  <i class="fa fa-frown-o"></i></small></h2><br/><strong>Motivo</strong>: '.$e->getMessage();
    }

    include_once('../core/footer.php');
?>
