<?php
include_once('../core/kernel.php');
include_once('../core/config.php');
include_once('../core/header.php');

?>

        <div class="page-header">
          <div class="row">
            <div class="col-md-12">
              <h1 id="buttons">
                <i class="fa fa-exclamation-triangle fa-fw" aria-hidden="true"></i> Vencimientos <small>Reportes sobre los vencimientos de insumos</small>
              </h1>
            </div>
          </div>
        </div>

<?php

try{
    $fecha      =    $db->select('
                        	select	date_format(max(fecha),\'%d/%m/%Y\') as fecha
                        	from	stock_oca_discriminado
                    ')[0]->fecha;
?>
<h2>Vencimiento de Lotes (<?php echo $fecha;?>)</h2>
    <p>Se detalla por lote sus fechas de vencimiento correspondientes.</p>
    <p>Referencia de colores asociados a fechas de vencimientos.</p>
    <ol>
        <li><span style="background-color: black" title="Negro">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>: menor a 1 mes</li>
        <li><span style="background-color: red" title="Rojo">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>: 1 mes a 3 meses</li>
        <li><span style="background-color: darkorange" title="Naranja">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>: 3 meses a 6 meses</li>
        <li><span style="background-color: gold" title="Amarillo">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>: 6 meses a 12 meses</li>
        <li><span style="background-color: yellowgreen" title="Verde">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>: más de 1 año</li>
    </ol>
    <table id="tabla" width="100%">
        <thead>
        <tr>
            <th>Codigo OCA</th>
            <th>Insumo</th>
            <th>Presentacion</th>
            <th>Lote</th>
            <th>Cajas</th>
            <th>Unidades</th>
            <th>Vencimiento</th>
            <th>Vence en (meses)</th>
            <th>Programa</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>Codigo OCA</th>
            <th>Insumo</th>
            <th>Presentacion</th>
            <th>Lote</th>
            <th>Cajas</th>
            <th>Unidades</th>
            <th>Vencimiento</th>
            <th>Vence en (meses)</th>
            <th>Programa</th>
        </tr>
        </tfoot>
    </table>

<script src="/assets/funciones.js"></script>
<script>
    configuracion = {
        "pageLength": 50,
        "lengthMenu": [ 50, 75, 100, 150,200,400,500],
        "ajax":    '/stockoca/detalleModal.php?consulta=reporteVencimientos',
        autoWidth: false,
        "columns": [
            { "data": "codigo_oca"},
            { "data": "insumo"},
            { "data": "presentacion"},
            { "data": "lote"},
            { "data": "stock"},
            { "data": "stock_unidades"},
            { "data": "vencimiento"},
            { "data": "venceEnMeses"},
            { "data": "programa"}
        ],
        "order": [[ 7, "asc" ]],
        "columnDefs": [ {
            "targets": 3,
            "createdCell": function (td, cellData, rowData, row, col) {
                switch(rowData.alerta){
                    case 1:   $(td).css({'color': 'lightgray', 'background-color': 'black'});
                        break;

                    case 2:   $(td).css({'color': 'black', 'background-color': 'red'});
                        break;

                    case 3:   $(td).css({'color': 'black', 'background-color': 'darkorange'});
                        break;

                    case 4:   $(td).css({'color': 'black', 'background-color': 'gold'});
                        break;

                    case 5:   $(td).css({'color': 'black', 'background-color': 'YellowGreen'});
                        break;
                }
            }
        }]
    };
    DataTableULM('#tabla','Reporte de Vencimientos en OCA',configuracion);

</script>
<?php
    }
    catch(Exception $e){
        echo '<h2>Se ha producido un error <small>Es un bajón!!!  <i class="fa fa-frown-o"></i></small></h2><br/><strong>Motivo</strong>: '.$e->getMessage();
    }

    include_once('../core/footer.php');
?>
