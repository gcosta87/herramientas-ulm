<?php
include_once('../core/kernel.php');
include_once('../core/config.php');
include_once('../core/header.php');

?>

        <div class="page-header">
          <div class="row">
            <div class="col-md-12">
              <h1 id="buttons">
                <i class="fa fa-tasks fa-fw" aria-hidden="true"></i> Stock en OCA Simplificado <small>Reporte que emula el archivo excel</small>
              </h1>
            </div>
          </div>
        </div>

<?php

try{
    $fecha      =    $db->select('
                        	select	date_format(max(fecha),\'%d/%m/%Y\') as fecha
                        	from v_stock_oca_actual
                    ')[0]->fecha;
?>
<h2>Stock en OCA Simplicado (<?php echo $fecha;?>)</h2>
    <p>En base al ultimo reporte de <a href="/stockoca/" target="_blank">Stock en OCA</a>, se agrupan los insumos ignorando su presentacion, para emular el formato del Excel <strong>Stock ULM OCA.xlsx</strong>.</p>
    <table id="tabla" width="90%">
        <thead>
        <tr>
            <th>Código OCA</th>
            <th>Insumo</th>
            <th data-filtrar="false">Unidades</th>
            <th data-filtrar="false"><abbr data-toggle="tooltip" data-placement="top" title="Abastecimiento estimado en meses a partir de las unidades de stock indicadas">Abastecimiento</abbr></th>
            <th>Programa</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th width="12%">Código OCA</th>
            <th>Insumo</th>
            <th width="15%">Unidades</th>
            <th width="15%">Abastecimiento</th>
            <th width="15%">Programa</th>
        </tr>
        </tfoot>
    </table>
<script src="/assets/funciones.js"></script>
<script>
    var configuracion = {
        "ajax":    '/stockoca/detalleModal.php?consulta=stockOCASimplificado',
        "columns": [
            { "data": "codigo_oca_corto"},
            { "data": "insumo"},
            { "data": "stock_unidades_total"},
            { "data": "stock_abastecimiento_meses"},
            { "data": "programa"}
        ],
        "order": [[ 1, "asc" ]],
        "columnDefs": [
            {
                "targets": 3,
                "createdCell": function (td, cellData, rowData, row, col) {
                    if(rowData.stock_mensual_unidades > 0){
                        $(td).prop('title','Consumo mensual configurado '+rowData.stock_mensual_unidades);
                        if((cellData >=0 ) && (cellData <=4)){
                            $(td).css({'color': 'black', 'background-color': 'red'});
                        }
                        else{
                            if((cellData > 4) && (cellData <=8)){
                                $(td).css({'color': 'black', 'background-color': 'gold'});
                            }
                            else{
                                $(td).css({'color': 'black', 'background-color': 'yellowgreen'});
                            }
                        }
                    }
                }
            }
        ]
    };
    DataTableULM('#tabla','Stock en OCA Simplificado',configuracion);
</script>
<?php
    }
    catch(Exception $e){
        echo '<h2>Se ha producido un error <small>Es un bajón!!!  <i class="fa fa-frown-o"></i></small></h2><br/><strong>Motivo</strong>: '.$e->getMessage();
    }

    include_once('../core/footer.php');
?>
