<?php
/**
 * Se nuclean en un solo script varias (posibles) consultas via ajax y se retorna el resultado en un Objeto JSON simple
 * Ejemplo de uso:
 *  /programas/consultasAjax.php?consulta=NOMBRE_CONSULTA[&param1=xxxx&param2=xxxx...]
 *
 * nota: este script está basado en detalleModal.php
 */
include_once('../core/kernel.php');

$consulta=$_GET['consulta'];

$resultado = array();   //Estructura donde se almacena el resultado de la query a ejecutar
$error = true;

switch ($consulta){
    case 'consultaDestinosInsumo':      $pnudInsumoId   = $_GET['pnudInsumoId'];
                                        $programaId     = $_GET['programaId'];

                                        $resultado = $db->select("
                                                select	i.pnud_insumo as pnud_insumo,
                                                        pi.nombre_oca as insumo,
                                                        des.nombre_corto as destino,
                                                        (sum(dms.cantidad * pres.multiplicador)) as unidades_totales

                                                from	PROGRAMAS p 
                                                        left join INSUMOS_PROGRAMA ip
                                                        on (p.id = $programaId and p.id = ip.programa)
                                                        left join insumos i
                                                        on (i.pnud_insumo = $pnudInsumoId and ip.insumo = i.id)
                                                        left join LINEA_DMS dms
                                                        on (dms.codigo_oca = i.codigo_oca)
                                                        left join pnud_insumos pi
                                                        on (pi.codigo_oca_corto = i.codigo_oca_corto)
                                                        left join pnud_presentaciones pres
                                                        on (pres.id = i.presentacion)
                                                        left join LINEA_CMS cms
                                                        on (dms.pedido_numero = cms.pedido_numero)
                                                        left join pnud_destinos des
                                                        on (cms.destino = des.id)

                                                where	dms.fecha_archivo >= (curdate() - interval 30 day)

                                                group by	i.pnud_insumo, insumo, des.id
                                                order by	destino
                                        ");
                                        $error = false;
                                        break;
}

$respuesta = [
                'respuesta' => $resultado,
                'error'     => $error
            ];

echo json_encode($respuesta);
