<?php
include_once('../core/kernel.php');
include_once('../core/config.php');
include_once('../core/header.php');

use Models\Programa;
use Models\Insumo;
use Models\PNUDPresentacion;

?>

        <div class="page-header">
          <div class="row">
            <div class="col-md-12">
              <h1 id="buttons">
                <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i> Programas <small>Reportes sobre los diversos programas</small>
              </h1>
            </div>
          </div>
        </div>

<?php

try{
    $programaId = $_GET['id'];
    $programa = Programa::find($programaId);
?>

<a href="/programas/" role="button" class="btn btn-primary btn-sm pull-right" title="Volver a la página de selección del programa">Volver</a>
<h2>Reporte del programa <strong><?php echo $programa->nombre;?></strong></h2>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#stock" aria-controls="stock" role="tab" data-toggle="tab">Stock</a></li>
        <li role="presentation"><a href="#movimientos" aria-controls="movimientos" role="tab" data-toggle="tab">Últimos movimientos del depósito</a></li>
        <li role="presentation"><a href="#destinos" aria-controls="destinos" role="tab" data-toggle="tab">Distribuición a las Regiones y otros</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="stock">
            <div class="row">
                <div class="col-md-6">
                    <h3>Insumos <small>Reportados por el operador logístico</small></h3>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Insumo</th>
                                <th>Presentacion</th>
                                <th>Unidades</th>
                            </tr>
                        </thead>
                        <tbody>
            <?php
                $stockInsumos = $db->select("
                    select	nombre_oca as insumo,
                            presentacion as presentacion,
                            stock_unidades as cantidad_unidades
                    from	v_stock_oca_actual
                    where 	programa_id = $programaId
                    order by insumo, cantidad_unidades
                ");
                foreach ($stockInsumos as $stockInsumo){
                    echo '<tr>';
                    echo '<td>'.$stockInsumo->insumo.'</td>';
                    echo '<td>'.$stockInsumo->presentacion.'</td>';
                    echo '<td>'.$stockInsumo->cantidad_unidades.'</td>';

                    echo '</tr>';
                }
            ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <h3>Composición de stock <small>Representación visual</small></h3>
                    <div id="graficoDeStock" style="width: 900px; height: 500px;">
                        <div class="alert alert-warning alert-dismissible fade in hidden" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">×</span></button>
                            <strong>No hay datos!</strong>.
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane fade" id="movimientos">
<?php
        $ingresos = $db->select("            
            select	pi.nombre_oca as insumo,
                    (sum(rms.cantidad * pres.multiplicador)) as unidades_totales
                    
            from	PROGRAMAS p 
                    left join INSUMOS_PROGRAMA ip
                    on (p.id = $programaId and p.id = ip.programa)
                    left join insumos i
                    on (ip.insumo = i.id)
                    left join LINEA_RMS rms
                    on (rms.codigo_oca = i.codigo_oca)
                    left join pnud_insumos pi
                    on (pi.codigo_oca_corto = i.codigo_oca_corto)
                    left join pnud_presentaciones pres
                    on (pres.id = i.presentacion)
                    
            where rms.fecha_archivo >= (curdate() - interval 30 day)
            group by i.pnud_insumo
            order by insumo
        ");

        $egresos = $db->select("
            select	i.pnud_insumo as pnud_insumo,
                    pi.nombre_oca as insumo,
                    (sum(dms.cantidad * pres.multiplicador)) as unidades_totales
                    
            from	PROGRAMAS p 
                    left join INSUMOS_PROGRAMA ip
                    on (p.id = $programaId and p.id = ip.programa)
                    left join insumos i
                    on (ip.insumo = i.id)
                    left join LINEA_DMS dms
                    on (dms.codigo_oca = i.codigo_oca)
                    left join pnud_insumos pi
                    on (pi.codigo_oca_corto = i.codigo_oca_corto)
                    left join pnud_presentaciones pres
                    on (pres.id = i.presentacion)
                    
            where dms.fecha_archivo >= (curdate() - interval 30 day)
            group by i.pnud_insumo
            order by insumo
        ");
?>
            <div class="row">
                <div class="col-md-12">
                    <h3>Ultimos movimientos en el deposito de OCA</h3>
                    <p>Se muestran los movimientos, totalizando las unidades.</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Ingresos de los últimos 30 días</div>
                                <div class="panel-body">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Insumo</th>
                                            <th>Unidades</th>
                                        </tr>
                                        </thead>
<?php
                                    foreach ($ingresos as $ingreso){
                                        echo '<tr>';
                                        echo '<td>'.$ingreso->insumo.'</td>';
                                        echo '<td>'.$ingreso->unidades_totales.'</td>';
                                        echo '</tr>';
                                    }

?>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Egresos de los últimos 30 días</div>
                                <div class="panel-body">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Insumo</th>
                                            <th>Unidades</th>
                                        </tr>
                                        </thead>
<?php
                                    foreach ($egresos as $egreso){
                                        echo '<tr>';
                                        echo '<td><a href="#" onclick="mostrarDetalleDistribucion('.$egreso->pnud_insumo.',\''.$egreso->insumo.'\')" title="Representacion grafica de la distribucion del insumo">'.$egreso->insumo.'</a></td>';
                                        echo '<td>'.$egreso->unidades_totales.'</td>';
                                        echo '</tr>';
                                    }

?>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane fade" id="destinos">
            <p>Se detallan la distribución de los insumos a los diversos destinos de los <strong>últimos 30 días</strong>.</p>
            <table id="tablaDetalleDestinos">
                <thead>
                <tr>
                    <th>Insumo</th>
                    <th>Destino</th>
                    <th>Unidades</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Insumo</th>
                    <th>Destino</th>
                    <th>Unidades</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
<script>
    $('#tablaDetalleDestinos').DataTable({
        language: {
            url: '/assets/DataTables/Spanish.json'
        },
        "dom": 'lfrtip',
        "pageLength": 25,
        "ajax":    '/stockoca/detalleModal.php?consulta=tablaDetalleDestinos&programaId=<?php echo $programaId;?>',
        "columns": [
            { "data": "insumo"},
            { "data": "destino"},
            { "data": "unidades_totales"}
        ]
    });
</script>

<?php
    $reporteComposicionInsumos = $db->select("
        select	codigo_oca_corto,
                nombre_oca as insumo,
                sum(stock_unidades) as unidades
        from	v_stock_oca_actual
        where 	programa_id = $programaId
        group by codigo_oca_corto, insumo
        order by insumo, unidades
    ");
?>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<?php
    if(count($reporteComposicionInsumos) >= 1){
?>
    <script type="text/javascript">

        function renderizarGraficosDestinos(pnud_insumo){

            $.ajax({
                url: "/programas/consultasAjax.php?consulta=consultaDestinosInsumo&pnudInsumoId="+pnud_insumo+"&programaId=<?php echo $programaId; ?>",
                method: 'GET',
                dataType: 'json'
            })
            .fail(function() {
                alertify.error('Fallo la consulta al servidor!. :(');
            })
            .done(function( data ) {
                var registros = [];

                data.respuesta.forEach(function(elemento, index){
                    var registro = [
                        elemento.insumo,
                        elemento.destino,
                        elemento.unidades_totales
                    ]

                    registros.push(registro);
                })


                //Datos del grafico destinos
                var datosDestinos= new google.visualization.DataTable();
                datosDestinos.addColumn('string', 'Insumo');
                datosDestinos.addColumn('string', 'Destino');
                datosDestinos.addColumn('number', 'Cantidad');

                datosDestinos.addRows(registros);
                var opcionesDestinos = {
                    width: 500,
                    height: 900
                };

                var chartDestinos = new google.visualization.Sankey(document.getElementById('graficoDeDestinos'));
                chartDestinos.draw(datosDestinos, opcionesDestinos);
            });
        }

        function mostrarDetalleDistribucion(pnud_insumo, nombreInsumo){
            renderizarGraficosDestinos(pnud_insumo);

            //Se renderiza el modal
            $('#modal_titulo').html('Destinos de '+nombreInsumo);
            $('#modal').modal('show');
        }


        google.charts.load("current", {packages:['corechart','sankey']});
        google.charts.setOnLoadCallback(renderizarGraficos);


        function renderizarGraficos() {
            //Datos del grafico stock
            var datosStock = google.visualization.arrayToDataTable([
                ['Insumo', 'Cantidades'],
<?php

    $datos='';
    foreach ($reporteComposicionInsumos as $detalleInsumo){
        $datos.="['".$detalleInsumo->insumo."',".$detalleInsumo->unidades. '], ';
    }
    //Se remueve el ultimo ",
    $datos=substr($datos,0,-2);
    echo $datos;

?>
            ]);
            var opcionesStock = {
                title: 'Grafico',
                pieHole: 0.2
            };
            var chartStock = new google.visualization.PieChart(document.getElementById('graficoDeStock'));
            chartStock.draw(datosStock, opcionesStock);
        }
    </script>

        <!-- Modal para mostrar grafico de destinos -->
        <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modal_titulo"></h4>
                    </div>
                    <div class="modal-body">
                        <div id="graficoDeDestinos" style="width: 500px; height: 900px;">
                            <div class="alert alert-warning alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">×</span></button>
                                <strong>No hay datos!</strong>.
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>


<?php
        }
        else{
            echo "
                <script>
                    $('#graficoDeStock .alert').removeClass('hidden');
                </script>
            ";
        }
    }
    catch(Exception $e){
        echo '<h2>Se ha producido un error <small>Es un bajón!!!  <i class="fa fa-frown-o"></i></small></h2><br/><strong>Motivo</strong>: '.$e->getMessage();
    }

    include_once('../core/footer.php');
?>
