<?php
include_once('../core/kernel.php');
include_once('../core/config.php');
include_once('../core/header.php');

use Models\Programa;
use Models\Insumo;
use Models\PNUDPresentacion;

?>

        <div class="page-header">
          <div class="row">
            <div class="col-md-12">
              <h1 id="buttons">
                <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i> Programas <small>Reportes sobre los diversos programas</small>
              </h1>
            </div>
          </div>
        </div>

<h2>Selección del programa <small>Eliga sobre que programa se realizará el reporte</small></h2>
<?php

try{
    $programas = Programa::orderby('nombre')->get();

    echo "<div class='list-group'>";
    foreach ($programas as $programa){
        echo "<a href='/programas/reporte.php?id=".$programa->id."' class='list-group-item'>$programa</a>";
    }
    echo '</div>';


?>


<?php
    }
    catch(Exception $e){
        echo '<h2>Se ha producido un error <small>Es un bajón!!!  <i class="fa fa-frown-o"></i></small></h2><br/><strong>Motivo</strong>: '.$e->getMessage();
    }

    include_once('../core/footer.php');
?>
