<?php include_once('../core/kernel.php'); ?>
<?php include_once('../core/config.php'); ?>
<?php include_once('../core/header.php'); ?>

        <div class="page-header">
          <div class="row">
            <div class="col-md-12">
              <h1 id="buttons">
                <i class="fa fa-tasks fa-fw" aria-hidden="true"></i> Stock OCA  <small>Reportes sobre el stock en OCA</small>
              </h1>
            </div>
          </div>
        </div>
<?php
try{
    $excepcionesEnPresentaciones  = $db->select('
            select 	v_i.codigo_oca as codigo_oca,
                    pe.multiplicador as multiplicador,
                    pe.nombre nombre,
                    v_i.nombre as insumo
            
            from	presentaciones_excepciones pe
                    left join
                    v_insumos v_i
                    on (v_i.id= pe.insumo)
    ');
?>
<h2><i class="fa fa-briefcase"></i> Presentaciones <small>Visualizacion y gestión de las presentaciones y su uso</small></h2>
<div class="row">
    <div class="col-md-8">
        <p>Las presentaciones son utilizadas para multiplicar las cantidades que figuran en el stock del operador logistico, RMS, DMS,etc..</p>
        <table width="60%" id="ABMDatatable">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Multiplicador</th>
                    <th><abbr data-toggle="Indica si actualmente esta siendo utilizado en alguno insumo de la nomina" data-placement="top">Usado</abbr></th>
                    <th data-filtrar="false">Acciones</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Multiplicador</th>
                    <th>Usado</th>
                    <th>Acciones</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<script src="/assets/funciones.js"></script>
<script>
    var configuracion = {
        "ajax":    '/abm/ajax.php?entity=PNUDPresentacion&operation=list',
        "columns": [
            { "data": "id", "width":"50"},
            { "data": "nombre"},
            { "data": "multiplicador","width":"100"},
            { "data": "usado","width":"50"},
            { "data": "","width":"50"}
        ],
        "autoWidth": false,
        "order": [[ 1, "asc" ]],
        "rowId": function(row) {

            return 'entity_' + row.id;
        },
        "columnDefs": [
            {
                "targets": 4,
                "render": function ( data, type, row ) {
                    switch(row.usado){
                        case 'No':      return '<a role="button" class="btnABMAcciones accionEditar" href="javascript:mostrarModalPresentacion('+row.id+')" title="Editar la presentacion"><i class="fa fa-pencil"></i></a>';
                                        break;
                        default:        return '';
                    }

                }
            },
            {
                "targets": 1,
                "createdCell": function (td, cellData, rowData, row, col) {
                    switch(rowData.usado){
                        case 'Si':   $(td).css({'color': 'black', 'background-color': 'yellowgreen'});
                            break;
                        case 'No':   $(td).css({'color': 'lightgray', 'background-color': 'black'});
                            break;
                    }
                }
            }
        ]

    };
    DataTableULM('#ABMDatatable','PNUD Presentaciones de la ULM',configuracion);



    function mostrarModalPresentacion(presentacionId){
        formularioABMEdicion(
            'modalEdicion_form',
            presentacionId,
            function(){
                //callback si todo salio OK: muestro el modal
                $('#modalEdicion').modal('show');
            }
        );
    }

    function enviarEdicionPresentacion(){

        formularioABM(
            'modalEdicion_form',
            function(){
                //callback si todo salio OK: refresco el datatable
                datatableULMAjaxRefresh('#ABMDatatable');
                $('#modalEdicion').modal('hide');
            }
        );

    }

    $(function(){
        $('[data-toggle="tooltip"]').tooltip()
    });
</script>
        <!-- Modal para editar presentacion-->
        <div class="modal fade" id="modalEdicion" tabindex="-1" role="dialog" aria-labelledby="modalEdicion">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Presentación > <span id="modalABM_operacionLabel"></span></h4>
                    </div>
                    <div class="modal-body">
                        <form name="modalEdicion_form" action="#" data-entity="PNUDPresentacion" method="POST">
                            <input type="hidden" id="presentacion_id" name="entity[id]" value=""/>
                            <input type="hidden" name="_operation" value=""/>
                            <div class="form-group">
                                <label for="presentacion_nombre">Nombre</label>
                                <input type="text" class="form-control" id="presentacion_nombre" name="entity[nombre]" maxlength="64" minlength="4" required/>
                            </div>
                            <div class="form-group">
                                <label for="presentacion_multiplicador">Multiplicador</label>
                                <input type="number" max="50000" min="1" class="form-control" id="presentacion_multiplicador" name="entity[multiplicador]" placeholder="12345" value="" required/>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn" id="modalABM_operacionButton" onclick="enviarEdicionPresentacion()"></button>
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
<?php
    }
    catch(Exception $e){
        echo '<h2>Se ha producido un error <small>Es un bajón!!!  <i class="fa fa-frown-o"></i></small></h2><br/><strong>Motivo</strong>: '.$e->getMessage();
    }

    include_once('../core/footer.php');
?>
