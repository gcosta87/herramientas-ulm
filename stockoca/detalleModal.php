<?php
/**
 * Pequeño script que retorna las respuestas para los modales
 * Basicamente se nuclea las posibles consultas (indicadas via parametro GET) en este script y se generaliza la respuesta segun el caso.
 * /detalleModal.php?consulta=$__CONSULTA__&paramX=a,...
 */
include_once('../core/kernel.php');
$consulta=$_GET['consulta'];
$resultado = array();   //Estructura donde se almacena el resultado de la query a ejecutar

switch ($consulta){
    case 'detalleHistoricoInsumos':     $codigoOCA= $_GET['codigo_oca'];
                                        $resultado = $db->select("
                                                                (
                                                                    select	date_format(fecha_archivo,'%d/%m/%Y') as fecha, cantidad, 'egreso' as tipo, pedido_numero as referencia, fecha_archivo
                                                                    from	LINEA_DMS
                                                                    where	fecha_archivo >= (CURDATE() - INTERVAL 60 day)
                                                                            and
                                                                            codigo_oca = '$codigoOCA'
                                                                )
                                                                union all
                                                                (
                                                                    select	date_format(fecha_archivo,'%d/%m/%Y') as fecha, cantidad, 'ingreso' as tipo, concat('RMS',referencia) as referencia, fecha_archivo
                                                                    from	LINEA_RMS
                                                                    where	fecha_archivo >= (CURDATE() - INTERVAL 60 day)
                                                                            and
                                                                            codigo_oca = '$codigoOCA'
                                                                )           
                                                                order by fecha_archivo desc
                                                      ");
                                        break;
    case 'detalleLotesInsumo':          $codigoOCA= $_GET['codigo_oca'];
                                        $fecha= $_GET['fecha'];
                                        $resultado = $db->select("
                                                                select	discriminado.lote as lote,
                                                                        discriminado.total as stock, 
                                                                        (discriminado.total * v_i.presentacion_multiplicador) as stock_unidades, 
                                                                        date_format(discriminado.vencimiento,'%d/%m/%Y') as fecha_vencimiento
                                                                from
                                                                        (
                                                                            select codigo_oca, lote, vencimiento, sum(cantidad) as total
                                                                            from stock_oca_discriminado
                                                                            where codigo_oca = '$codigoOCA'
                                                                                  and 
                                                                                  fecha = '$fecha'
                                                                           group by codigo_oca, lote, vencimiento
                                                                
                                                                        ) discriminado
                                                                        left join v_insumos v_i
                                                                        on (v_i.codigo_oca = discriminado.codigo_oca)
                                                                order by discriminado.vencimiento, discriminado.lote      
                                                      ");
                                        break;

    case 'insumosGenericosDePrograma':  $id= $_GET['programaId'];
                                        $resultado = $db->select("
                                                                select	pi.nombre_oca as insumo
                                                                from	INSUMOSGENERICOS_PROGRAMA igp
                                                                        left join   pnud_insumos pi ON (igp.pnud_insumo = pi.id)
                                                                where igp.programa = $id
                                                                order by pi.nombre_oca
                                                      ");
                                        break;
    case 'tablaDetalleDestinos':  $id= $_GET['programaId'];
                                        $resultado = $db->select("
                                                                select	i.pnud_insumo as pnud_insumo,
                                                                        pi.nombre_oca as insumo,
                                                                        des.nombre_corto as destino,
                                                                        (sum(dms.cantidad * pres.multiplicador)) as unidades_totales
                                                                
                                                                from	PROGRAMAS p 
                                                                        left join INSUMOS_PROGRAMA ip
                                                                        on (p.id = $id and p.id = ip.programa)
                                                                        left join insumos i
                                                                        on (ip.insumo = i.id)
                                                                        left join LINEA_DMS dms
                                                                        on (dms.codigo_oca = i.codigo_oca)
                                                                        left join pnud_insumos pi
                                                                        on (pi.codigo_oca_corto = i.codigo_oca_corto)
                                                                        left join pnud_presentaciones pres
                                                                        on (pres.id = i.presentacion)
                                                                        left join LINEA_CMS cms
                                                                        on (dms.pedido_numero = cms.pedido_numero)
                                                                        left join pnud_destinos des
                                                                        on (cms.destino = des.id)
                                                                
                                                                where dms.fecha_archivo >= (curdate() - interval 30 day)
                                                                group by i.pnud_insumo, insumo, des.id
                                                                order by destino, insumo
                                                       ");
                                        break;
    case 'tablaDetalleDistribucionDestino': $id= $_GET['destinoId'];
                                            $resultado = $db->select("
                                                            select	v_i.nombre as insumo,
                                                                    sum(dms.cantidad * v_i.presentacion_multiplicador) as unidades
                                                            
                                                            from	pnud_destinos destino
                                                                    left join LINEA_CMS cms
                                                                    on (destino.id = $id and cms.destino = destino.id )
                                                                    left join LINEA_DMS dms
                                                                    on (dms.pedido_numero = cms.pedido_numero)
                                                                    left join v_insumos v_i
                                                                    on (dms.codigo_oca = v_i.codigo_oca)
                                                            
                                                            where
                                                                cms.fecha_archivo between (current_date() - INTERVAL 30 day) and (current_date())
                                                            group by v_i.codigo_oca_corto,v_i.nombre   
                                                            order by insumo
                                                          ");
                                            break;
    case 'reporteVencimientos':
                                            $resultado = $db->select('
                                                        SELECT	*
                                                        FROM	ulm_extras.v_stock_oca_vencimientos
                                            ');
                                            break;
    case 'reporteVencimientosSimplificado':
                                            $resultado = $db->select('

                                                        SELECT  codigo_oca, insumo, presentacion, vencimiento, sum(stock) as stock, sum(stock_unidades) as stock_unidades, venceEnMeses, alerta, programa
                                                        FROM    v_stock_oca_vencimientos
                                                        GROUP BY codigo_oca, insumo, presentacion, vencimiento, venceEnMeses, alerta, programa


                                            ');
                                            break;

    case 'stockOCASimplificado':
                                            $resultado = $db->select('
                                                    select 	stock_simplificado.codigo_oca_corto,
                                                            stock_simplificado.insumo,
                                                            stock_simplificado.stock_unidades_total,
                                                            stock_simplificado.stock_mensual_unidades,
                                                            if(	stock_mensual_unidades >= 1,
                                                                 round((stock_simplificado.stock_unidades_total/stock_simplificado.stock_mensual_unidades)) ,
                                                                \'(no calculado)\'
                                                            ) as stock_abastecimiento_meses,
                                                            stock_simplificado.programa
                                                    from (
                                                        select	codigo_oca_corto as codigo_oca_corto,
                                                                nombre_oca as insumo,
                                                                sum(stock_unidades) stock_unidades_total,
                                                                stock_mensual_unidades,
                                                                programa
                                                        from	v_stock_oca_actual
                                                        group by	codigo_oca_corto, nombre_oca, programa, stock_mensual_unidades
                                                        order by	programa, nombre_oca, programa
                                                    ) stock_simplificado
                                            ');
                                            break;
}

//Respuesta como la espera el Datatable
$respuesta = ['data' => $resultado];
echo json_encode($respuesta);
