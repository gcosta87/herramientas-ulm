<?php
include_once('../core/kernel.php');
include_once('../core/config.php');
include_once('../core/header.php');

?>
        <div class="page-header">
          <div class="row">
            <div class="col-md-12">
              <h1 id="buttons">
                <i class="fa fa-tasks fa-fw" aria-hidden="true"></i> Stock OCA  <small>Reportes sobre el stock en OCA</small>
              </h1>
            </div>
          </div>
        </div>
<?php

try{
    // Se calcula la fecha mayor en base a las actualizaciones de CMS/RMS indicadas en la view correspondiente
    $fechaStock  = $db->select('
            select	
                    if(e.dms_actualizacion_fecha >=  e.rms_actualizacion_fecha,e.dms_actualizacion_fecha, e.rms_actualizacion_fecha) as fecha
            from	v_estado_datos e

    ')[0]->fecha;

    $valoresStock = $db->select('
        select  *
        from    v_stock_ulm_actual 
    ');
?>

<h2><i class="fa fa-list-ul"></i> Stock ULM (<span title="Fecha del RMS o DMS más reciente"><?php echo $fechaStock;?></span>) &nbsp;&nbsp;&nbsp;<small>Stock calculado en base a los RMS/DMS</small></h2>
<table width="90%" id="tabla">
    <thead>
        <tr>
            <th>Codigo OCA</th>
            <th>Insumo</th>
            <th>Presentacion</th>
            <th>Cajas</th>
            <th>Unids.</th>
            <th>Programa</th>
            <th>Movimientos</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>Codigo OCA</th>
            <th>Insumo</th>
            <th>Presentacion</th>
            <th>Cajas</th>
            <th>Unids.</th>
            <th>Programa</th>
            <th>Movimientos</th>
        </tr>
    </tfoot>
<?php
    foreach ($valoresStock as $valorStock){
        echo '<tr data-codigoOCA="'.$valorStock->codigo_oca.'">';
        echo '<td><a href="/reportes/insumo.php?codigoULM='.$valorStock->insumo_id.'" title="Analizar la informacion almacenada en la Base de datos" target="_blank">'.$valorStock->codigo_oca .'</a></td>';
        echo '<td>'.$valorStock->nombre_oca .'</td>';
        echo '<td>'.$valorStock->presentacion .'</td>';
        echo '<td>'.$valorStock->stock .'</td>';
        echo '<td>'.$valorStock->stock_unidades .'</td>';
        echo '<td>'.$valorStock->programa .'</td>';
        echo '<td><a nohref onclick="javascript:mostrarDetalle(\''.$valorStock->codigo_oca .'\');" role="button" ><i class="fa fa-eye"></a></td>';
        echo '</tr>';
    }
?>
</table>
<script>
    //Inicializacion de la tabla principal del stock
    $('#tabla').DataTable({
        language: {
            url: '/assets/DataTables/Spanish.json'
        },
        "pageLength": 25,
        dom: 'Blfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                text: 'Copiar tabla',
                exportOptions: {
                    modifier: {
                        page: 'current'
                    }
                }
            }
        ]
    });

    function mostrarDetalle(codigoOCA){
        $('#modalDetalle_titulo').text('Detalles del insumo '+codigoOCA);

        //Se busca los campos del insumo de la tabla
        $detallesInsumo=$('tr[data-codigoOCA="'+codigoOCA+'"] td');

        //Se setea los campos del modal. 1=Nombre, 2=presentacion
        $('td#mostrarDetalle_nombre').text($($detallesInsumo.get(1)).text());
        $('td#mostrarDetalle_presentacion').text($($detallesInsumo.get(2)).text());

        //Datatable
        //Si el datatable esta inicializado, se lo invoca...sino se lo configura
        if ( $.fn.dataTable.isDataTable( '#tablaModal' ) ) {
            $('#tablaModal').DataTable().destroy();
        }

        $('#tablaModal').DataTable({
            language: {
                url: '/assets/DataTables/Spanish.json'
            },
            "pageLength": 7,
            "ajax":    '/stockoca/detalleModal.php?consulta=detalleHistoricoInsumos&codigo_oca='+codigoOCA,
            "columns": [
                { "data": "fecha"},
                { "data": "cantidad"},
                { "data": "tipo"},
                { "data": "referencia"}
            ]
        });

        $('#modalDetalle').modal('show');
    }
</script>
        <!-- Modal para des-->
        <div class="modal fade" id="modalDetalle" tabindex="-1" role="dialog" aria-labelledby="modalDetalle">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modalDetalle_titulo"></h4>
                    </div>
                    <div class="modal-body">
                        <h4>Detalles del insumo</h4>
                        <table style="margin-left: 25px;">
                            <tr>
                                <td width="100"><strong>Nombre</strong></td>
                                <td id="mostrarDetalle_nombre"></td>
                            </tr>
                            <tr>
                                <td><strong>Presentacion</strong></td>
                                <td id="mostrarDetalle_presentacion"></td>
                            </tr>
                        </table>
                        <br/><br/>
                        <h4>Histórico de Egresos/Ingresos (últimos 60 días)</h4>
                        <table id="tablaModal">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Cantidad</th>
                                    <th>Tipo</th>
                                    <th><span title="Indica el numero de pedido (egreso) o archivo RMS (ingreso)">Referencia</span></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Cantidad</th>
                                    <th>Tipo</th>
                                    <th>Referencia</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
<?php
    }
    catch(Exception $e){
        echo '<h2>Se ha producido un error <small>Es un bajón!!!  <i class="fa fa-frown-o"></i></small></h2><br/><strong>Motivo</strong>: '.$e->getMessage();
    }

    include_once('../core/footer.php');
?>
