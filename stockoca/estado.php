<?php include_once('../core/kernel.php'); ?>
<?php include_once('../core/config.php'); ?>
<?php include_once('../core/header.php'); ?>

        <div class="page-header">
          <div class="row">
            <div class="col-md-12">
              <h1 id="buttons">
                <i class="fa fa-tasks fa-fw" aria-hidden="true"></i> Stock OCA <small>Reportes sobre el stock en OCA</small>
              </h1>
            </div>
          </div>
        </div>


<?php
    try{
        $estadoDatos= $db->select('select * from v_estado_datos')[0];
?>
<h2><i class="fa fa-database"></i> Estado de la base de datos <small>Informacion relativa a los ultimos datos importados</small></h2>
<table class="table">
    <thead>
        <tr>
            <th>Stocks <i class="fa fa-question-circle" title="Cantidad de stocks del Operador Logistico importados"></i></th>
            <th>Fecha Último Stock <i class="fa fa-question-circle" title="El ultimo stock enviado por el Operador Logistico"></i></th>

            <th>CMS<i class="fa fa-question-circle" title="Cantidad de archivos CMS importados"></i></th>
            <th>Fecha Ultimo<i class="fa fa-question-circle" title="La fecha del ultimo archivo CMS"></i></th>
            <th>Último<i class="fa fa-question-circle" title="Ultimo CMS importado a la Base de Datos"></i></th>

            <th>DMS<i class="fa fa-question-circle" title="Cantidad de archivos DMS importados"></i></th>
            <th>Fecha Ultimo<i class="fa fa-question-circle" title="La fecha del ultimo archivo DMS"></i></th>
            <th>Último<i class="fa fa-question-circle" title="Ultimo DMS importado a la Base de Datos"></i></th>

            <th>RMS<i class="fa fa-question-circle" title="Cantidad de archivos RMS importados"></i></th>
            <th>Fecha Ultimo<i class="fa fa-question-circle" title="La fecha del ultimo archivo RMS"></i></th>
            <th>Último<i class="fa fa-question-circle" title="Ultimo RMS importado a la Base de Datos"></i></th>
        </tr>
    </thead>
    <tr>
        <td style="border-bottom: 2px dimgrey solid;border-left: 2px dimgrey solid;"><?php echo $estadoDatos->cantidad_stock; ?></td>
        <td style="border-bottom: 2px dimgrey solid;"><?php echo $estadoDatos->fecha_actualizacion_stock; ?></td>

        <td style="border-bottom: 2px #a4bca4 solid;border-left: 2px #a4bca4 solid;"><?php echo $estadoDatos->cms_cantidad_importada; ?></td>
        <td style="border-bottom: 2px #a4bca4 solid;"><?php echo $estadoDatos->cms_actualizacion_fecha; ?></td>
        <td style="border-bottom: 2px #a4bca4 solid;"><?php echo $estadoDatos->cms_actualizacion_archivo; ?></td>

        <td style="border-bottom: 2px orange solid;border-left: 2px orange solid;"><?php echo $estadoDatos->dms_cantidad_importada; ?></td>
        <td style="border-bottom: 2px orange solid;"><?php echo $estadoDatos->dms_actualizacion_fecha; ?></td>
        <td style="border-bottom: 2px orange solid;"><?php echo $estadoDatos->dms_actualizacion_archivo; ?></td>

        <td style="border-bottom: 2px dodgerblue solid;border-left: 2px dodgerblue solid;"><?php echo $estadoDatos->rms_cantidad_importada; ?></td>
        <td style="border-bottom: 2px dodgerblue solid;"><?php echo $estadoDatos->rms_actualizacion_fecha; ?></td>
        <td style="border-bottom: 2px dodgerblue solid;"><?php echo $estadoDatos->rms_actualizacion_archivo; ?></td>
    </tr>
</table>
<?php
    $informacionProgramas = $db->select('
        select	p.id as programaId, p.nombre as programa, count(*) as cantidadInsumos
        from	PROGRAMAS p 
                left join INSUMOSGENERICOS_PROGRAMA igp
                on (p.id = igp.programa)
        group by p.id
        order by p.nombre
    ');
?>
<h3> Información cargada y asociaciones <small>Las principales entidades y sus datos asociados</small></h3>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">Últimos (5) stocks importados</div>
            <div class="panel-body">
                <p>Se detallan los stock del operador logístico que fueron importados, detallando la cantidad de insumos reportados en cada uno.</p>
            </div>
            <table class="table">
                <thead>
                <tr>
                    <th>Fecha del stock</th>
                    <th>Cantidad de insumos</th>
                </tr>
                </thead>
                <?php
                $reporteStock = $db->select("
                        select date_format(fecha, '%d/%m/%Y') as fecha, count(*) as cantidad_insumos
                        from stock_oca stock
                        group by fecha
                        order by stock.fecha desc
                        limit 5
             
                  ");

                foreach ($reporteStock as $stock){
                    echo '<tr>';
                    echo '    <td>'.$stock->fecha.'</td>';
                    echo '    <td>'.$stock->cantidad_insumos.'</td>';
                    echo '</tr>';
                }
                ?>
                <tr><td></td><td></td></tr>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">Otras entidades</div>
            <div class="panel-body">
                <p>Se muestra informacion relativa a otras entidades cargadas en la Base de datos las cuales son utilizadas a lo largo de los reportes</p>
            </div>
            <table class="table">
                <thead>
                <tr>
                    <th>Entidad</th>
                    <th>Cantidad</th>
                </tr>
                </thead>
            <?php
            $consultaEntidades = $db->select('
                                        select	\'Insumos (PNUD)\' as entidad,
                                                (select count(distinct(codigo_oca_corto)) from pnud_insumos) as total,
                                                \'Codigos de OCA con sus descripciones utilizadas para reconocer los insumos\' as detalle
                                                
                                        union all
                                        select 	\'Insumos genericos asociados a programas\' as entidad,
                                                (select count(distinct(pnud_insumo)) from INSUMOSGENERICOS_PROGRAMA) as total,
                                                \'Insumos genericos (sin presentacion) asociados a los distintos programas\' as detalle
                                                
                                        union all
                                        select 	\'Insumos asociados a programas\' as entidad,
                                                (select count(distinct(insumo)) from INSUMOS_PROGRAMA) as total,
                                                \'Insumos (con presentacion y laboratorio) asociados a los distintos programas\' as detalle
                                                
                                        union all
                                        
                                        select	\'Presentaciones (PNUD)\' as entidad,
                                                (select count(*) from pnud_presentaciones) as total,
                                                \'Las distintas presentaciones importadas del PNUD\' as detalle                                                
                                        union all
                                        
                                        select	\'Proveedores (PNUD)\' as entidad,
                                                (select count(*) from pnud_proveedores) as total,
                                                \'Los distintos proveedores importados del PNUD\' as detalle
                                        union all
                                        
                                        select	\'Destinos (PNUD)\' as entidad,
                                                (select count(*) from pnud_destinos) as total,
                                                \'Los distintos destinos importados del PNUD\' as detalle             
            ');

            foreach ($consultaEntidades as $consultaEntidad){
                echo '<tr>';
                echo '    <td>'.$consultaEntidad->entidad.'</td>';
                echo '    <td>'.$consultaEntidad->total.' <i class="fa fa-question-circle" title="'.$consultaEntidad->detalle.'"></i></td>';
                echo '</tr>';
            }
            ?>
            <tr><td></td><td></td></tr>
            </table>
        </div>
    </div>
</div>

<div class="row">


    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">Programas</div>
            <div class="panel-body">
                <p>Se presentan los insumos (genericos) asociados a los diversos programas. De esta asociación se presentan los reportes.</p>
            </div>
            <table class="table">
                <thead>
                <tr>
                    <th>Programa</th>
                    <th>Insumos asociados <i class="fa fa-question-circle" title="Cantidad de insumos genéricos asociados al programa"></i></th>
                </tr>
                </thead>
                <?php
                foreach ($informacionProgramas as $informacionPrograma ){
                    echo '<tr data-programaId="'.$informacionPrograma->programaId.'">';
                    echo '    <td>'.$informacionPrograma->programa.'</td>';
                    echo '    <td><button class="btn btn-info btn-sm" type="button" title="Visualizar los insumos asociados al programa" onclick="javascript:mostrarDetalle('.$informacionPrograma->programaId.',\''.$informacionPrograma->programa.'\');"><span class="badge badge-info">'.$informacionPrograma->cantidadInsumos.'</span></button></td>';
                    echo '</tr>';
                }
                ?>
            </table>
        </div>
    </div>


</div>
<script>
    function mostrarDetalle(programaId,programaNombre){
        $('#modalDetalle_titulo').text('Insumos del programa  '+programaNombre);

        //Datatable
        //Si el datatable esta inicializado, se lo invoca...sino se lo configura
        if ( $.fn.dataTable.isDataTable( '#tablaModal' ) ) {
            $('#tablaModal').DataTable().destroy();
        }

        $('#tablaModal').DataTable({
            language: {
                url: '/assets/DataTables/Spanish.json'
            },
            "dom": 'lfrtip',
            "pageLength": 10,
            "ajax":    '/stockoca/detalleModal.php?consulta=insumosGenericosDePrograma&programaId='+programaId,
            "columns": [
                { "data": "insumo"},
            ]
        });

        $('#modalDetalle').modal('show');
    }

</script>
        <!-- Modal para detalles de programas-->
        <div class="modal fade" id="modalDetalle" tabindex="-1" role="dialog" aria-labelledby="modalDetalle">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modalDetalle_titulo"></h4>
                    </div>
                    <div class="modal-body">
                        <table id="tablaModal">
                            <thead>
                            <tr>
                                <th>Insumo</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

<?php
    }
    catch(Exception $e){
        echo '<h2>Se ha producido un error <small>Es un bajón!!!  <i class="fa fa-frown-o"></i></small></h2><br/><strong>Motivo</strong>: '.$e->getMessage();
    }

    include_once('../core/footer.php');
?>
