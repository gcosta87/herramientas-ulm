<?php
include_once('../core/kernel.php');
include_once('../core/config.php');
include_once('../core/header.php');

?>

        <div class="page-header">
          <div class="row">
            <div class="col-md-12">
              <h1 id="buttons">
                <i class="fa fa-tasks fa-fw" aria-hidden="true"></i> Stock OCA  <small>Reportes sobre el stock en OCA</small>
              </h1>
            </div>
          </div>
        </div>
        <div class="menuSubHeader">
<?php

try{
    $parametroFecha = null;
    $parametroFechaSeteado = isset($_GET['fecha']); //flag auxilar

    if ($parametroFechaSeteado){
        $parametroFecha = $_GET['fecha'];
    }
    else{
        // Se calcula la fecha mas recientes del stock.
        $parametroFecha  = $db->select('
          select    max(fecha) as fecha
          from      stock_oca
        ')[0]->fecha;
    }

    //Fechas para armar el selector 
    $fechasDeStock  = $db->select('
                select    distinct date_format(fecha,\'%d/%m/%Y\') as fecha_legible, fecha
                from      stock_oca
                order by  fecha desc
                limit 18
        ');

    $valoresStock = $db->select("
        select  *
        from    v_stock_oca
        where   fecha = '$parametroFecha' 
    ");

    $numeroDeMes = intval(explode('-',$parametroFecha)[1]);
    //Si es el stock actual y la fecha indica que es de este mes: se habilita el btn de eliminacion
    if((!$parametroFechaSeteado) && (intval(date('m')) == $numeroDeMes)){
?>

        <a role="button" class="btn btn-danger btn-sm pull-right" title="Elimina el stock actual" data-toggle="tooltip" data-placement="top" onclick="javascript:eliminarStockActual('<?php echo $parametroFecha;?>')"><i class="fa fa-trash fa-fw"></i></a>
<?php
    }
?>
        <a role="button" class="btn btn-info btn-sm pull-right" title="Importar el excel enviado por el operador logistico" data-toggle="tooltip" data-placement="top" onclick="modalImportador()">Importar Excel</a>
    </div>
<h2><i class="fa fa-tasks"></i> Stock OCA
    <form method="GET" action="/stockoca" style="display: inline;" name="formularioFecha">
        <select name="fecha" id="selectorFecha" style="font-size:inherit; border:none;" title="Consulte el stock histórico de OCA" data-toggle="tooltip" data-placement="top">
<?php
        foreach ($fechasDeStock as $fechaDeStock){
            if($parametroFecha != $fechaDeStock->fecha){
                echo '<option value="'.$fechaDeStock->fecha.'">'.$fechaDeStock->fecha_legible.'</option>\n';
            }
            else{
                echo '<option value="'.$fechaDeStock->fecha.'" selected>'.$fechaDeStock->fecha_legible.'</option>\n';
            }
        }
?>
        </select>
    </form>
    <small>Reportado por el operador logístico OCA</small></h2>

<?php
    if($parametroFechaSeteado){
        echo '
              <div class="alert alert-warning fade in" role="alert">
                <strong>Atención!</strong>, estás accediendo a un histórico del Stock OCA.</p>
              </div>
        ';
    }
?>

    <table width="100%" id="tabla">
        <thead>
            <tr>
                <th>Codigo OCA</th>
                <th>Insumo</th>
                <th>Presentacion</th>
                <th data-filtrar="false">Cajas</th>
                <th data-filtrar="false">Unids.</th>
                <th>Programa</th>
                <th>Proveedor</th>
                <th data-filtrar="false">Lotes</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Codigo OCA</th>
                <th>Insumo</th>
                <th>Presentacion</th>
                <th>Cajas</th>
                <th>Unids.</th>
                <th>Programa</th>
                <th>Proveedor</th>
                <th>Lotes</th>
            </tr>
        </tfoot>
<?php
    foreach ($valoresStock as $valorStock){
        echo '<tr data-codigoOCA="'.$valorStock->codigo_oca.'">';
        echo '<td><a href="/reportes/insumo.php?codigoULM='.$valorStock->insumo_id.'" title="Analizar la informacion almacenada en la Base de datos" target="_blank">'.$valorStock->codigo_oca .'</a></td>';
        echo '<td>'.$valorStock->nombre_oca .'</td>';
        echo '<td>'.$valorStock->presentacion .'</td>';
        echo '<td>'.$valorStock->stock .'</td>';
        echo '<td>'.$valorStock->stock_unidades .'</td>';
        echo '<td>'.$valorStock->programa .'</td>';
        echo '<td>'.$valorStock->proveedor .'</td>';
        echo '<td><a nohref onclick="javascript:mostrarDetalle(\''.$valorStock->codigo_oca .'\');" role="button" ><i class="fa fa-barcode"></a></td>';
        echo '</tr>';
    }
?>
    </table>

<script src="/assets/funciones.js"></script>
<script>

    //Inicializacion de la tabla principal del stock
    DataTableULM('#tabla','Reporte de Stock en OCA');

    /**
     * Muestra los detalles de los lotes
     * @param codigoOCA
     */
    function mostrarDetalle(codigoOCA){
        $('#modalDetalle_titulo').text('Lotes del insumo '+codigoOCA);

        //Se busca los campos del insumo de la tabla
        $detallesInsumo=$('tr[data-codigoOCA="'+codigoOCA+'"] td');

        //Se setea los campos del modal. 1=Nombre, 2=presentacion
        $('td#mostrarDetalle_nombre').text($($detallesInsumo.get(1)).text());
        $('td#mostrarDetalle_presentacion').text($($detallesInsumo.get(2)).text());

        //Datatable
        //Si el datatable esta inicializado, se lo invoca...sino se lo configura
        if ( $.fn.dataTable.isDataTable( '#tablaModal' ) ) {
            $('#tablaModal').DataTable().destroy();
        }

        //obtengo la fecha del stock
        fechaStock = $('#selectorFecha').val();


        $('#tablaModal').DataTable({
            language: {
                url: '/assets/DataTables/Spanish.json'
            },
            "pageLength": 7,
            "ajax":    '/stockoca/detalleModal.php?consulta=detalleLotesInsumo&codigo_oca='+codigoOCA+'&fecha='+fechaStock,
            "columns": [
                { "data": "lote"},
                { "data": "stock"},
                { "data": "stock_unidades"},
                { "data": "fecha_vencimiento"}
            ]
        });

        $('span#fechaStockDiscriminado').text(fechaStock);
        $('#modalDetalle').modal('show');
    }

    $(function(){
        $('#selectorFecha').change(function(){
            var valorSeleccionado = $('#selectorFecha').val();
            var valorStockMasReciente = $('#selectorFecha option').first().val();

            //Si es el stock mas reciente, refresco la pagina sin parametros
            if(valorStockMasReciente == valorSeleccionado){
                window.location= '/stockoca/';
            }
            else{
                $('form[name=formularioFecha]').submit();
            }
        });

        //realiza el envio del formulario
        $('#btnImportar').click(function(e){
            e.preventDefault();
            var $formularioImportar = $('form[name="formluarioImportacion"]')[0];
            var formData = new FormData($formularioImportar); //$formularioImportar.serialize();

            $.ajax({
                url: '/stockoca/importacionAction.php',
                type: 'POST',
                data: formData,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if(data.error == false){
                        //refresco el form
                        $formularioImportar.reset();

                        //aviso al usr
                        alertify.success('Se han importado los datos. Se recargará en unos segundos la página para mostrarlos.');
                        setTimeout(function(){

                            window.location='/stockoca/';
                        },6000)
                    }
                    else{
                        alertify.error(data.mensaje,20);
                    }
                },
                error: function () {
                    alertify.error('Se produjo un error al enviar los datos de la importacion',10);
                }
            });
        });

        $('[data-toggle="tooltip"]').tooltip();
    });

    /**
     * Despliega el modal de importación de excel
     */
    function modalImportador(){
        $('#modalImportador').modal('show');
    }

    /**
     * Invoca via Ajax la eliminacion del stock indicado en la fecha (YYYY-MM-DD)
     * @param fechaString
     */
    function eliminarStockActual(fechaString){
        alertify.confirm(
            'Eliminacion de Stock actual',
            '¿Estás seguro que queres borrar el stock actual?',
            function(){
                $.ajax({
                    url: '/archivosOCA/consultasAjax.php?consulta=eliminarUltimoStockOCA&fecha='+fechaString,
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        if(data.error == false){
                            //aviso al usr
                            alertify.success('Se ha eliminado correctamente el stock!. Se refrescará la pagina para mostrar los cambios');
                            setTimeout(function(){
                                window.location='/stockoca/';
                            },6000);
                        }
                        else{
                            alertify.error(data.mensaje);
                        }
                    },
                    error: function () {
                        alertify.error('Se produjo un error al enviar la operacion de eliminacion al servidor!.');
                    }
                });

            },
            function(){}
        );
    }

</script>



        <!-- Modal para mostrar detalles-->
        <div class="modal fade" id="modalDetalle" tabindex="-1" role="dialog" aria-labelledby="modalDetalle">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modalDetalle_titulo"></h4>
                    </div>
                    <div class="modal-body">
                        <h4>Detalles del insumo</h4>
                        <table style="margin-left: 30px;">
                            <tr>
                                <td width="100"><strong>Nombre</strong></td>
                                <td id="mostrarDetalle_nombre"></td>
                            </tr>
                            <tr>
                                <td><strong>Presentacion</strong></td>
                                <td id="mostrarDetalle_presentacion"></td>
                            </tr>
                        </table>
                        <br/><br/>
                        <h4>Lotes asociados (<span title="Fecha del reporte discriminado cargado en la base de datos" id="fechaStockDiscriminado"></span>)</h4>
                        <table id="tablaModal">
                            <thead>
                                <tr>
                                    <th>Lote</th>
                                    <th>Cajas</th>
                                    <th>Unidades</th>
                                    <th>Vencimiento</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Lote</th>
                                    <th>Cajas</th>
                                    <th>Unidades</th>
                                    <th>Vencimiento</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

    <!-- Modal para importar Excel -->
    <div class="modal fade" id="modalImportador" tabindex="-1" role="dialog" aria-labelledby="modalImportador">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Importar Excel de Stock OCA</h4>
                </div>
                <div class="modal-body">
                    <h4>Importación <small>Importe el stock enviado por el operador logístico.</small></h4>
                    <form name="formluarioImportacion">
                        <div class="form-group">
                            <label for="importacion_fecha">Fecha del Stock</label>
                            <input type="date" class="form-control" id="importacion_fecha" name="importacion_fecha" value="<?php echo date('Y-m-d');?>" min="<?php echo (new DateTime('first day of this month'))->format('Y-m-d');?>" max="<?php echo (new DateTime('today'))->format('Y-m-d');?>" required/>
                            <p class="help-block">Fecha precisa en la cual se generó el reporte de stock.</p>
                        </div>
                        <div class="form-group">
                            <label for="importacion_archivo">Archivo Excel</label>
                            <input type="file" id="importacion_archivo" name="importacion_archivo" required/>
                        </div>
                        <button role="button" id="btnImportar" type="submit" class="btn btn-default" >Importar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

<?php
    }
    catch(Exception $e){
        echo '<h2>Se ha producido un error <small>Es un bajón!!!  <i class="fa fa-frown-o"></i></small></h2><br/><strong>Motivo</strong>: '.$e->getMessage();
    }

    include_once('../core/footer.php');
?>
