<?php include_once('../core/kernel.php'); ?>
<?php include_once('../core/config.php'); ?>
<?php include_once('../core/header.php'); ?>

        <div class="page-header">
          <div class="row">
            <div class="col-md-12">
              <h1 id="buttons">
                <i class="fa fa-tasks fa-fw" aria-hidden="true"></i> Stock OCA  <small>Reportes sobre el stock en OCA</small>
              </h1>
            </div>
          </div>
        </div>
        <div class="menuSubHeader">
            <a class="btn btn-sm btn-success pull-right" data-toggle="tooltip" data-placement="top" title="Creación de un insumo distribuible" onclick="mostrarModalCreacion()"><i class="fa fa-plus"></i> Agregar insumo</a>
            <a class="btn btn-sm btn-danger pull-right" data-toggle="tooltip" data-placement="top" title="Importacion de insumos en base al Stock OCA" onclick="importacionDeInsumos()"><i class="fa fa-table"></i> Importar</a>

        </div>
<h2><i class="fa fa-briefcase"></i> Stock Distribución <small>Visualizacion y gestión de los insumos que son posibles distribuir</small></h2>
<div class="row">
    <div class="col-md-12">
        <table width="60%" id="ABMDatatable">
            <thead>
                <tr>
                    <th>Codigo OCA</th>
                    <th>Insumo</th>
                    <th>Presentacion</th>
                    <th>Programa</th>
                    <th>Total</th>
                    <th data-filtrar="false">Acciones</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Codigo OCA</th>
                    <th>Insumo</th>
                    <th>Presentacion</th>
                    <th>Programa</th>
                    <th>Total</th>
                    <th>Acciones</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<script src="/assets/MagicSuggest/magicsuggest-min.js"></script>
<script src="/assets/funciones.js"></script>
<script>
    var configuracion = {
        "ajax":    '/abm/ajax.php?entity=StockOCADistribucion&operation=list',
        "columns": [
            { "data": "codigo_oca","width":"50"},
            { "data": "insumo","width":"150"},
            { "data": "presentacion","width":"150"},
            { "data": "programa","width":"100"},
            { "data": "total","width":"40"},
            { "data": "","width":"50"}
        ],
        "autoWidth": false,
        "order": [[ 1, "asc" ]],
        "rowId": function(row) {
            return 'entity_' + row.codigo_oca;
        },
        "columnDefs": [
            {
                "targets": 5,
                "render": function ( data, type, row ) {
                    var accionesHTML =  '<a role="button"  class="btnABMAcciones accionEditar" href="javascript:mostrarModalEdicion(\''+row.codigo_oca+'\')" title="Editar el insumo a distribuir"><i class="fa fa-pencil"></i></a>';
                        accionesHTML += '<a role="button" class="btnABMAcciones accionEliminar" href="javascript:formularioEliminacion(\'StockOCADistribucion\',\''+row.codigo_oca+'\',\'insumo\');" title="Eliminar insumo a distribuir"><i class="fa fa-trash"></i></a>';

                    return accionesHTML;
                }
            }
        ]
    };
    DataTableULM('#ABMDatatable','Stock OCA Distribucion',configuracion);


    //Simil singleton donde se tiene una unica instancia del selector enriquecido de insumos
    var $instanciaSelector = null;

    /**
     * Modal que se muestra ante la creacion de la entidad
     * @param presentacionId
     * @param usado
     */
    function mostrarModalCreacion(){
        formularioABMCreacion(
            'modalABM_form',
            function(){
                $instanciaSelector.clear();
                $('#insumo_codigo_oca').val('');
                $('#modalABM').modal('show');
            }
        );
    }



    /**
     * Modal que se muestra ante la edicion de la entidad
     * @param presentacionId
     * @param usado
     */
    function mostrarModalEdicion(entityId){
        formularioABMEdicion(
            'modalABM_form',
            entityId,
            function(){

                //seteo el selector
                $instanciaSelector.setValue([entityId]);
                //callback si todo salio OK: muestro el modal
                $('#modalABM').modal('show');
            }
        );
    }

    function enviarEdicion(){
        formularioABM(
            'modalABM_form',
            function(){
                //callback si todo salio OK: refresco el datatable
                datatableULMAjaxRefresh('#ABMDatatable');
                $('#modalABM').modal('hide');
            }
        );

    }


    function invocarImportacion(){
        callOperationEntity('StockOCADistribucion','importar','parametro',function(data){alertify.success(data)});

        //siempre se refresca la tabla por las dudas de resultados intermedios
        setTimeout(function (){
            datatableULMAjaxRefresh();
        },800)

    }

    function importacionDeInsumos(){
        alertify.confirm(   'Importar en base al Stock OCA',
                            '¿Desea definir los insumos a distribuir en base a los vigentes en Stock OCA?<br/><small>Esto implica que se borren los insumos actuales y se definan los mismos que están publicados en el stock.</small>',
                            function(){ invocarImportacion();},
                            null
        );
    }

    $(function(){
        setTimeout(function(){
            $('[data-toggle="tooltip"]').tooltip();
        },1500);
        //inicializacion del selector de insumos
        $instanciaSelector = selectorInsumosULM('#selector_insumo','#insumo_codigo_oca');
    });
</script>
        <!-- Modal para editar entidad-->
        <div class="modal fade" id="modalABM" tabindex="-1" role="dialog" aria-labelledby="modalABM">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Insumo de Stock Distribucion > <span id="modalABM_operacionLabel"></span></h4>
                    </div>
                    <div class="modal-body">
                        <form name="modalABM_form" action="#" data-entity="StockOCADistribucion" method="POST">
                            <input type="hidden" name="_operation" value=""/>
                            <div class="form-group">
                                <label for="insumo_codigo_oca">Insumo</label>
                                <input type="hidden" class="form-control" id="insumo_codigo_oca" name="entity[codigo_oca]" maxlength="15" required/>
                                <input type="text" class="form-control" id="selector_insumo"/>

                            </div>
                            <div class="form-group">
                                <label for="insumo_total">Total</label>
                                <input type="number" class="form-control" id="insumo_total" name="entity[total]" min="1" max="999999" required/>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn" id="modalABM_operacionButton" onclick="enviarEdicion()"></button>
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
<?php
    include_once('../core/footer.php');
?>
