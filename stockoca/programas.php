<?php include_once('../core/kernel.php'); ?>
<?php include_once('../core/config.php'); ?>
<?php include_once('../core/header.php'); ?>

        <div class="page-header">
          <div class="row">
            <div class="col-md-12">
              <h1 id="buttons">
                <i class="fa fa-tasks fa-fw" aria-hidden="true"></i> Stock OCA  <small>Reportes sobre el stock en OCA</small>
              </h1>
            </div>
          </div>
        </div>
        <div class="menuSubHeader">
            <a class="btn btn-sm btn-success pull-right" data-toggle="tooltip" data-placement="top" title="Creación de un nuevo insumo genérico" onclick="mostrarModalCreacion()"><i class="fa fa-plus"></i> Agregar programa</a>
        </div>
<h2><i class="fa fa-ellipsis-v"></i> Programas <small>Visualizacion y gestión de los programas</small></h2>
<div class="row">
    <div class="col-md-8">
        <table width="60%" id="ABMDatatable">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th><abbr data-toggle="tooltip" data-placement="top" title="Indica si posee actualmente stock en oca">Stock en OCA</abbr></th>
                    <th><abbr data-toggle="tooltip" data-placement="top" title="Indica si posee insumos con vencimiento menor a 6 meses">Insumos con vto próximo</abbr></th>
                    <th data-filtrar="false">Acciones</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Stock en OCA</th>
                    <th>Insumos con vto proximo</th>
                    <th>Acciones</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<script src="/assets/funciones.js"></script>
<script>
    var configuracion = {
        "ajax":    '/abm/ajax.php?entity=Programa&operation=list',
        "columns": [
            { "data": "id", "width":"20"},
            { "data": "nombre","width":"150"},
            { "data": "posee_stock_actual","width":"100"},
            { "data": "vto_menor_6m","width":"50"},
            { "data": "","width":"50"}
        ],
        autoWidth: false,
        "order": [[ 1, "asc" ]],
        "rowId": function(row) {
            return 'entity_' + row.id;
        },
        "columnDefs": [
            {
                "targets": 2,   // posee_stock_actual
                "createdCell": function (td, cellData, rowData, row, col) {
                    switch(rowData.posee_stock_actual){
                        case 'Si':   $(td).css({'color': 'black', 'background-color': 'yellowgreen'});
                            break;
                        case 'No':   $(td).css({'color': 'lightgray', 'background-color': 'black'});
                            break;
                    }
                }
            },
            {
                "targets": 3,   // vto_menor_6m
                "render": function ( data, type, row ) {
                    if (data == null){  //puede ser que no posea stock
                        if(row.posee_stock_actual == 'Si'){
                            return 'No'
                        }
                        else{
                            return '(sin stock)'
                        }
                    }
                    else{
                        return 'Si ('+data+' meses)';
                    }

                }
            },
            {
                "targets": 4,
                "render": function ( data, type, row ) {
                    var accionesHTML =  '<a role="button" class="btnABMAcciones accionEditar" href="javascript:mostrarModalEdicion('+row.id+')" title="Editar el programa"><i class="fa fa-pencil"></i></a>';
                    accionesHTML +=  '<a role="button" class="btnABMAcciones accionVisualizar" href="/programas/reporte.php?id='+row.id+'" title="Visualizar reporte del programa"><i class="fa fa-signal"></i></a>';
                    return accionesHTML;
                }
            }
        ]
    };
    DataTableULM('#ABMDatatable','PNUD Insumos',configuracion);


    /**
     * Modal que se muestra ante la edicion de la entidad
     * @param presentacionId
     * @param usado
     */
    function mostrarModalCreacion(){
        formularioABMCreacion(
            'modalABM_form',
            function(){
                $('#modalABM').modal('show');
            }
        );
    }



    /**
     * Modal que se muestra ante la edicion de la entidad
     * @param presentacionId
     * @param usado
     */
    function mostrarModalEdicion(entityId, entidadEditable){
        formularioABMEdicion(
            'modalABM_form',
            entityId,
            function(){
                //callback si todo salio OK: muestro el modal
                $('#modalABM').modal('show');
            }
        );
    }

    function enviarEdicion(){
        formularioABM(
            'modalABM_form',
            function(){
                //callback si todo salio OK: refresco el datatable
                datatableULMAjaxRefresh('#ABMDatatable');
                $('#modalABM').modal('hide');
            }
        );

    }

    $(function(){

        setTimeout(function(){
            $('[data-toggle="tooltip"]').tooltip();
        },1500);

    });
</script>
        <!-- Modal para editar entidad-->
        <div class="modal fade" id="modalABM" tabindex="-1" role="dialog" aria-labelledby="modalABM">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Programa > <span id="modalABM_operacionLabel"></span></h4>
                    </div>
                    <div class="modal-body">
                        <form name="modalABM_form" action="#" data-entity="Programa" method="POST">
                            <input type="hidden" id="programa_id" name="entity[id]" value=""/>
                            <input type="hidden" name="_operation" value=""/>
                            <div class="form-group">
                                <label for="programa_nombre">Nombre</label>
                                <input type="text" class="form-control" id="programa_nombre" name="entity[nombre]" maxlength="64" minlength="5" required/>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn" id="modalABM_operacionButton" onclick="enviarEdicion()"></button>
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
<?php
    include_once('../core/footer.php');
?>
