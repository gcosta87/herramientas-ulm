<?php
include_once('../core/kernel.php');
include_once('../core/config.php');
include_once('../core/header.php');
use Models\Programa;
?>
        <div class="page-header">
          <div class="row">
            <div class="col-md-12">
              <h1 id="buttons">
                <i class="fa fa-tasks fa-fw" aria-hidden="true"></i> Stock OCA  <small>Reportes sobre el stock en OCA</small>
              </h1>
            </div>
          </div>
        </div>
<div class="menuSubHeader">
    <a class="btn btn-sm btn-success pull-right" data-toggle="tooltip" data-placement="top" title="Añadir un insumo concreto a partir de su codigo" onclick="mostrarModalCreacion()"><i class="fa fa-plus"></i> Añadir insumo</a>
</div>
<h2><i class="fa fa-product-hunt"></i> Nómina de insumos&nbsp;<small>Listado de insumos procesables por el Sistema</small></h2>
<p>Se listan los insumos que son  <abbr data-toggle="tooltip" data-placement="top" title="Se reconocen presentaciones, proveedores,...">identificables</abbr> y/o <abbr data-toggle="tooltip" data-placement="top"  title="Es posible calcular su stock en unidades, programa asociado,..." >procesables</abbr> por el Sistema</p>
<p>Adicionalmente se marcan con color <strong style="color: black; background-color: yellowgreen;"> verde </strong> aquellos insumos que sean <abbr data-toggle="tooltip" data-placement="top" title="Indica si el insumo es utilizado (al formar parte del stock de los ultimos 6 meses)">activos</abbr>, y con <strong style="color: lightgray; background-color: black;"> negro </strong> los que no cumplan con dicha condicion.</p>
    <table id="ABMDatatable" width="100%">
        <thead>
        <tr>
            <th>Codigo ULM</th>
            <th>Codigo OCA</th>
            <th>Insumo</th>
            <th>Presentacion</th>
            <th>Proveedor (PNUD)</th>
            <th>Programa</th>
            <th>Activo</th>
            <th data-filtrar="false">Acciones</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>Codigo ULM</th>
            <th>Codigo OCA</th>
            <th>Insumo</th>
            <th>Presentacion</th>
            <th>Proveedor (PNUD)</th>
            <th>Programa</th>
            <th>Activo</th>
            <th>Acciones</th>
        </tr>
        </tfoot>
    </table>
<script src="/assets/funciones.js"></script>
<script>
    var configuracion = {
        "ajax":    '/abm/ajax.php?entity=Insumo&operation=list',
        "autoWidth": false,
        "columns": [
            { "data": "id", "width":30},
            { "data": "codigo_oca","width":80},
            { "data": "insumo","width":200},
            { "data": "presentacion"},
            { "data": "proveedor"},
            { "data": "programa"},
            { "data": "activo","width":50},
            { "data": ""}
        ],
        "order": [[ 2, "asc" ]],
        "columnDefs": [
            {
                "targets": 7,
                "render": function ( data, type, row ) {
                    return  '<a role="button" target="_blank" class="btnABMAcciones accionAsociar" title="Asociar insumo con programa" onclick="mostrarModalAsociarPrograma('+row.id+','+row.programa_id+')"><i class="fa fa-link"></i></a>'+
                            '<a role="button" target="_blank" class="btnABMAcciones" title="Analizar la información del insumo registrada en la base de datos" href="/reportes/insumo.php?codigoULM='+row.id+'"><i class="fa fa-database"></i></a>';
                }
            },
            {
                "targets": 1,
                "createdCell": function (td, cellData, rowData, row, col) {
                    switch(rowData.activo){
                        case 'Si':   $(td).css({'color': 'black', 'background-color': 'yellowgreen'});
                            break;
                        case 'No':   $(td).css({'color': 'lightgray', 'background-color': 'black'});
                            break;
                    }
                }
            }
        ]
    };
    DataTableULM('#ABMDatatable','Nomina de Insumos de la ULM',configuracion);


    /**
     * Modal que se muestra ante la creacion de la entidad
     * @param presentacionId
     * @param usado
     */
    function mostrarModalCreacion(){
        formularioABMCreacion(
            'modalABM_form',
            function(){
                $('#insumo_codigo_oca').val('');
                $('#modalABM').modal('show');
            }
        );
    }

    /**
     * Realiza el envio de la operacion de creacion de  un nuevo insumo concreto
     */
    function enviarFormulario(){
        formularioABM(
            'modalABM_form',
            function(){
                //callback si todo salio OK: refresco el datatable
                datatableULMAjaxRefresh('#ABMDatatable');
                $('#modalABM').modal('hide');
            }
        );

    }

    /**
     * Despliega el modal para la seleccion del programa
     * @param id
     * @param idTarget
     */
    function mostrarModalAsociarPrograma(id, idTarget){
        var $form = $('form[name="modalAsociarInsumo_form"]');

        //seteo los parametros requeridos
        $form.find('input[name="entity[id]"]').val(id);
        $form.find('select[name="programa_id"]').val(idTarget);

        //muestro el modal
        $('#modalAsociarInsumo').modal('show');
    }

    /**
     * En base a lo seleccionado en el modal de asociar programa se solicita la operacion al server
     */
    function asociarPrograma() {
        var $form = $('form[name="modalAsociarInsumo_form"]');
        var entityId, targetId,operacion;

        //recupero los valores
        entityId = $form.find('input[name="entity[id]"]').val()
        targetId = $form.find('select[name="programa_id"]').val();
        operacion = (targetId == -1) ? ABM_FORMULARIO_DESASOCIACION : ABM_FORMULARIO_ASOCIACION;


        asociateEntity('Insumo',entityId,'Programa',targetId,operacion, function () {
           //si salio todo bien...cierro el modal,refresco la tabla y muestro mensaje satisfactorio
            $('#modalAsociarInsumo').modal('hide');
            datatableULMAjaxRefresh();

            if(operacion === ABM_FORMULARIO_ASOCIACION){
                alertify.success('Se ha asociado correctamente el insumo!.');
            }
            else{
                alertify.success('Se ha desasociado correctamente el insumo!.');
            }
        });
    }

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

</script>


<!-- Modal para crear la entidad-->
<div class="modal fade" id="modalABM" tabindex="-1" role="dialog" aria-labelledby="modalABM">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insumo de la Nómina > <span id="modalABM_operacionLabel"></span></h4>
            </div>
            <div class="modal-body">
                <form name="modalABM_form" action="#" data-entity="Insumo" method="POST">
                    <input type="hidden" name="_operation" value=""/>
                    <div class="form-group">
                        <label for="insumo_codigo_oca">Codigo OCA</label>
                        <input type="text" class="form-control" id="insumo_codigo_oca" name="entity[codigo_oca]" maxlength="15" minlength="15" required aria-describedby="ayudaCodigoOCA"/>
                        <span id="ayudaCodigoOCA" class="help-block">Introduzca el Codigo OCA del insumo que se quiera añadir</span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn" id="modalABM_operacionButton" onclick="enviarFormulario()"></button>
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal para asociar la entidad -->
<div class="modal fade" id="modalAsociarInsumo" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insumo de la Nómina > Asociar a programa</h4>
            </div>
            <div class="modal-body">
                <form name="modalAsociarInsumo_form" action="#" data-entity="Insumo" method="POST">
                    <input type="hidden" name="entity[id]" value=""/>
                    <div class="form-group">
                        <label for="selector_programa">Programa</label>
                        <select class="form-control" id="selector_programa" name="programa_id" required aria-describedby="ayudaSelectorPrograma">
                            <option value="-1">(sin asociar)</option>
                            <?php
                            $programas  = Programa::orderby('nombre')->get();
                            if($programas){
                                foreach($programas as $programa){
                                    echo '<option value="'.$programa->id.'">'.$programa->nombre.'</option>';
                                }
                            }
                            ?>
                        </select>
                        <span id="ayudaSelectorPrograma" class="help-block">Se puede cambiar el programa asociado al insumo o dejarlo sin asociar.</span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn" id="modalAsociarInsumo_btnSubmit" onclick="asociarPrograma()">Asociar</button>
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<?php

include_once('../core/footer.php');
?>
