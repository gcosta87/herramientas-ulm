<?php include_once('../core/kernel.php'); ?>
<?php include_once('../core/config.php'); ?>
<?php include_once('../core/header.php'); ?>

<div class="page-header" xmlns="http://www.w3.org/1999/html">
          <div class="row">
            <div class="col-md-12">
              <h1 id="buttons">
                <i class="fa fa-tasks fa-fw" aria-hidden="true"></i> Stock OCA  <small>Reportes sobre el stock en OCA</small>
              </h1>
            </div>
          </div>
        </div>
        <div class="menuSubHeader">
            <a class="btn btn-sm btn-success pull-right" data-toggle="tooltip" data-placement="top" title="Creación de un destino" onclick="mostrarModalCreacion()"><i class="fa fa-plus"></i> Agregar destino</a>

        </div>
<h2><i class="fa fa-map-marker"></i> Destinos <small>Visualizacion y gestión de los destinos utilizados con OCA</small></h2>
<div class="row">
    <div class="col-md-12">
        <table width="60%" id="ABMDatatable">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Dirección</th>
                    <th>Localidad</th>
                    <th>Activo</th>
                    <th data-filtrar="false">Acciones</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Dirección</th>
                    <th>Localidad</th>
                    <th>Activo</th>
                    <th>Acciones</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<script src="/assets/MagicSuggest/magicsuggest-min.js"></script>
<script src="/assets/funciones.js"></script>
<script>
    var configuracion = {
        "ajax":    '/abm/ajax.php?entity=PNUDDestino&operation=list',
        "columns": [
            { "data": "id","width":20},
            { "data": "nombre","width":150},
            { "data": "direccion","width":150},
            { "data": "localidad","width":100},
            { "data": "activo","width":35},
            { "data": "","width":50}
        ],
        "autoWidth": false,
        "order": [[ 1, "asc" ]],
        "rowId": function(row) {
            return 'entity_' + row.id;
        },
        "columnDefs": [
            {
                "targets": 5,
                "render": function ( data, type, row ) {
                   var codigoHTML = '<a role="button"  class="btnABMAcciones accionEditar" href="javascript:mostrarModalEdicion(\''+row.id+'\')" title="Editar el destino"><i class="fa fa-pencil"></i></a>';

                    if(row.direccion != null && row.localidad != null){
                        codigoHTML += '<a role="button"  class="btnABMAcciones" href="javascript:mostrarMapa(\''+row.direccion+'\',\''+row.localidad+'\')" title="Ubicar en mapa"><i class="fa fa-map"></i></a>';
                    }

                    return codigoHTML;
                }
            }
        ]
    };
    DataTableULM('#ABMDatatable','Destinos',configuracion);


    /**
     * Modal que se muestra ante la creacion de la entidad
     * @param presentacionId
     * @param usado
     */
    function mostrarModalCreacion(){
        formularioABMCreacion(
            'modalABM_form',
            function(){
                $('#modalABM').modal('show');
            }
        );
    }



    /**
     * Modal que se muestra ante la edicion de la entidad
     * @param presentacionId
     * @param usado
     */
    function mostrarModalEdicion(entityId){
        formularioABMEdicion(
            'modalABM_form',
            entityId,
            function(){
                //callback si todo salio OK: muestro el modal
                $('#modalABM').modal('show');
            }
        );
    }

    function enviarEdicion(){
        formularioABM(
            'modalABM_form',
            function(){
                //callback si todo salio OK: refresco el datatable
                datatableULMAjaxRefresh('#ABMDatatable');
                $('#modalABM').modal('hide');
            }
        );

    }


    function mostrarMapa(direccion,localidad){
        var $modalMapa = $('#modalMapa');
        //Se acomoda la direccion segun los consejos de la documentacion de google maps
        //Fuente https://support.google.com/maps/answer/3092445
        var direccionLimpia =direccion.replace(/\s*\([^)]+\)\s*/i,' ');//Se eliminan los posibles comentarios en parentesis
        direccionLimpia = direccion.replace(/n([0-9]+)/i,"$1");  //Se elimina el "n" presediendo numeros
        direccionLimpia = direccion.replace(/esq(uina)?|entre/i,"%26"); //Se elimina la "esquina" y se la reemplaza por & (urlencoded)
        console.log(direccionLimpia);

        var queryValue = direccionLimpia+', '+localidad+', Argentina';    //direccion,localidad,argentina


        var url='https://www.google.com/maps/embed/v1/place?key=<?php echo $config['google_maps_key'];?>&q='+queryValue;

        var $iframe = $('#iframeGM');
        $iframe.hide();
        $('#imgCargando').show();
        $iframe.attr('src', url);

        $modalMapa.modal('show');
    }


    $(function(){
        $('#iframeGM').load(function(){
            $('#imgCargando').hide();
            $(this).show();
        });


        setTimeout(function(){
            $('[data-toggle="tooltip"]').tooltip();
        },1500);


    });
</script>
        <!-- Modal para editar entidad-->
        <div class="modal fade" id="modalABM" tabindex="-1" role="dialog" aria-labelledby="modalABM">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Destino > <span id="modalABM_operacionLabel"></span></h4>
                    </div>
                    <div class="modal-body">
                        <form name="modalABM_form" action="#" data-entity="PNUDDestino" method="POST">
                            <input type="hidden" name="_operation" value=""/>
                            <input type="hidden" name="entity[id]"/>
                            <div class="form-group">
                                <label for="destino_nombre">Nombre</label>
                                <input type="text" class="form-control" id="destino_nombre" name="entity[nombre]" maxlength="75" minlength="3" required/>
                            </div>
                            <div class="form-group">
                                <label for="destino_nombre_corto">Nombre Corto (opcional)</label>
                                <input type="text" class="form-control" id="destino_nombre_corto" name="entity[nombre_corto]" maxlength="32" minlength="3" aria-describedby="ayudaNombreCorto"/>
                                <span id="ayudaNombreCorto" class="help-block">Este nombre se usa en algunos reportes (hoy no implementandos del todo)</span>
                            </div>
                            <div class="form-group">
                                <label for="destino_direcion">Direccion</label>
                                <input type="text" class="form-control" id="destino_direccion" name="entity[direccion]" maxlength="50" minlength="5" required/>
                            </div>
                            <div class="form-group">
                                <label for="destino_localidad">Localidad</label>
                                <input type="text" class="form-control" id="destino_localidad" name="entity[localidad]" maxlength="45" minlength="5" required/>
                            </div>
                            <div class="form-group">
                                <label for="destino_nombreFT">Nombre FT</label>
                                <input type="text" class="form-control" id="destino_nombreFT" name="entity[nombre_ft]" maxlength="64" minlength="4" aria-describedby="ayudaNombreFT"/>
                                <span id="ayudaNombreFT" class="help-block">Introduzca el nombre con el cual se hace referencia en los Excel FT y Seguimiento de Pedidos. Afecta en el validador de CMS+DMS al mostrar su interpreación</span>
                            </div>
                            <div class="form-group">
                                <label for="destino_activo">Activo</label>
                                <select class="form-control" id="destino_activo" name="entity[activo]" required/>
                                    <option value="Si" selected>Si</option>
                                    <option value="No">No</option>
                                </select>
                                <span id="ayudaNombreFT" class="help-block">Indique si lo considera como <strong>un destino que actualmente es usado</strong>. Afecta al Asistente de Egresos/Distribuciones</span>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn" id="modalABM_operacionButton" onclick="enviarEdicion()"></button>
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal para mostrar ubicacion -->
        <div class="modal fade" id="modalMapa" tabindex="-1" role="dialog" aria-labelledby="modalABM">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Ubicacion orientativa</span></h4>
                    </div>
                    <div class="modal-body">
                        <center>
                            <img id="imgCargando" width="100" src="/assets/cargando.svg" title="Cargando espere..."/>
                            <iframe id="iframeGM"
                                width="850"
                                height="450"
                                frameborder="0"
                                src=""
                                allowfullscreen
                            >
                            </iframe
                        </center>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

<?php
    include_once('../core/footer.php');
?>
