<?php include_once('../core/kernel.php'); ?>
<?php include_once('../core/config.php'); ?>
<?php include_once('../core/header.php'); ?>

        <div class="page-header">
          <div class="row">
            <div class="col-md-12">
              <h1 id="buttons">
                <i class="fa fa-tasks fa-fw" aria-hidden="true"></i> Stock OCA  <small>Reportes sobre el stock en OCA</small>
              </h1>
            </div>
          </div>
        </div>
        <div class="menuSubHeader">
            <a class="btn btn-sm btn-success pull-right" data-toggle="tooltip" data-placement="top" title="Creación de un nuevo insumo genérico" onclick="mostrarModalCreacion()"><i class="fa fa-plus"></i> Agregar insumo</a>

        </div>
<h2><i class="fa fa-briefcase"></i> Insumos <small>Visualizacion y gestión de los insumos genericos (PNUD)</small></h2>
<div class="row">
    <div class="col-md-12">
        <table width="60%" id="ABMDatatable">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Codigo OCA</th>
                    <th><abbr data-toggle="tooltip" data-placement="top" title="Parámetro para indicar el consumo mensual (en unids) del insumo">Consumo Mensual</abbr></th>
                    <th><abbr data-toggle="tooltip" data-placement="top" title="Nombre que se utiliza en el Excel FT / Seguimiento de Pedidos">Nombre en FT</abbr></th>
                    <th><abbr data-toggle="tooltip" data-placement="top" title="Indica si esta siendo usado en la nómina de insumos">Usado</abbr></th>
                    <th data-filtrar="false">Acciones</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Codigo OCA</th>
                    <th>Consumo Mensual</th>
                    <th>Nombre en FT</th>
                    <th>Usado</th>
                    <th>Acciones</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<script src="/assets/funciones.js"></script>
<script>
    var configuracion = {
        "ajax":    '/abm/ajax.php?entity=PNUDInsumo&operation=list',
        "columns": [
            { "data": "id", "width":"20"},
            { "data": "nombre","width":"150"},
            { "data": "codigo_oca","width":"50"},
            { "data": "stock_mensual_unidades","width":"50"},
            { "data": "nombre_ft","width":"100"},
            { "data": "usado","width":"40"},
            { "data": "","width":"50"}
        ],
        "autoWidth": false,
        "order": [[ 1, "asc" ]],
        "rowId": function(row) {
            return 'entity_' + row.id;
        },
        "columnDefs": [
            {
                "targets": 6,
                "render": function ( data, type, row ) {

                    var editable = (row.editable == 'Si');

                    var accionesHTML =  '<a role="button"  class="btnABMAcciones accionEditar" href="javascript:mostrarModalEdicion('+row.id+','+editable+')" title="Editar la presentacion"><i class="fa fa-pencil"></i></a>';

                    if(row.eliminable == 'Si'){
                        accionesHTML +=  '<a role="button" class="btnABMAcciones accionEliminar" href="javascript:formularioEliminacion(\'PNUDInsumo\','+row.id+',\'nombre\');" title="Eliminar insumo"><i class="fa fa-trash"></i></a>';
                    }

                    return accionesHTML;
                }
            },
            {
                "targets": 1,
                "createdCell": function (td, cellData, rowData, row, col) {
                    switch(rowData.usado){
                        case 'Si':   $(td).css({'color': 'black', 'background-color': 'yellowgreen'});
                            break;
                        case 'No':   $(td).css({'color': 'lightgray', 'background-color': 'black'});
                            break;
                    }
                }
            }
        ]
    };
    DataTableULM('#ABMDatatable','PNUD Insumos',configuracion);


    /**
     * Modal que se muestra ante la edicion de la entidad
     * @param presentacionId
     * @param usado
     */
    function mostrarModalCreacion(){
        formularioABMCreacion(
            'modalABM_form',
            function(){
                //restablezco el posible bloqueo de readonly sobre el codigo oca que haya quedado
                $('#insumo_codigo_oca_corto').prop('readonly',false)
                $('#modalABM').modal('show');
            }
        );
    }



    /**
     * Modal que se muestra ante la edicion de la entidad
     * @param presentacionId
     * @param usado
     */
    function mostrarModalEdicion(entityId, entidadEditable){
        formularioABMEdicion(
            'modalABM_form',
            entityId,
            function(){
                //si esta siendo usado inhabilito la edicion del codigo
                if(entidadEditable){
                    $('#insumo_codigo_oca_corto').prop('readonly',false)
                }
                else{
                    $('#insumo_codigo_oca_corto').prop('readonly',true)
                }

                //callback si todo salio OK: muestro el modal
                $('#modalABM').modal('show');
            }
        );
    }

    function enviarEdicion(){
        formularioABM(
            'modalABM_form',
            function(){
                //callback si todo salio OK: refresco el datatable
                datatableULMAjaxRefresh('#ABMDatatable');
                $('#modalABM').modal('hide');
            }
        );

    }

    $(function(){

        setTimeout(function(){
            $('[data-toggle="tooltip"]').tooltip();
        },1500);

    });
</script>
        <!-- Modal para editar entidad-->
        <div class="modal fade" id="modalABM" tabindex="-1" role="dialog" aria-labelledby="modalABM">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Insumo > <span id="modalABM_operacionLabel"></span></h4>
                    </div>
                    <div class="modal-body">
                        <form name="modalABM_form" action="#" data-entity="PNUDInsumo" method="POST">
                            <input type="hidden" id="insumo_id" name="entity[id]" value=""/>
                            <input type="hidden" name="_operation" value=""/>
                            <div class="form-group">
                                <label for="insumo_nombre">Nombre</label>
                                <input type="text" class="form-control" id="insumo_nombre" name="entity[nombre_oca]" maxlength="64" required/>
                            </div>
                            <div class="form-group">
                                <label for="insumo_nombre">Código OCA</label>
                                <input type="text" class="form-control" id="insumo_codigo_oca_corto" name="entity[codigo_oca_corto]" maxlength="8" minlength="8" required/>
                            </div>
                            <div class="form-group">
                                <label for="insumo_nombre">Nombre en FT</label>
                                <input type="text" class="form-control" id="insumo_nombre_ft" name="entity[nombre_ft]" maxlength="64"/>
                            </div>
                            <div class="form-group">
                                <label for="insumo_nombre">Consumo Mensual</label>
                                <input type="number" max="9999999" min="0" class="form-control" id="insumo_stock_mensual_unidades" name="entity[stock_mensual_unidades]" placeholder="100000" value="" required aria-describedby="ayudaStockMensual"/>
                                <span id="ayudaStockMensual" class="help-block">Indica el consumo mensual estimado.<br/>Se utiliza para el cálculo de abastecimiento.</span>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn" id="modalABM_operacionButton" onclick="enviarEdicion()"></button>
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
<?php
    include_once('../core/footer.php');
?>
