<?php
/**
 * Se procesan el archivo y fecha enviada desde el formualrio de importacion (index.php)
 */
include_once('../core/kernel.php');
include_once('../core/config.php');
include '../vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';
include '../vendor/phpoffice/phpexcel/Classes/PHPExcel/Shared/Date.php';




//
//  CONSTANTES
//

//  Cabecera del rango de campos que se leeran. Se utiliza para validar
const CABECERA_PARCIAL_EXCEL =  ["Lote","FecVto","Palletn","Saldo","CodigoOL"];

//  Indices originales de la cabecera parcial del excel
const   REGISTRO_STOCK_DISCRIMINADO_LOTE        = 0;
const   REGISTRO_STOCK_DISCRIMINADO_FECHA_VTO   = 1;
const   REGISTRO_STOCK_DISCRIMINADO_PALLET      = 2;
const   REGISTRO_STOCK_DISCRIMINADO_SALDO       = 3;
const   REGISTRO_STOCK_DISCRIMINADO_CODIGO_OL   = 4;

//  Indices del registro creado a partir de la lectura del excel
const   REGISTRO_ARRAY_DISCRIMINADO_LOTE        = 0;
const   REGISTRO_ARRAY_DISCRIMINADO_FECHA_VTO   = 1;
const   REGISTRO_ARRAY_DISCRIMINADO_SALDO       = 2;
const   REGISTRO_ARRAY_DISCRIMINADO_CODIGO_OL   = 3;
const   REGISTRO_ARRAY_DISCRIMINADO_FECHA       = 4;

//Insersiones a la tablas
const SQL_INSERT_STOCK_OCA_DISCRIMINADO = 'INSERT INTO `stock_oca_discriminado`(`lote`,`vencimiento`,`cantidad`,`codigo_oca`,`fecha`)  VALUES (?, ?, ?, ?, ?)';
const SQL_INSERT_STOCK_OCA              = 'INSERT INTO `stock_oca`(`codigo_oca`,`total`,`fecha`) VALUES (?, ?, ?)';

//
//  FUNCIONES
//

/**
 * Establece la respuesta para retornar.
 * @param array $respuesta
 * @param string $mensaje motivo del error
 */
function generarRespueastaErronea(&$respuesta = array(),$mensaje){
    $respuesta['error'] = true;
    $respuesta['mensaje'] = $mensaje;
}

/**
 * Valida que la cabecera concuerde con lo esperado.
 * @param array $registro
 * @throws Exception en caso de que sea invalida
 */
function validarCabeceraExcel($registroCabecera = []){
    if(count($registroCabecera) != count(CABECERA_PARCIAL_EXCEL)){
        throw new Exception('La cabecera no posee los 7 campos esperados, en el siguiente orden: Produ, DescProdu, Lote, FecVto, Palletn, Saldo, CodigoOL');
    }

    $camposDiferentes = array_diff($registroCabecera,CABECERA_PARCIAL_EXCEL);
    if($camposDiferentes){
        throw new Exception('La cabecera posee campos diferentes a los esperados. Esperados: '. implode(',',CABECERA_PARCIAL_EXCEL) .'. Diferencia encontrada: '. implode(',',$camposDiferentes) .'.');
    }
}

/***
 *
 * @param $uploadFile
 * @param array $respuesta
 * @throws Exception ante casos de error
 */
function procesarArchivoExcel($uploadFile,&$respuesta = []){
    $archivoSubido  = $uploadFile['tmp_name'];

    //  Read your Excel workbook
    try {
        $inputFileType = PHPExcel_IOFactory::identify($archivoSubido);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($archivoSubido);
    } catch(Exception $e) {
        unlink($uploadFile['tmp_name']);
        throw new Exception('Error al cargar el archivo "'.pathinfo($archivoSubido,PATHINFO_BASENAME).'": '.$e->getMessage());
    }

    //  Get worksheet dimensions
    $sheet = $objPHPExcel->getSheet(0);
    $highestRow = $sheet->getHighestRow();

    //  Loop through each row of the worksheet in turn
    $registros = [];
    try{
        //Debe ampliarse la presicion para evitar lecutras erroneas de numeros en coma floatante para el caso del insumo con codigo oca = "111641271716127"
        ini_set("precision", "15");


        //Registro Cabecera del Excel
        $rowCabecera  = $sheet->rangeToArray('C1:G1', NULL,TRUE,FALSE)[0];
        $respuesta['cabecera'] = $rowCabecera;

        //Se valida la cabecera
        validarCabeceraExcel($rowCabecera);

        unset($rowCabecera);

        for ($row = 2; $row <= $highestRow; $row++){
            //Lectura del rango Cx:Gx Donde
            // C: Lote  	D: FecVto       E:Palletn   	F:Saldo	    G:CodigoOL
            $rowData = $sheet->rangeToArray('C' . $row . ':' . 'G' . $row,
                NULL,
                TRUE,
                FALSE)[0];

            $registros[] = [
                trim($rowData[REGISTRO_STOCK_DISCRIMINADO_LOTE]),
                PHPExcel_Shared_Date::ExcelToPHPObject($rowData[REGISTRO_STOCK_DISCRIMINADO_FECHA_VTO])->format('Y-m-d'),
                intval($rowData[REGISTRO_STOCK_DISCRIMINADO_SALDO]),
                strval($rowData[REGISTRO_STOCK_DISCRIMINADO_CODIGO_OL])
            ];

            unset($rowData);
        }
    }
    catch(Exception $e) {
        unlink($uploadFile['tmp_name']);
        throw new Exception('Error al leer el archivo "'.pathinfo($archivoSubido,PATHINFO_BASENAME).'": '.$e->getMessage());
    }

    //Eliminacion del archivo temporal
    unlink($uploadFile['tmp_name']);

    //debug
    $respuesta['registros'] = $registros;
}


/**
 * Se totalizan los registros del stock discriminado para generar los que se utilizaran para persistir en la BD
 * @param array $registrosStockOCA [0=]
 */
function totalizarRegistrosStockDiscriminado(&$registrosStockOCA = array(),$registroStockOCADiscriminado){
    $codigoOCA  = $registroStockOCADiscriminado[REGISTRO_ARRAY_DISCRIMINADO_CODIGO_OL];
    $cantidad   = $registroStockOCADiscriminado[REGISTRO_ARRAY_DISCRIMINADO_SALDO];

    //Si ya se esta totalizando el valor para el CODIGO_OCA, se incrementa sino, se inicializa
    if(array_key_exists($codigoOCA,$registrosStockOCA)){
        $registrosStockOCA[$codigoOCA] += $cantidad;
    }
    else{
        $registrosStockOCA[$codigoOCA] = $cantidad;
    }
}

/***
 * Define una transaccion en la BD, con los registros a persistir. Se almacenaran todos
 * @param array $respuesta
 */
function persitirRegistrosEnBD(&$respuesta,\Illuminate\Database\Connection $db){
    $fechaDelReporte = $respuesta['fecha'];

    //array donde se totalizan los valores segun codigo_oca,
    $registrosStockOCATotalizacion = [];

    try{
        $db->beginTransaction();

        //  Armado de los insert para el stock descriminado
        foreach($respuesta['registros'] as  $registro){
            //Se añade la fecha requerida para el insert
            $registro[]= $fechaDelReporte;

            //se totaliza
            totalizarRegistrosStockDiscriminado($registrosStockOCATotalizacion,$registro);

            $db->insert(SQL_INSERT_STOCK_OCA_DISCRIMINADO,$registro);
        }

        //  Se renderiza los datos para el stock oca
        $registrosStockOCA = [];
        foreach ($registrosStockOCATotalizacion as $codigoOCA => $cantidad){
            $registrosStockOCA[] = [$codigoOCA,$cantidad,$fechaDelReporte];
        }
        unset($registrosStockOCATotalizacion);

        //debug
        $respuesta['stockOCA'] = $registrosStockOCA;

        //  Armado de insert para el Stock OCA
        foreach($registrosStockOCA as $registro){
            $db->insert(SQL_INSERT_STOCK_OCA,$registro);
        }

        $db->commit();
    }
    catch (Exception $exception){
        $db->rollBack();
        throw new Exception('Hubo un error al guardar los cambios en la BD. Motivo: '.$exception->getMessage());
    }

    unset($registrosStockOCATotalizacion);

    //TODO definir una respuesta satisfactoria o intrudicir detalles
    $respuesta['mensaje']="Se han importado correctamente todos los registros!.";
}

//
// MAIN
//

$respuesta = [
    'error' => false
];

//lectura de params
$importacionFecha = $_POST['importacion_fecha'];
$uploadFile = $_FILES['importacion_archivo'];   //$uploadFile['name'];  //Nombre original

$respuesta['fecha']= $importacionFecha;
ini_set('memory_limit', '120M');     // OK - 512MB
ini_set('max_execution_time', '1600');     // OK - 512MB

try {
    procesarArchivoExcel($uploadFile, $respuesta);
    persitirRegistrosEnBD($respuesta, $db);
}
catch(Exception $exception){
    generarRespueastaErronea($respuesta,$exception->getMessage());
}
echo json_encode($respuesta);
