<?php
require_once 'ABMEntity.php';

class Programa extends ABMEntity {

    const FORM_VALUE_NOMBRE = 'nombre';


    public function __construct(){
        parent::__construct();
    }


    public function onModify($entityModel){
        $this->fillEntity($entityModel);
        static::sendJSONSuccessResponse('Se ha modificado el programa ;)');
    }


    public function onCreation($entityModel){
        $this->fillEntity($entityModel);
        static::sendJSONSuccessResponse('Se ha creado el programa ;)');
    }


    public function onDelete($entityId){
        static::operationNoSupported();
    }

    /**
     * @param $entityModel
     * @throws Exception en caso de habler problemas al persistir (se trata el caso de DuplicateteKey)
     */
    public final function fillEntity($entityModel){
        try {
            //Se recuperan los valores del form y se setean a la entity
            $entityModel->nombre = $this->getFormDataValue(Programa::FORM_VALUE_NOMBRE);
            $entityModel->save();
        }
        catch (Exception $exception){
            if(strpos($exception->getMessage(),'Duplicate entry') === false){
                throw new Exception('Error al guardar en la BD: '.$exception->getMessage());
            }
            else{
                throw new Exception('El nombre del programa ya existe, y por lo tanto no es posible guardar los cambios!.');
            }
        }
    }

    /**
     * Retorna el listado de entities.
     * @return array
     */
    public static function ajaxList(){
       return Programa::getDatabase()->select("
                    select	p.id,
                            p.nombre,
                            if((count(distinct v_soa.insumo_id) > 0),'Si','No') as posee_stock_actual,
                            min(distinct v_sov.venceEnMeses) as vto_menor_6m        
                    from	PROGRAMAS p
                            left join v_stock_oca_vencimientos v_sov
                            on (p.id = v_sov.programa_id and v_sov.venceEnMeses<=6)
                            left join v_stock_oca_actual v_soa
                            on (p.id = v_soa.programa_id)
                    group by p.id, p.nombre
              ");
    }


    /**
     * Retorna la informacion de la entity
     * @return array
     */
    public static function ajaxShow($id){
        return Programa::getDatabase()->select("
                     SELECT id,
                            nombre
                            
                     FROM   PROGRAMAS
                     
                     WHERE id = $id;
              ");
    }


    /**
     * Metodo que valida los datos del form.
     *
     * @param $isCreationOperation indica si la operacion que se ejecuto en la invocacion es de creacion o no (edicion)
     * @throws Exception ante datos invalidos
     */
    public function validateForm($isCreationOperation){
        if(preg_match('/[a-zA-Z0-9ÑñáéíóúÁÉÍÓÚ ]{5,64}/',$this->getFormDataValue(Programa::FORM_VALUE_NOMBRE)) !== 1){
            throw new Exception('El nombre del insumo es invalido. Debe ser una cadena de texto con una longitud máxima de 64 caracteres');
        }
    }

}