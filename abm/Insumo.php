<?php
require_once 'ABMEntity.php';

class Insumo extends ABMEntity {

    const FORM_VALUE_CODIGO_OCA         = 'codigo_oca';

    public function __construct(){
        parent::__construct();
    }


    public function onModify($entityModel){
        $this->operationNoSupported();
    }

    public function onDelete($entityModel){
        $this->operationNoSupported();
    }


    public function onCreation($entityModel){
        $this->fillEntity($entityModel);
        static::sendJSONSuccessResponse('Se ha añadido el insumo ;)');
    }

    /***
     * Invoca la asociacion de la instancia a otra (target)
     * @param $entityId
     * @param $targetEntity string nombre de la entity a asociar
     * @param $targetId integer id de la entity a asociar
     * @param $operacion string constante que identifica la operacion posible ASSOCIATE o UNASSOCIATE
     * @throws Exception en caso de error
     */
    public function onAssociate($entityId,$targetEntity, $targetId,$operacion){
        //Actualmente solo se puede asociar con Programa
        if($targetEntity !== 'Programa'){
            throw new Exception('Se está intentando asociar un insumo con una entidad que no se esperaba: '.$targetEntity);
        }

        $insumo = $this->getEntityModel();
        if(!$insumo){
            throw new Exception('No ha sido posible encontrar al insumo con id #'.$entityId);
        }

        //Siempre se desasociacia
        if($operacion == ABMEntity::FORM_OPERATION_ASSOCIATE){

            $insumo->programas()->sync([$targetId]);
        }
        else{
            //elimina la relacion
            $insumo->programas()->detach();
        }


        static::sendJSONSuccessResponse('Se ha asociado correctamente el programa al insumo indicado.');
    }

    /**
     * @param $entityModel
     * @throws Exception en caso de habler problemas al persistir (se trata el caso de DuplicateteKey)
     */
    public final function fillEntity($entityModel){
        try {
            //Se resuelve a través de una RAW Query
            $db = Insumo::getDatabase();
            $resultOk = false;

            $resultOk = $db->statement('SET @codigoOCA = \''.$this->getFormDataValue(Insumo::FORM_VALUE_CODIGO_OCA).'\';');
            if(!$resultOk){
                throw new Exception('No se ha podido setear parte de la consulta a la Base de Datos.');
            }

            $resultOk = $db->insert('
                INSERT INTO `ulm_extras`.`insumos`(`codigo_oca`,`presentacion`,`proveedor`,`codigo_oca_corto`,`pnud_insumo`) VALUES
                (
                    @codigoOCA,
                    (select p.id from pnud_presentaciones p where p.id = CAST(substr(@codigoOCA,9,2) AS UNSIGNED) ),
                    (select prov.id from pnud_proveedores prov where prov.id = CAST(substr(@codigoOCA,13,3) AS UNSIGNED) ),
                    substr(@codigoOCA,1,8),
                    (select pi.id from pnud_insumos pi where pi.codigo_oca_corto = substr(@codigoOCA,1,8) )
                )
            ');
            if(!$resultOk){
                throw new Exception('No se ha podido insertar el nuevo insumo.');
            }

        }
        catch (Exception $exception){
            if(strpos($exception->getMessage(),'Duplicate entry') !== false){
                throw new Exception('El codigo de OCA provisto ya existe, y por lo tanto no es posible guardar los cambios!.');
            }
            elseif(strpos($exception->getMessage(),'constraint violation') !== false){
                throw new Exception('El Codigo OCA provisto hace referencia a entidades que no existen. Revise la presentacion, proveedor e insumo generico!.');
            }
            else{
                throw new Exception('Error al guardar en la BD: '.$exception->getMessage());
            }
        }
    }

    /**
     * Retorna el listado de entities.
     * @return array
     */
    public static function ajaxList(){
       return Insumo::getDatabase()->select("
                    select	v_i.id as id,
                            v_i.codigo_oca as codigo_oca,
                            v_i.nombre as insumo,
                            v_i.presentacion as presentacion,
                            v_i.proveedor as proveedor,
                            ifnull(p.nombre,'(sin asociar)') as programa,
                            case when codigosUsados.codigo_oca IS NULL
                                then 'No'
                                else 'Si'
                            end as activo,
                            ifnull(p.id,'-1') as programa_id


                    from	v_insumos v_i
                            left join (
                                        select	distinct(codigo_oca) as codigo_oca
                                        from	stock_oca
                                        where 	fecha > (current_date() - INTERVAL 180 day)
                                    ) codigosUsados
                            on (codigosUsados.codigo_oca = v_i.codigo_oca)
                            left join INSUMOS_PROGRAMA ip
                            on (ip.insumo = v_i.id)
                            left join PROGRAMAS p
                            on (p.id = ip.programa)
                            
                    order by v_i.nombre
              ");
    }



    /**
     * Retorna el listado de entities.
     * @return array
     */
    public static function ajaxSelection(){
        $where = '';
        //Se obtiene la palabra de clave de busqueda
        $query = isset($_REQUEST['query']) ? $_REQUEST['query'] : null;

        if($query){
            $where = "where nombre like '%$query%' or codigo_oca like '%$query%'";
        }

        return Insumo::getDatabase()->select("
                    select    id as id, codigo_oca as codigoOCA, nombre as insumo, presentacion as presentacion, proveedor as proveedor
                    from      v_insumos v_i
                    $where
                    order by  nombre, presentacion
              ");
    }



    /**
     * Retorna la informacion de la entity
     * @return array
     */
    public static function ajaxShow($id){
        //esta operacion no estará implementada
        return [];
    }


    /**
     * Metodo que valida los datos del form.
     *
     * @param $isCreationOperation indica si la operacion que se ejecuto en la invocacion es de creacion o no (edicion)
     * @throws Exception ante datos invalidos
     */
    public function validateForm($isCreationOperation){
        if(preg_match('/[A-Z0-9]{15}/',$this->getFormDataValue(Insumo::FORM_VALUE_CODIGO_OCA)) !== 1){
            throw new Exception('El Codigo OCA es invalido. Debe ser una cadena de texto de longitud 15, conformada con letras mayusculas y/o numeros.');
        }
    }
}