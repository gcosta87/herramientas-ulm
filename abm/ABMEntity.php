<?php
require_once '../core/kernel.php';

abstract class ABMEntity{

    //  CONSTANTES
    //  Constantes relativas al form
    const FORM_ENTITY_ID = 'id';

    const FORM_ENTITY = 'entity';

    const FORM_OPERATION        = '_operation';

    const FORM_OPERATION_CREATE         = 'c';
    const FORM_OPERATION_MODIFY         = 'm';
    const FORM_OPERATION_DELETE         = 'd';
    const FORM_OPERATION_ASSOCIATE      = 'a';
    const FORM_OPERATION_UNASSOCIATE    = 'u';

    //  Relativas a las consultas Ajax
    const AJAX_LIST         = 'list';
    const AJAX_SHOW         = 'show';
    const AJAX_SELECTION    = 'select';
    const AJAX_CALL_OPERACION    = 'call_operation';





    //  ATRIBS
    protected $request;

    // METODOS


    public function __construct(){
        $this->setRequest($_REQUEST);

        try{
            if(!array_key_exists(ABMEntity::FORM_OPERATION,$this->getRequest())){
                throw  new Exception('No se ha definido una operacion en el formulario.');
            }

            switch($this->getOperation()){
                case ABMEntity::FORM_OPERATION_CREATE:      $this->validateForm(true);                   //true= isCreation
                                                            $entityModel = $this->retriveEntity(true);
                                                            $this->onCreation($entityModel);
                                                            break;

                case ABMEntity::FORM_OPERATION_MODIFY:      $this->validateForm(false);
                                                            $entityModel = $this->retriveEntity(false);
                                                            $this->onModify($entityModel);
                                                            break;

                case ABMEntity::FORM_OPERATION_DELETE:      $this->onDelete($this->getFormEntityId());
                                                            break;

                case ABMEntity::FORM_OPERATION_ASSOCIATE:
                case ABMEntity::FORM_OPERATION_UNASSOCIATE:
                                                            if(empty($this->getRequest()['target'])){
                                                                throw new Exception('No ha sido definido la entidad a la cual hay que asociar (target).');
                                                            }
                                                            $targetForm = $this->getRequest()['target'];
                                                            $this->onAssociate($this->getFormEntityId(),$targetForm['entity'],$targetForm['id'],$this->getOperation());
                                                            break;

                default:    throw new Exception('Se ha definido una operacion que no es valida o no esta bien definida.');
                            break;
            }
        }
        catch(Exception $exception){
            ABMEntity::sendJSONErrorResponse($exception->getMessage());
        }
    }

    //METODOS ABSTRACTOS / TEMPLATE
    //
    /**
     * Metodo que valida los datos del form. Es ejecutado antes de todo proceso.
     *
     * @param $isCreationOperation indica si la operacion que se ejecuto en la invocacion es de creacion o no (edicion)
     * @throws Exception ante datos invalidos
     */
    public abstract function validateForm($isCreationOperation);

    /**
     * Metodo en cual se retorna una model entity en funcion del tipo de operacion (creacion o edicion).
     *
     * @param $isCreationOperation indica si la operacion que se ejecuto en la invocacion es de creacion o no (edicion)
     * @return Illuminate\Database\Eloquent\Model
     */
    public final function retriveEntity($isCreationOperation){
        if($isCreationOperation){
            return $this->getNewEntityModel();
        }
        else{
            return $this->getEntityModel();
        }
    }

    /**
     * Crea una instancia en funcioon de los datos brindados por el form (y la utilizacion del fillable de la entity)
     * @param $entityModel
     */
    public function onCreation($entityModel){
        //TODO falta implementar en funcion del analisis de entitdades
        new Exception('Funcionalidad no implementada');
    }

    public abstract function onModify($entityModel);


    /***
     * Invoca la eliminacion de la instancia basado en su ID.
     * @param $entityId
     * @throws Exception en caso de error
     */
    public function onDelete($entityId){
        try{
            call_user_func('Models\\'.$this->getEntityName().'::destroy',$entityId);
            static::sendJSONSuccessResponse('Se ha eliminado correctamente ;)');
        }
        catch(Exception $exception){
            throw new Exception('Ha ocurrido un error al intentar eliminar al '.$this->getEntityName().' #'.$entityId.'.');
        }
    }


    /***
     * Invoca la asociacion de la instancia a otra (target)
     * @param $entityId
     * @param $targetEntity string nombre de la entity a asociar
     * @param $targetId integer id de la entity a asociar
     * @param $operacion string constante que identifica la operacion posible ASSOCIATE o UNASSOCIATE
     * @throws Exception en caso de error
     */
    public function onAssociate($entityId,$targetEntity, $targetId,$operacion){
        $this->operationNoSupported();
    }



    /**
     * @param $operacion la operacion a realizar
     * @return String retorna JSONResponse
     */
    public static function ajax($operacion,$parametros = []){
        $data = null;
        $error = false;
        $message = '';
        try{
            switch ($operacion){
                case ABMEntity::AJAX_LIST:      $data = static::ajaxList();
                                                break;
                case ABMEntity::AJAX_SELECTION: header('Content-Type: application/ajax');
                                                echo json_encode(static::ajaxSelection());
                                                return;
                                                break;
                case ABMEntity::AJAX_SHOW:      if(array_key_exists('id',$parametros)){
                                                    $id = is_numeric($parametros['id']) ? intval($parametros['id']) : $parametros['id'];
                                                    $data = static::ajaxShow($id);
                                                    if(empty($data)){
                                                        throw new Exception('No se ha encontrado la entidad!.');
                                                    }
                                                }
                                                else{
                                                    throw new Exception('Falta el parametro id de la entitidad!.');
                                                }

                                                break;

                case ABMEntity::AJAX_CALL_OPERACION:
                                                //Se espera que $parametros posea: [method_name] => 'nombre' y opcionalmente [method_data]=> 'abc...']
                                                if(!array_key_exists('method_name',$parametros)){
                                                    throw new Exception('Falta el parametro method_name para indicar el nombre del metodo!.');
                                                }


                                                //Se delega todo tipo de tratamiento dentro de la entidad
                                                $data = static::ajaxCallOperation($parametros['method_name'], (array_key_exists('method_data',$parametros)) ? $parametros['method_data'] : null );
                                                break;

                default:            throw new Exception('Operacion ajax no encontrada!.');
            }

        }
        catch (Exception $exception){
            $error = true;
            $message = $exception->getMessage();
        }

        ABMEntity::sendJSONResponse($error, $message, $data);
    }

    /**
     * Retorna el listado de entities.
     * @return array
     */
    public static abstract function ajaxList();

    /**
     * Retorna la informacion de entities.
     * @return array
     */
    public static abstract function ajaxShow($id);

    /**
     * Retorna un array con los datos para ser utilizados por algun selector
     * @return array
     */
    public static function ajaxSelection(){
        return [];
    }


    /**
     * Funcion ajax para invocar a una operacion.
     * Se hizo generica para que la entidad que lo implemente pueda invocar operaciones
     * @param  $metodo nombre del metodo (pasado como "method_name")
     * @param $data string datos que se requieran
     */
    public static function ajaxCallOperation($metodo, $data = null){
        //TODO mejorar al lanzar una excepcion
        return 'Invocacion de CALL_OPERATION no implementada en clase concreta!';
    }

    // PSEUDO GETTER / SETTER Y/O HELPERS
    //
    /**
     * Retorna los datos (raw) que fueron enviados por el formulario
     */
    public final function getFormData(){
        return $this->getRequest()[ABMEntity::FORM_ENTITY];
    }


    /**
     * Retorna la operacion requerida en el formulario y/o solicitud.
     * @return string constante de operacion ABM::FORM_OPERATION_XYZ
     */
    public final function getOperation(){
        return $this->getRequest()[ABMEntity::FORM_OPERATION];
    }


    /**
     * Retorna los datos (raw) que fueron enviados por el formulario
     * @throws Exception en caso de no poder leer los valores
     */
    public final function getFormDataValue($key){
        if(!array_key_exists($key,$this->getFormData())){
            throw new Exception('No existe el atributo '.$key.' dentro del form de '.$this->getEntityName());
        }
        return $this->getFormData()[$key];
    }

    /**
     * Retorna si el form posee el atributo/clave que se consulta.
     * @return boolean true caso afirmativo, false caso contrario
     */
    public final function hasFormDataValue($key){
        return (array_key_exists($key,$this->getFormData()));
    }

    /**
     * Determina si se trata de una de un formulario de creacion o no (edicion)
     * @return bool
     */
    public final function isCreation(){
        return ($this->getOperation() == ABMEntity::FORM_OPERATION_CREATE);
    }

    /**
     * Determina si se trata de un formulario y/o operacion de edicion
     * @return bool
     */
    public final function isEdition(){
        return ($this->getOperation() == ABMEntity::FORM_OPERATION_MODIFY);
    }


    /**
     * Determina si se trata de un formulario y/o operacion de edicion
     * @return bool
     */
    public final function isDeletion(){
        return ($this->getOperation() == ABMEntity::FORM_OPERATION_DELETE);
    }



    /**
     * Retorna el nombre de la Entidad que se esta modelando
     * @return String
     */
    public function getEntityName(){
        return get_class($this);
    }


    /**
     * Retorna el modelo (Model Eloquent) asociado a dicha entidad basado en el ID. Este id puede ser especficiado o bien tomado del form
     * @param int $entityId (opcional)
     * @return Illuminate\Database\Eloquent\Model o null en caso de no encontrarlo
     */
    public function getEntityModel($entityId = null){
        $id = $entityId ? $entityId : $this->getFormEntityId();
        return call_user_func('Models\\'.$this->getEntityName().'::find',$id);
    }


    /**
     * Retorna el ID del entity indicado en el form
     * @return int ID de la entity
     */
    public function getFormEntityId(){
        return $this->getFormData()[ABMEntity::FORM_ENTITY_ID];
    }


    /**
     * Retorna una nueva instancia del modelo (Model Eloquent) asociado a dicha entidad.
     * @return Illuminate\Database\Eloquent\Model o null en caso de encontrasr
     */
    public final function getNewEntityModel(){
        $classEntity = 'Models\\'.$this->getEntityName();
        return new $classEntity();
    }



    /**
     * Lanza una exepcion normalizada para indicar que determinado metodo no es soportado
     * @throws Exception respuesta generica
     */
    public final function operationNoSupported(){
        throw new Exception('Operacion no soportada para la entidad '.$this->getEntityName());
    }

    /**
     * Renderizado de un Response generico para enviar al cliente
     * @param bool $error true: errores, false caso contrario
     * @param string $message: mensaje/leyenda para mostrar (ya sea por errores o para mostrar en gral)
     * @param array $data datas
     */
    public static final function sendJSONResponse($error, $message='',$data = []){
        header('Content-Type: application/ajax');
        echo    json_encode([
                    'error' => $error,
                    'message' => $message,
                    'data' => $data
                ]);
    }


    /**
     * Retorna un response al cliente con un mensaje de error.
     * @param $message
     */
    public static final function sendJSONErrorResponse($message){
        return ABMEntity::sendJSONResponse(true,'Ha ocurrido un error. Motivo: '.$message);
    }

    /**
     * Retorna un response al cliente con un mensaje de satisfactorio.
     * @param $message
     */
    public static final function sendJSONSuccessResponse($message){
        return ABMEntity::sendJSONResponse(false,$message);
    }

    //  GETTERS / SETTERS
    //
    /**
     * Request Original ($_REQUEST)
     * @return array
     */
    public final function getRequest(){
        return $this->request;
    }

    /**
     * @param mixed $request
     */
    private final function setRequest($request = []){
        $this->request = $request;
    }

    /**
     * Recupera una conexion a la base de datos
     * @return \Illuminate\Database\Connection
     */
    public static final function getDatabase(){
        global $db;
        return $db;
    }




}