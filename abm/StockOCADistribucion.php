<?php
require_once 'ABMEntity.php';
require_once '../models/StockOCADistribucion.php';

class StockOCADistribucion extends ABMEntity {

    const FORM_VALUE_CODIGO_OCA = 'codigo_oca';
    const FORM_VALUE_TOTAL      = 'total';

    public function __construct(){
        parent::__construct();
    }


    public function onModify($entityModel){
        $this->fillEntity($entityModel);
        static::sendJSONSuccessResponse('Se ha modificado el registro del stock ;)');
    }


    public function onCreation($entityModel){
        $this->fillEntity($entityModel);
        static::sendJSONSuccessResponse('Se ha creado el registro del stock ;)');
    }


    /**
     * @param $entityModel
     * @throws Exception en caso de habler problemas al persistir (se trata el caso de DuplicateteKey)
     */
    public final function fillEntity($entityModel){
        try {
            //Se recuperan los valores del form y se setean a la entity
            $entityModel->codigo_oca  = $this->getFormDataValue(StockOCADistribucion::FORM_VALUE_CODIGO_OCA);
            $entityModel->total  = $this->getFormDataValue(StockOCADistribucion::FORM_VALUE_TOTAL);

            $entityModel->save();
        }
        catch (Exception $exception){
            //TODO analizar si se detecta correctamente por tratarse de PK
            if(strpos($exception->getMessage(),'Duplicate entry') === false){
                throw new Exception('Error al guardar en la BD: '.$exception->getMessage());
            }
            else{
                throw new Exception('El codigo de OCA provisto ya existe, y por lo tanto no es posible guardar los cambios!.');
            }
        }
    }

    /**
     * Retorna el listado de entities.
     * @return array
     */
    public static function ajaxList(){
       return StockOCADistribucion::getDatabase()->select("
                     select	distinct
                                distribucion.codigo_oca as codigo_oca,
                                v_i.nombre as insumo,
                                v_i.presentacion as presentacion,
                                p.nombre as programa,
                                distribucion.total as total
                    from 	stock_oca_distribucion distribucion
                            left join v_insumos v_i
                            on (v_i.codigo_oca = distribucion.codigo_oca)
                            left join INSUMOS_PROGRAMA ip
                            on (v_i.id = ip.insumo)
                            left join PROGRAMAS p
                            on (p.id = ip.programa)
                            
                    order by v_i.nombre
              ");
    }


    /**
     * Retorna la informacion de la entity
     * @return array
     */
    public static function ajaxShow($id){
        return StockOCADistribucion::getDatabase()->select('
                     SELECT codigo_oca,
                            total
                            
                     FROM   stock_oca_distribucion
                     
                     WHERE codigo_oca = "'.$id.'"
              ');
    }


    public static function ajaxCallOperation($metodo, $data = null){
        if($metodo !== 'importar'){
            throw new Exception('Operacion no soportada o no implementada!.');
        }

        //Se ejecutan operaciones raw para simplificar...
        $db = StockOCADistribucion::getDatabase();
        if(! $db->statement('truncate stock_oca_distribucion;')){
            throw new Exception('No ha sido posible limpiar la tabla de stock.');
        }

        $operacionSQL = 'insert into stock_oca_distribucion(codigo_oca, total) select codigo_oca, stock from v_stock_oca_actual;';
        if(! $db->statement($operacionSQL)){
            throw new Exception('No ha sido posible hacer las inserciones necesarias en la tabla de stock de distribucion.');
        }

        return "Los insumos han sido importados correctamente";
    }


    /**
     * Metodo que valida los datos del form.
     *
     * @param $isCreationOperation indica si la operacion que se ejecuto en la invocacion es de creacion o no (edicion)
     * @throws Exception ante datos invalidos
     */
    public function validateForm($isCreationOperation){
        if(preg_match('/[A-Z0-9]{8}/',$this->getFormDataValue(StockOCADistribucion::FORM_VALUE_CODIGO_OCA)) !== 1){
            throw new Exception('El codigo del insumo es invalido.');
        }

        $total= intval($this->getFormDataValue(StockOCADistribucion::FORM_VALUE_TOTAL));
        if($total < 1 and $total > 999999){
            throw new Exception('El total indicado es invalido. Debe ser un numero entre 1 y 999.999');
        }
    }


    /**
     * Se sobreescribe la recuperacion de la entidad dado que se recupera apartir del codigo_oca
     * @param int $entityId (opcional)
     * @return Illuminate\Database\Eloquent\Model o null en caso de no encontrarlo
     */
    public final function getEntityModel($entityId = null){
        $id = $entityId ? $entityId : $this->getFormEntityId();
        $query = call_user_func_array('Models\\'.$this->getEntityName().'::where',['codigo_oca',$id]);

        return $query->firstOrFail();
    }


    /**
     *
     * @param $entityId
     * @throws Exception
     */
    public function onDelete($entityId){
        try{
            $entidad = $this->getEntityModel($entityId);

            if(!$entidad){
                throw new Exception('No ha sido posible recuperar la entidad en base a su id ('.$entityId.')');
            }

            $entidad->delete();
            static::sendJSONSuccessResponse('Se ha eliminado correctamente ;)');
        }
        catch(Exception $exception){
            throw new Exception('Ha ocurrido un error al intentar eliminar al '.$this->getEntityName().' #'.$entityId.'.');
        }
    }


    /**
     * Se sobreescribe el ID, dado que es una
     * @return string codigo_oca
     */
    public function getFormEntityId(){
        $id=null;

        if($this->hasFormDataValue(StockOCADistribucion::FORM_VALUE_CODIGO_OCA)){
            $id = $this->getFormDataValue(StockOCADistribucion::FORM_VALUE_CODIGO_OCA);
        }
        else{
            $id = parent::getFormEntityId();
        }

        return $id;
    }
}