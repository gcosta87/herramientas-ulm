<?php
require_once 'ABMEntity.php';
require_once '../models/PNUDDestino.php';

class PNUDDestino extends ABMEntity {

    /*
      ,
    ,
    ,
    ,
    ,


     */
    const FORM_VALUE_NOMBRE         = 'nombre';
    const FORM_VALUE_NOMBRE_CORTO   = 'nombre_corto';
    const FORM_VALUE_LOCALIDAD      = 'localidad';
    const FORM_VALUE_NOMBRE_FT      = 'nombre_ft';
    const FORM_VALUE_DIRECCION      = 'direccion';
    const FORM_VALUE_ACTIVO         = 'activo';

    public function __construct(){
        parent::__construct();
    }


    public function onModify($entityModel){
        $this->fillEntity($entityModel);
        static::sendJSONSuccessResponse('Se ha modificado el destino ;)');
    }


    public function onCreation($entityModel){
        $this->fillEntity($entityModel);
        static::sendJSONSuccessResponse('Se ha creado el destino ;)');
    }


    public function onDelete($entityId){
        static::operationNoSupported();
    }

    /**
     * @param $entityModel
     * @throws Exception en caso de habler problemas al persistir (se trata el caso de DuplicateteKey)
     */
    public final function fillEntity($entityModel){
        try {
            //Se recuperan los valores del form y se setean a la entity
            $entityModel->nombre        = $this->getFormDataValue(PNUDDestino::FORM_VALUE_NOMBRE);
            $entityModel->nombre_corto  = $this->getFormDataValue(PNUDDestino::FORM_VALUE_NOMBRE_CORTO);
            $entityModel->nombre_ft     = $this->getFormDataValue(PNUDDestino::FORM_VALUE_NOMBRE_FT);

            $entityModel->direccion = $this->getFormDataValue(PNUDDestino::FORM_VALUE_DIRECCION);
            $entityModel->localidad = $this->getFormDataValue(PNUDDestino::FORM_VALUE_LOCALIDAD);
            $entityModel->activo    = ($this->getFormDataValue(PNUDDestino::FORM_VALUE_ACTIVO) == 'Si') ? true:false;

            $entityModel->save();
        }
        catch (Exception $exception){
            throw new Exception('Error al guardar en la BD: '.$exception->getMessage());
        }
    }

    /**
     * Retorna el listado de entities.
     * @return array
     */
    public static function ajaxList(){
       return PNUDDestino::getDatabase()->select("
                     select	id,
                            nombre,
                            direccion, 
                            localidad,
                            if(activo = 0,'No','Si') as activo

                    from	pnud_destinos        

                    order by nombre
              ");
    }


    /**
     * Retorna la informacion de la entity
     * @return array
     */
    public static function ajaxShow($id){
        return PNUDDestino::getDatabase()->select('
                     select	id,
                            nombre,
                            localidad,
                            nombre_corto,
                            nombre_ft,
                            direccion, 
                            if(activo = 0,\'No\',\'Si\') as activo

                    from	pnud_destinos        
                    where   id = "'.$id.'"
              ');
    }


    /**
     * Metodo que valida los datos del form.
     *
     * @param $isCreationOperation indica si la operacion que se ejecuto en la invocacion es de creacion o no (edicion)
     * @throws Exception ante datos invalidos
     */
    public function validateForm($isCreationOperation){
        if(preg_match('/[ÑñáéíóúÁÉÍÓÚ]/',$this->getFormDataValue(PNUDDestino::FORM_VALUE_DIRECCION))){
            throw new Exception('La direccion no puede contener acentos ni Ñ. Reemplace estos para poder guardar el destino.');
        }

        if(preg_match('/[ÑñáéíóúÁÉÍÓÚ]/',$this->getFormDataValue(PNUDDestino::FORM_VALUE_LOCALIDAD))){
            throw new Exception('La localidad no puede contener acentos ni Ñ. Reemplace estos para poder guardar el destino.');
        }

    }




}