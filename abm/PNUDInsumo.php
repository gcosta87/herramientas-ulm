<?php
require_once 'ABMEntity.php';

class PNUDInsumo extends ABMEntity {

    const FORM_VALUE_NOMBRE_OCA         = 'nombre_oca';
    const FORM_VALUE_NOMBRE_FT          = 'nombre_ft';
    const FORM_VALUE_CODIGO_OCA_CORTO   = 'codigo_oca_corto';
    const FORM_VALUE_STOCK_MENSUAL_UNIDADES   = 'stock_mensual_unidades';

    public function __construct(){
        parent::__construct();
    }


    public function onModify($entityModel){
        $this->fillEntity($entityModel);
        static::sendJSONSuccessResponse('Se ha modificado el insumo ;)');
    }


    public function onCreation($entityModel){
        $this->fillEntity($entityModel);
        static::sendJSONSuccessResponse('Se ha creado el insumo ;)');
    }


    /**
     * @param $entityModel
     * @throws Exception en caso de habler problemas al persistir (se trata el caso de DuplicateteKey)
     */
    public final function fillEntity($entityModel){
        try {
            //Se recuperan los valores del form y se setean a la entity
            $entityModel->nombre_oca = $this->getFormDataValue(PNUDInsumo::FORM_VALUE_NOMBRE_OCA);
            $entityModel->nombre_ft = $this->getFormDataValue(PNUDInsumo::FORM_VALUE_NOMBRE_FT);
            $entityModel->codigo_oca_corto = $this->getFormDataValue(PNUDInsumo::FORM_VALUE_CODIGO_OCA_CORTO);
            $entityModel->stock_mensual_unidades= $this->getFormDataValue(PNUDInsumo::FORM_VALUE_STOCK_MENSUAL_UNIDADES);

            $entityModel->save();
        }
        catch (Exception $exception){
            if(strpos($exception->getMessage(),'Duplicate entry') === false){
                throw new Exception('Error al guardar en la BD: '.$exception->getMessage());
            }
            else{
                throw new Exception('El codigo de OCA provisto ya existe, y por lo tanto no es posible guardar los cambios!.');
            }
        }
    }

    /**
     * Retorna el listado de entities.
     * @return array
     */
    public static function ajaxList(){

        //Se consideran eliminables/editables aquellos creados a partir del flujo del ABM (id > 847). Solo porque los codigos generados hasta el momento
        // han sido (presumiblemente) utilizados con OCA, por ende es preferible

       return PNUDInsumo::getDatabase()->select("
                        select	DISTINCT 
                                pi.id as id,
                                pi.nombre_oca as nombre,
                                pi.codigo_oca_corto as codigo_oca,
                                pi.nombre_ft as nombre_ft,
                                pi.stock_mensual_unidades as stock_mensual_unidades,

                                if(i.id is null,'No','Si') as usado,
                                if((pi.id > 847) and (i.id is null),'Si','No') as editable,
                                if((pi.id > 847) and (i.id is null),'Si','No') as eliminable

                        from	pnud_insumos pi
                                left join insumos i
                                on (pi.codigo_oca_corto = i.codigo_oca_corto)
 
                        order by nombre
              ");
    }


    /**
     * Retorna la informacion de la entity
     * @return array
     */
    public static function ajaxShow($id){
        return PNUDInsumo::getDatabase()->select("
                     SELECT id,
                            nombre_oca,
                            nombre_ft,
                            codigo_oca_corto,
                            stock_mensual_unidades
                            
                     FROM   pnud_insumos
                     
                     WHERE id = $id;
              ");
    }


    /**
     * Metodo que valida los datos del form.
     *
     * @param $isCreationOperation indica si la operacion que se ejecuto en la invocacion es de creacion o no (edicion)
     * @throws Exception ante datos invalidos
     */
    public function validateForm($isCreationOperation){

        if(preg_match('/[a-zA-Z0-9ÑñáéíóúÁÉÍÓÚ ]{2,64}/',$this->getFormDataValue(PNUDInsumo::FORM_VALUE_NOMBRE_OCA)) !== 1){
            throw new Exception('El nombre del insumo es invalido. Debe ser una cadena de texto con una longitud máxima de 64 caracteres');
        }

        if(preg_match('/[A-Z0-9]{8}/',$this->getFormDataValue(PNUDInsumo::FORM_VALUE_CODIGO_OCA_CORTO)) !== 1){
            throw new Exception('El codigo del insumo es invalido. Debe ser una cadena de texto de longitud 8, conformada con letras mayusculas y/o numeros.');
        }

        $nombreFT = $this->getFormDataValue(PNUDInsumo::FORM_VALUE_NOMBRE_FT);
        if(!empty($nombreFT)){
            if(preg_match('/[a-zA-Z0-9ÑñáéíóúÁÉÍÓÚ ]{2,64}/',$nombreFT) !== 1){
                throw new Exception('El nombre FT del insumo es invalido. Debe ser una cadena de texto con una longitud máxima de 64 caracteres');
            }
        }

        $stockMensual = $this->getFormDataValue(PNUDInsumo::FORM_VALUE_STOCK_MENSUAL_UNIDADES);
        if(preg_match('/[0-9]{1,8}/',$stockMensual) !== 1){
            throw new Exception('El parametro de stock mensual debe ser un numero entre 0 y 99.999.999.');
        }
        else{
            $stockMensual = intval($stockMensual);

            if($stockMensual < 0 || $stockMensual >= 99999999){
                throw new Exception('El parametro de stock mensual debe poseer un valor entre 0 y 99.999.999.');
            }
        }
    }

}