<?php
require_once 'ABMEntity.php';

class PNUDPresentacion extends ABMEntity {

    const FORM_VALUE_NOMBRE         = 'nombre';
    const FORM_VALUE_MULTIPLICADOR  = 'multiplicador';

    public function __construct(){
        parent::__construct();
    }


    public function onModify($entityModel)
    {
        //Se recuperan los valores del form y se setean a la entity
        $entityModel->nombre = $this->getFormDataValue(PNUDPresentacion::FORM_VALUE_NOMBRE);
        $entityModel->multiplicador = $this->getFormDataValue(PNUDPresentacion::FORM_VALUE_MULTIPLICADOR);
        $entityModel->save();

        static::sendJSONSuccessResponse('Se ha modificado la presentacion ;)');
    }

    //  Operaciones no soportadas dado que se re-utilizan las presentaciones
    //
    public function onCreation($entityModel)
    {
        $this->operationNoSupported();
    }

    public function onDelete($entityId)
    {
        $this->operationNoSupported();
    }


    /**
     * Retorna el listado de entities.
     * @return array
     */
    public static function ajaxList(){
       return PNUDPresentacion::getDatabase()->select("
                      select    distinct 
                                p.id as id,
                                p.nombre as nombre,
                                p.multiplicador as multiplicador,
                                if(i.id is null,'No','Si') as usado
                                
                      from      pnud_presentaciones p
                                left join insumos i
                                on (p.id = i.presentacion)
                      order by nombre
              ");
    }


    /**
     * Retorna la informacion de la entity
     * @return array
     */
    public static function ajaxShow($id){
        return PNUDPresentacion::getDatabase()->select("
                     SELECT id,
                            nombre,
                            multiplicador
                            
                     FROM   pnud_presentaciones
                     
                     WHERE id = $id;
              ");
    }


    /**
     * Metodo que valida los datos del form.
     *
     * @param $isCreationOperation indica si la operacion que se ejecuto en la invocacion es de creacion o no (edicion)
     * @throws Exception ante datos invalidos
     */
    public function validateForm($isCreationOperation){

        if(preg_match('/[a-zA-Z0-9ÑñáéíóúÁÉÍÓÚ ]{2,64}/',$this->getFormDataValue(PNUDPresentacion::FORM_VALUE_NOMBRE)) !== 1){
            throw new Exception('El nombre de la presentacion es invalido. Debe ser una cadena de texto con una longitud máxima de 64 caracteres');
        }

        $multiplicador = $this->getFormDataValue(PNUDPresentacion::FORM_VALUE_MULTIPLICADOR);
        if(preg_match('/[0-9]{1,5}/',$multiplicador) !== 1){
            throw new Exception('El multiplicador debe ser un numero entre 1 y 50.000.');
        }
        else{
            $multiplicador = intval($multiplicador);

            if($multiplicador <= 0 || $multiplicador >= 50000){
                throw new Exception('El multiplicador debe poseer un valor entre 1 y 50.000.');
            }
        }
    }

}