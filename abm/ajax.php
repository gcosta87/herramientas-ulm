<?php
/**
 * Pequeño script que retorna las respuestas de las entities.
 * Ejemplo:
 *      /abm/ajax.php?entity=PNUDENTITY&operation=OPERACION
 * Donde:
 *    - PNUDEntity: es una de las /abm/PNUDXXXX
 *    - OPERACION: es una de las operaciones soportadas, indicadas en las constastes ABMEntity::AJAX_XXX
 */

require_once '../core/kernel.php';
require_once 'ABMEntity.php';

$response = ['error'=>false,'message'=>''];
if(isset($_GET['entity'])){

    $entity = $_GET['entity'];
    $operacion = $_GET['operation'];

    $parametros = (isset($_GET['parameters']))? $_GET['parameters'] : [];

    try{
        require_once "$entity.php";
        //invocacion dinamica
       call_user_func($entity.'::ajax', $operacion,$parametros);
    }
    catch (Exception $exception){
        ABMEntity::sendJSONErrorResponse($exception->getMessage());
    }
}
else{
    ABMEntity::sendJSONErrorResponse('Error al invocar al servicio');
}

